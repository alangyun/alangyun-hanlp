package com.hankcs.hanlp.dictionary.occurrence;

public interface ITermFrequency {

	int termFrequency(String word);
}
