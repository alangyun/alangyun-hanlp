/*
 * <author>Han He</author>
 * <email>me@hankcs.com</email>
 * <create-date>2021-01-30 11:12 PM</create-date>
 *
 * <copyright file="DynamicCustomDictionary.java">
 * Copyright (c) 2021, Han He. All Rights Reserved, http://www.hankcs.com/
 * See LICENSE file in the project root for full license information.
 * </copyright>
 */
package com.hankcs.hanlp.dictionary;

import com.hankcs.hanlp.config.NlpSetting;
import com.hankcs.hanlp.ahocorasick.AhoCorasickDoubleArrayTrie;
import com.hankcs.hanlp.collection.array.ByteArray;
import com.hankcs.hanlp.collection.trie.DoubleArrayTrie;
import com.hankcs.hanlp.collection.trie.bintrie.BinTrie;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.chartable.CharTable;
import com.hankcs.hanlp.meta.roletag.Nature;
import com.hankcs.hanlp.meta.searcher.BaseSearcher;
import com.hankcs.hanlp.meta.word.WordAttribute;
import com.hankcs.hanlp.utility.LexiconUtility;
import com.hankcs.hanlp.utility.Predefine;
import com.hankcs.hanlp.utility.TextUtility;

import java.io.*;
import java.util.*;

/**
 * 用户自定义词典<br>
 * 注意自定义词典的动态增删改不是线程安全的。
 *
 * @author hankcs
 */
public class DynamicCustomDictionary {
	private static String myName = DynamicCustomDictionary.class.getSimpleName();
	
	/** 用于储存用户动态插入词条的二分trie树 */
	public BinTrie<WordAttribute> trie;
	/** 用于储存文件中的词条 */
	public DoubleArrayTrie<WordAttribute> dat;
	/** 本词典是从哪些路径加载得到的 */
	public String path[];
	/** 是否执行字符正规化（繁体->简体，全角->半角，大写->小写），切换配置后必须删CustomDictionary.txt.bin缓存 */
	public boolean normalization = NlpSetting.Normalization;

	/**
	 * 构造一份词典对象，并加载{@code com.hankcs.hanlp.HanLP.Config#CustomDictionaryPath}
	 */
	public DynamicCustomDictionary() {
		this(NlpSetting.CustomDictionaryPath);
	}

	/**
	 * 构造一份词典对象，并加载指定路径的词典
	 *
	 * @param path 词典路径
	 */
	public DynamicCustomDictionary(String... path) {
		this(new DoubleArrayTrie<WordAttribute>(), new BinTrie<WordAttribute>(), path);
	}

	/**
	 * 使用高级数据结构构造词典对象，并加载指定路径的词典
	 *
	 * @param dat  双数组trie树
	 * @param trie trie树
	 * @param path 词典路径
	 */
	public DynamicCustomDictionary(DoubleArrayTrie<WordAttribute> dat, BinTrie<WordAttribute> trie, String[] path) {
		this.dat = dat;
		this.trie = trie;
		if (path != null) {
			load(path);
		}
	}

	/**
	 * 加载指定路径的词典
	 *
	 * @param path 词典路径
	 * @return 是否加载成功
	 */
	public boolean load(String... path) {
		long start = System.currentTimeMillis();
		if (!loadMainDictionary(path[0], normalization)) {
			NlpLogger.warning(myName, "自定义词典{}加载失败",Arrays.toString(path));
			return false;
		}
			NlpLogger.info(myName, "自定义词典加载成功:{}个词条，耗时{}ms" ,dat.size(), (System.currentTimeMillis() - start));
			this.path = path;
			return true;
		
	}

	/**
	 * 加载词典
	 *
	 * @param mainPath 缓存文件文件名
	 * @param path     自定义词典
	 * @param isCache  是否缓存结果
	 */
	public static boolean loadMainDictionary(String mainPath, String path[], DoubleArrayTrie<WordAttribute> dat, boolean isCache, boolean normalization) {
		NlpLogger.info(myName, "自定义词典开始加载:{}" , mainPath);
		if (loadDat(mainPath, dat))
			return true;
		TreeMap<String, WordAttribute> map = new TreeMap<String, WordAttribute>();
		LinkedHashSet<Nature> customNatureCollector = new LinkedHashSet<Nature>();
		try {
			// String path[] = NlpSetting.CustomDictionaryPath;
			for (String p : path) {
				Nature defaultNature = Nature.n;
				File file = new File(p);
				String fileName = file.getName();
				int cut = fileName.lastIndexOf(' ');
				if (cut > 0) {
					// 有默认词性
					String nature = fileName.substring(cut + 1);
					p = file.getParent() + File.separator + fileName.substring(0, cut);
					try {
						defaultNature = LexiconUtility.convertStringToNature(nature, customNatureCollector);
					} catch (Exception e) {
						NlpLogger.error(myName, "配置文件[{}]写错了！" , p, e);
						continue;
					}
				}
				NlpLogger.info(myName, "以默认词性[{}]加载自定义词典{}中……",defaultNature,p);
				boolean success = load(p, defaultNature, map, customNatureCollector, normalization);
				if (!success)
					NlpLogger.warning(myName, "失败：{}" , p);
			}
			if (map.size() == 0) {
				NlpLogger.warning(myName, "没有加载到任何词条");
				map.put(Predefine.TAG_OTHER, null); // 当作空白占位符
			}
			NlpLogger.info(myName, "正在构建DoubleArrayTrie……");
			dat.build(map);
			if (isCache) {
				// 缓存成dat文件，下次加载会快很多
				NlpLogger.info(myName, "正在缓存词典为dat文件……");
				// 缓存值文件
				List<WordAttribute> attributeList = new LinkedList<WordAttribute>();
				for (Map.Entry<String, WordAttribute> entry : map.entrySet()) {
					attributeList.add(entry.getValue());
				}
				DataOutputStream out = new DataOutputStream(new BufferedOutputStream(IOUtil.newOutputStream(mainPath + Predefine.BIN_EXT)));
				// 缓存用户词性
				if (customNatureCollector.isEmpty()) // 热更新
				{
					for (int i = Nature.begin.ordinal() + 1; i < Nature.values().length; ++i) {
						customNatureCollector.add(Nature.values()[i]);
					}
				}
				IOUtil.writeCustomNature(out, customNatureCollector);
				// 缓存正文
				out.writeInt(attributeList.size());
				for (WordAttribute attribute : attributeList) {
					attribute.save(out);
				}
				dat.save(out);
				out.close();
			}
		} catch (FileNotFoundException e) {
			NlpLogger.warning(myName, "自定义词典{}不存在！",mainPath, e);
			return false;
		} catch (IOException e) {
			NlpLogger.warning(myName, "自定义词典{}读取错误！",mainPath, e);
			return false;
		} catch (Exception e) {
			NlpLogger.warning(myName, "自定义词典{}缓存失败！" ,mainPath, e);
		}
		return true;
	}

	/**
	 * 使用词典路径为缓存路径，加载指定词典
	 *
	 * @param mainPath 词典路径（+.bin等于缓存路径）
	 * @return
	 */
	public boolean loadMainDictionary(String mainPath, boolean normalization) {
		return loadMainDictionary(mainPath, NlpSetting.CustomDictionaryPath, this.dat, true, normalization);
	}

	/**
	 * 加载用户词典（追加）
	 *
	 * @param path                  词典路径
	 * @param defaultNature         默认词性
	 * @param customNatureCollector 收集用户词性
	 * @return
	 */
	public static boolean load(String path, Nature defaultNature, TreeMap<String, WordAttribute> map, LinkedHashSet<Nature> customNatureCollector,
			boolean normalization) {
		try {
			String splitter = "\\s";
			if (path.endsWith(".csv")) {
				splitter = ",";
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(path), "UTF-8"));
			String line;
			boolean firstLine = true;
			while ((line = br.readLine()) != null) {
				if (firstLine) {
					line = IOUtil.removeUTF8BOM(line);
					firstLine = false;
				}
				String[] param = line.split(splitter);
				if (param[0].length() == 0)
					continue; // 排除空行
				if (normalization)
					param[0] = CharTable.convert(param[0]); // 正规化

				int natureCount = (param.length - 1) / 2;
				WordAttribute attribute;
				if (natureCount == 0) {
					attribute = new WordAttribute(defaultNature);
				} else {
					attribute = new WordAttribute(natureCount);
					for (int i = 0; i < natureCount; ++i) {
						attribute.nature[i] = LexiconUtility.convertStringToNature(param[1 + 2 * i], customNatureCollector);
						attribute.frequency[i] = Integer.parseInt(param[2 + 2 * i]);
						attribute.totalFrequency += attribute.frequency[i];
					}
				}
//                if (updateAttributeIfExist(param[0], attribute, map, rewriteTable)) continue;
				map.put(param[0], attribute);
			}
			br.close();
		} catch (Exception e) {
			NlpLogger.error(myName, "自定义词典{}读取错误！",path, e);
			return false;
		}

		return true;
	}

	/**
	 * 如果已经存在该词条,直接更新该词条的属性
	 *
	 * @param key          词语
	 * @param attribute    词语的属性
	 * @param map          加载期间的map
	 * @param rewriteTable
	 * @return 是否更新了
	 */
	private boolean updateAttributeIfExist(String key, WordAttribute attribute, TreeMap<String, WordAttribute> map,
			TreeMap<Integer, WordAttribute> rewriteTable) {
		int wordID = CoreDictionary.getWordID(key);
		WordAttribute attributeExisted;
		if (wordID != -1) {
			attributeExisted = CoreDictionary.get(wordID);
			attributeExisted.nature = attribute.nature;
			attributeExisted.frequency = attribute.frequency;
			attributeExisted.totalFrequency = attribute.totalFrequency;
			// 收集该覆写
			rewriteTable.put(wordID, attribute);
			return true;
		}

		attributeExisted = map.get(key);
		if (attributeExisted != null) {
			attributeExisted.nature = attribute.nature;
			attributeExisted.frequency = attribute.frequency;
			attributeExisted.totalFrequency = attribute.totalFrequency;
			return true;
		}

		return false;
	}

	/**
	 * 往自定义词典中插入一个新词（非覆盖模式）<br>
	 * 动态增删不会持久化到词典文件
	 *
	 * @param word                新词 如“裸婚”
	 * @param natureWithFrequency 词性和其对应的频次，比如“nz 1 v 2”，null时表示“nz 1”
	 * @return 是否插入成功（失败的原因可能是不覆盖、natureWithFrequency有问题等，后者可以通过调试模式了解原因）
	 */
	public boolean add(String word, String natureWithFrequency) {
		if (contains(word))
			return false;
		return insert(word, natureWithFrequency);
	}

	/**
	 * 往自定义词典中插入一个新词（非覆盖模式）<br>
	 * 动态增删不会持久化到词典文件
	 *
	 * @param word 新词 如“裸婚”
	 * @return 是否插入成功（失败的原因可能是不覆盖等，可以通过调试模式了解原因）
	 */
	public boolean add(String word) {
		if (normalization)
			word = CharTable.convert(word);
		if (contains(word))
			return false;
		return insert(word, null);
	}

	/**
	 * 往自定义词典中插入一个新词（覆盖模式）<br>
	 * 动态增删不会持久化到词典文件
	 *
	 * @param word                新词 如“裸婚”
	 * @param natureWithFrequency 词性和其对应的频次，比如“nz 1 v 2”，null时表示“nz 1”。
	 * @return 是否插入成功（失败的原因可能是natureWithFrequency问题，可以通过调试模式了解原因）
	 */
	public boolean insert(String word, String natureWithFrequency) {
		if (word == null)
			return false;
		if (normalization)
			word = CharTable.convert(word);
		WordAttribute att = natureWithFrequency == null ? new WordAttribute(Nature.nz, 1) : WordAttribute.create(natureWithFrequency);
		if (att == null)
			return false;
		if (dat.set(word, att))
			return true;
		if (trie == null)
			trie = new BinTrie<WordAttribute>();
		trie.put(word, att);
		return true;
	}

	/**
	 * 以覆盖模式增加新词<br>
	 * 动态增删不会持久化到词典文件
	 *
	 * @param word
	 * @return
	 */
	public boolean insert(String word) {
		return insert(word, null);
	}

	public static boolean loadDat(String path, DoubleArrayTrie<WordAttribute> dat) {
		return loadDat(path, NlpSetting.CustomDictionaryPath, dat);
	}

	/**
	 * 从磁盘加载双数组
	 *
	 * @param path          主词典路径
	 * @param customDicPath 用户词典路径
	 * @return
	 */
	public static boolean loadDat(String path, String customDicPath[], DoubleArrayTrie<WordAttribute> dat) {
		try {
			if (NlpSetting.CustomDictionaryAutoRefreshCache && isDicNeedUpdate(path, customDicPath)) {
				return false;
			}
			ByteArray byteArray = ByteArray.createByteArray(path + Predefine.BIN_EXT);
			if (byteArray == null)
				return false;
			int size = byteArray.nextInt();
			if (size < 0) // 一种兼容措施,当size小于零表示文件头部储存了-size个用户词性
			{
				while (++size <= 0) {
					Nature.create(byteArray.nextString());
				}
				size = byteArray.nextInt();
			}
			WordAttribute[] attributes = new WordAttribute[size];
			final Nature[] natureIndexArray = Nature.values();
			for (int i = 0; i < size; ++i) {
				// 第一个是全部频次，第二个是词性个数
				int currentTotalFrequency = byteArray.nextInt();
				int length = byteArray.nextInt();
				attributes[i] = new WordAttribute(length);
				attributes[i].totalFrequency = currentTotalFrequency;
				for (int j = 0; j < length; ++j) {
					attributes[i].nature[j] = natureIndexArray[byteArray.nextInt()];
					attributes[i].frequency[j] = byteArray.nextInt();
				}
			}
			if (!dat.load(byteArray, attributes))
				return false;
		} catch (Exception e) {
			NlpLogger.warning(myName, "读取失败",e);
			return false;
		}
		return true;
	}

	/**
	 * 获取本地词典更新状态
	 *
	 * @return true 表示本地词典比缓存文件新，需要删除缓存
	 */
	public static boolean isDicNeedUpdate(String mainPath, String path[]) {
		if (NlpSetting.IOAdapter != null && !NlpSetting.IOAdapter.getClass().getName().contains("com.hankcs.hanlp.corpus.io.FileIOAdapter")) {
			return false;
		}
		String binPath = mainPath + Predefine.BIN_EXT;
		File binFile = new File(binPath);
		if (!binFile.exists()) {
			return true;
		}
		long lastModified = binFile.lastModified();
		// String path[] = NlpSetting.CustomDictionaryPath;
		for (String p : path) {
			File f = new File(p);
			String fileName = f.getName();
			int cut = fileName.lastIndexOf(' ');
			if (cut > 0) {
				p = f.getParent() + File.separator + fileName.substring(0, cut);
			}
			f = new File(p);
			if (f.exists() && f.lastModified() > lastModified) {
				IOUtil.deleteFile(binPath); // 删掉缓存
				NlpLogger.info(myName, "已清除自定义词典缓存文件！");
				return true;
			}
		}
		return false;
	}

	/**
	 * 查单词
	 *
	 * @param key
	 * @return
	 */
	public WordAttribute get(String key) {
		if (normalization)
			key = CharTable.convert(key);
		WordAttribute attribute = dat.get(key);
		if (attribute != null)
			return attribute;
		if (trie == null)
			return null;
		return trie.get(key);
	}

	/**
	 * 删除单词<br>
	 * 动态增删不会持久化到词典文件
	 *
	 * @param key
	 */
	public void remove(String key) {
		if (normalization)
			key = CharTable.convert(key);
		if (trie == null)
			return;
		trie.remove(key);
	}

	/**
	 * 前缀查询
	 *
	 * @param key
	 * @return
	 */
	public LinkedList<Map.Entry<String, WordAttribute>> commonPrefixSearch(String key) {
		return trie.commonPrefixSearchWithValue(key);
	}

	/**
	 * 前缀查询
	 *
	 * @param chars
	 * @param begin
	 * @return
	 */
	public LinkedList<Map.Entry<String, WordAttribute>> commonPrefixSearch(char[] chars, int begin) {
		return trie.commonPrefixSearchWithValue(chars, begin);
	}

	public BaseSearcher getSearcher(String text) {
		return new DynamicCustomDictionary.Searcher(text);
	}

	@Override
	public String toString() {
		return "DynamicCustomDictionary{" + "trie=" + trie + '}';
	}

	/**
	 * 词典中是否含有词语
	 *
	 * @param key 词语
	 * @return 是否包含
	 */
	public boolean contains(String key) {
		if (dat.exactMatchSearch(key) >= 0)
			return true;
		return trie != null && trie.containsKey(key);
	}

	/**
	 * 获取一个BinTrie的查询工具
	 *
	 * @param charArray 文本
	 * @return 查询者
	 */
	public BaseSearcher getSearcher(char[] charArray) {
		return new DynamicCustomDictionary.Searcher(charArray);
	}

	class Searcher extends BaseSearcher<WordAttribute> {
		/**
		 * 分词从何处开始，这是一个状态
		 */
		int begin;

		private LinkedList<Map.Entry<String, WordAttribute>> entryList;

		protected Searcher(char[] c) {
			super(c);
			entryList = new LinkedList<Map.Entry<String, WordAttribute>>();
		}

		protected Searcher(String text) {
			super(text);
			entryList = new LinkedList<Map.Entry<String, WordAttribute>>();
		}

		@Override
		public Map.Entry<String, WordAttribute> next() {
			// 保证首次调用找到一个词语
			while (entryList.size() == 0 && begin < c.length) {
				entryList = trie.commonPrefixSearchWithValue(c, begin);
				++begin;
			}
			// 之后调用仅在缓存用完的时候调用一次
			if (entryList.size() == 0 && begin < c.length) {
				entryList = trie.commonPrefixSearchWithValue(c, begin);
				++begin;
			}
			if (entryList.size() == 0) {
				return null;
			}
			Map.Entry<String, WordAttribute> result = entryList.getFirst();
			entryList.removeFirst();
			offset = begin - 1;
			return result;
		}
	}

	/**
	 * 获取词典对应的trie树
	 *
	 * @return
	 * @deprecated 谨慎操作，有可能废弃此接口
	 */
	public BinTrie<WordAttribute> getTrie() {
		return trie;
	}

	/**
	 * 解析一段文本（目前采用了BinTrie+DAT的混合储存形式，此方法可以统一两个数据结构）
	 *
	 * @param text      文本
	 * @param processor 处理器
	 */
	public void parseText(char[] text, AhoCorasickDoubleArrayTrie.IHit<WordAttribute> processor) {
		if (trie != null) {
			trie.parseText(text, processor);
		}
		DoubleArrayTrie<WordAttribute>.Searcher searcher = dat.getSearcher(text, 0);
		while (searcher.next()) {
			processor.hit(searcher.begin, searcher.begin + searcher.length, searcher.value);
		}
	}

	/**
	 * 解析一段文本（目前采用了BinTrie+DAT的混合储存形式，此方法可以统一两个数据结构）
	 *
	 * @param text      文本
	 * @param processor 处理器
	 */
	public void parseText(String text, AhoCorasickDoubleArrayTrie.IHit<WordAttribute> processor) {
		if (trie != null) {
			BaseSearcher searcher = this.getSearcher(text);
			int offset;
			Map.Entry<String, WordAttribute> entry;
			while ((entry = searcher.next()) != null) {
				offset = searcher.getOffset();
				processor.hit(offset, offset + entry.getKey().length(), entry.getValue());
			}
		}
		DoubleArrayTrie<WordAttribute>.Searcher searcher = dat.getSearcher(text, 0);
		while (searcher.next()) {
			processor.hit(searcher.begin, searcher.begin + searcher.length, searcher.value);
		}
	}

	/**
	 * 最长匹配
	 *
	 * @param text      文本
	 * @param processor 处理器
	 */
	public void parseLongestText(String text, AhoCorasickDoubleArrayTrie.IHit<WordAttribute> processor) {
		if (trie != null) {
			final int[] lengthArray = new int[text.length()];
			final WordAttribute[] attributeArray = new WordAttribute[text.length()];
			char[] charArray = text.toCharArray();
			DoubleArrayTrie<WordAttribute>.Searcher searcher = dat.getSearcher(charArray, 0);
			while (searcher.next()) {
				lengthArray[searcher.begin] = searcher.length;
				attributeArray[searcher.begin] = searcher.value;
			}
			trie.parseText(charArray, new AhoCorasickDoubleArrayTrie.IHit<WordAttribute>() {
				@Override
				public void hit(int begin, int end, WordAttribute value) {
					int length = end - begin;
					if (length > lengthArray[begin]) {
						lengthArray[begin] = length;
						attributeArray[begin] = value;
					}
				}
			});
			for (int i = 0; i < charArray.length;) {
				if (lengthArray[i] == 0) {
					++i;
				} else {
					processor.hit(i, i + lengthArray[i], attributeArray[i]);
					i += lengthArray[i];
				}
			}
		} else
			dat.parseLongestText(text, processor);
	}

	/**
	 * 热更新（重新加载）<br>
	 * 集群环境（或其他IOAdapter）需要自行删除缓存文件（路径 = NlpSetting.CustomDictionaryPath[0] +
	 * Predefine.BIN_EXT）
	 *
	 * @return 是否加载成功
	 */
	public boolean reload() {
		if (path == null || path.length == 0)
			return false;
		IOUtil.deleteFile(path[0] + Predefine.BIN_EXT); // 删掉缓存
		return loadMainDictionary(path[0], normalization);
	}
}
