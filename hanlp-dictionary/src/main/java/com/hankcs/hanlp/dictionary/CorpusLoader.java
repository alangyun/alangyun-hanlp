/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/9/8 22:57</create-date>
 *
 * <copyright file="CorpusLoader.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.dictionary;

import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.word.Document;
import com.hankcs.hanlp.meta.word.IWord;
import com.hankcs.hanlp.meta.word.Sentence;
import com.hankcs.hanlp.meta.word.Word;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * @author hankcs
 */
public class CorpusLoader {
	private static String myName = CorpusLoader.class.getSimpleName();
	
	public static void walk(String folderPath, Handler handler) {
		long start = System.currentTimeMillis();
		List<File> fileList = IOUtil.fileList(folderPath);
		int i = 0;
		for (File file : fileList) {
			Document document = convert2Document(file);
			NlpLogger.info(myName, "{}: {} / {}", file.getAbsolutePath(), ++i, fileList.size());
			handler.handle(document);
		}
		NlpLogger.info(myName, "耗时 {} ms", System.currentTimeMillis() - start);
	}

	public static void walk(String folderPath, HandlerThread[] threadArray) {
		long start = System.currentTimeMillis();
		List<File> fileList = IOUtil.fileList(folderPath);
		for (int i = 0; i < threadArray.length - 1; ++i) {
			threadArray[i].fileList = fileList.subList(fileList.size() / threadArray.length * i, fileList.size() / threadArray.length * (i + 1));
			threadArray[i].start();
		}
		threadArray[threadArray.length - 1].fileList = fileList.subList(fileList.size() / threadArray.length * (threadArray.length - 1), fileList.size());
		threadArray[threadArray.length - 1].start();
		for (HandlerThread handlerThread : threadArray) {
			try {
				handlerThread.join();
			} catch (InterruptedException e) {
				NlpLogger.warning(myName, "多线程异常", e);
			}
		}
		NlpLogger.info(myName, "耗时 {} ms", System.currentTimeMillis() - start);
	}

	public static List<Document> convert2DocumentList(String folderPath) {
		return convert2DocumentList(folderPath, false);
	}

	/**
	 * 读取整个目录中的人民日报格式语料
	 *
	 * @param folderPath 路径
	 * @param verbose
	 * @return
	 */
	public static List<Document> convert2DocumentList(String folderPath, boolean verbose) {
		long start = System.currentTimeMillis();
		List<File> fileList = IOUtil.fileList(folderPath);
		List<Document> documentList = new LinkedList<Document>();
		int i = 0;
		for (File file : fileList) {
			Document document = convert2Document(file);
			documentList.add(document);
			if (verbose)
				NlpLogger.info(myName, "{} / {}: {}", ++i, fileList.size(), file.getAbsolutePath());
		}
		if (verbose)
			NlpLogger.info(myName, "总数：{}, 耗时 {} ms", documentList.size(), System.currentTimeMillis() - start);

		return documentList;
	}

	public static List<Document> loadCorpus(String path) {
		return (List<Document>) IOUtil.readObjectFrom(path);
	}

	public static boolean saveCorpus(List<Document> documentList, String path) {
		return IOUtil.saveObjectTo(documentList, path);
	}

	public static List<List<IWord>> loadSentenceList(String path) {
		return (List<List<IWord>>) IOUtil.readObjectFrom(path);
	}

	public static boolean saveSentenceList(List<List<IWord>> sentenceList, String path) {
		return IOUtil.saveObjectTo(sentenceList, path);
	}

	public static List<List<IWord>> convert2SentenceList(String path) {
		List<Document> documentList = CorpusLoader.convert2DocumentList(path);
		List<List<IWord>> simpleList = new LinkedList<List<IWord>>();
		for (Document document : documentList) {
			for (Sentence sentence : document.sentenceList) {
				simpleList.add(sentence.wordList);
			}
		}

		return simpleList;
	}

	public static List<List<Word>> convert2SimpleSentenceList(String path) {
		List<Document> documentList = CorpusLoader.convert2DocumentList(path);
		List<List<Word>> simpleList = new LinkedList<List<Word>>();
		for (Document document : documentList) {
			simpleList.addAll(document.getSimpleSentenceList());
		}

		return simpleList;
	}

	public static Document convert2Document(File file) {
		NlpLogger.info(myName, "正在载入词条训练文件并转换为内存文档：{}", file.getPath());
		Document document = Document.create(file);
		if (document != null) {
			return document;
		} else {
			throw new IllegalArgumentException(file.getPath() + "读取失败");
		}
	}

	public static interface Handler {
		void handle(Document document);
	}

	/**
	 * 多线程任务
	 */
	public static abstract class HandlerThread extends Thread implements Handler {
		/**
		 * 这个线程负责处理这些事情
		 */
		public List<File> fileList;

		public HandlerThread(String name) {
			super(name);
		}

		@Override
		public void run() {
			long start = System.currentTimeMillis();
			NlpLogger.info(myName, "线程#{} 开始将文件转换为内存文档， 共{}篇", getName(), fileList.size());
			int i = 0;
			for (File file : fileList) {
				Document document = convert2Document(file);
				NlpLogger.debug(myName, "{}: {} / {}" ,file.getAbsoluteFile(),++i,fileList.size());
				handle(document);
			}
			NlpLogger.info(myName, "线程#{} 文件转换为文档完毕，共耗时{}ms", getName(), System.currentTimeMillis() - start);
		}
	}
}
