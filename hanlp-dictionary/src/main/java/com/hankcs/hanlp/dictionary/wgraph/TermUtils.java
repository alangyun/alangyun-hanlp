package com.hankcs.hanlp.dictionary.wgraph;

import java.util.Arrays;

import com.hankcs.hanlp.dictionary.CoreBiGramTableDictionary;
import com.hankcs.hanlp.dictionary.CoreDictionary;
import com.hankcs.hanlp.dictionary.CustomDictionary;
import com.hankcs.hanlp.meta.roletag.Nature;
import com.hankcs.hanlp.meta.word.WordAttribute;

import static com.hankcs.hanlp.utility.Predefine.TOTAL_FREQUENCY;
import static com.hankcs.hanlp.utility.Predefine.lambda;
import static com.hankcs.hanlp.utility.Predefine.myu;

/**
 * 分词辅助类<br/>
 * 将LexiconUtility中涉及到调用CoreDictionary函数的部分迁移过来<br/>
 * com.hankcs.hanlp.seg.SegmentHelper.java
 * @author hoobort
 * @email klxukun@126.com
 * @unit 北京诚朗信息技术有限公司
 * 2022年10月10日 上午11:07:57
 *
 */
public class TermUtils {

    /**
     * 从HanLP的词库中提取某个单词的属性（包括核心词典和用户词典）
     *
     * @param word 单词
     * @return 包含词性与频次的信息
     */
    public static WordAttribute getAttribute(String word)
    {
        WordAttribute attribute = CoreDictionary.get(word);
        if (attribute != null) return attribute;
        return CustomDictionary.get(word);
    }

    /**
     * 词库是否收录了词语（查询核心词典和用户词典）
     * @param word
     * @return
     */
    public static boolean contains(String word)
    {
        return getAttribute(word) != null;
    }

    /**
     * 从HanLP的词库中提取某个单词的属性（包括核心词典和用户词典）
     *
     * @param term 单词
     * @return 包含词性与频次的信息
     */
    public static WordAttribute getAttribute(Term term)
    {
        return getAttribute(term.word);
    }

    /**
     * 获取某个单词的词频
     * @param word
     * @return
     */
    public static int getFrequency(String word)
    {
        WordAttribute attribute = getAttribute(word);
        if (attribute == null) return 0;
        return attribute.totalFrequency;
    }

    /**
     * 设置某个单词的属性
     * @param word
     * @param attribute
     * @return
     */
    public static boolean setAttribute(String word, WordAttribute attribute)
    {
        if (attribute == null) return false;

        if (CoreDictionary.trie.set(word, attribute)) return true;
        if (CustomDictionary.DEFAULT.dat.set(word, attribute)) return true;
        if (CustomDictionary.DEFAULT.trie == null)
        {
            CustomDictionary.add(word);
        }
        CustomDictionary.DEFAULT.trie.put(word, attribute);
        return true;
    }

    /**
     * 设置某个单词的属性
     * @param word
     * @param natures
     * @return
     */
    public static boolean setAttribute(String word, Nature... natures)
    {
        if (natures == null) return false;

        WordAttribute attribute = new WordAttribute(natures, new int[natures.length]);
        Arrays.fill(attribute.frequency, 1);

        return setAttribute(word, attribute);
    }

    /**
     * 设置某个单词的属性
     * @param word
     * @param natures
     * @return
     */
    public static boolean setAttribute(String word, String... natures)
    {
        if (natures == null) return false;

        Nature[] natureArray = new Nature[natures.length];
        for (int i = 0; i < natureArray.length; i++)
        {
            natureArray[i] = Nature.create(natures[i]);
        }

        return setAttribute(word, natureArray);
    }


    /**
     * 设置某个单词的属性
     * @param word
     * @param natureWithFrequency
     * @return
     */
    public static boolean setAttribute(String word, String natureWithFrequency)
    {
        WordAttribute attribute = WordAttribute.create(natureWithFrequency);
        return setAttribute(word, attribute);
    }

    /**
     * 从一个词到另一个词的词的花费
     *
     * @param from 前面的词
     * @param to   后面的词
     * @return 分数
     */
    public static double calculateWeight(Vertex from, Vertex to)
    {
        int fFrom = from.getAttribute().totalFrequency;
        int fBigram = CoreBiGramTableDictionary.getBiFrequency(from.wordID, to.wordID);
        int fTo = to.getAttribute().totalFrequency;
        //        logger.info(String.format("%5s frequency:%6d, %s fBigram:%3d, weight:%.2f", from.word, frequency, from.word + "@" + to.word, fBigram, value));
        return -Math.log(lambda * (myu * fBigram / (fFrom + 1) + 1 - myu) + (1 - lambda) * fTo / TOTAL_FREQUENCY);
    }
}
