package com.hankcs.hanlp.dictionary.maker;

import com.hankcs.hanlp.collection.trie.bintrie.BinTrie;
import com.hankcs.hanlp.config.NlpSetting;
import com.hankcs.hanlp.dictionary.maker.entities.WordLineItem;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.word.IWord;
import com.hankcs.hanlp.meta.word.Word;

import java.io.*;
import java.util.*;

/**
 * 一个通用的词典制作工具，词条格式：词 标签 频次
 * 
 * @author hankcs
 */
public class DictionaryMaker implements ISaveAble {
	private static String myName = DictionaryMaker.class.getSimpleName();
	
	private BinTrie<WordLineItem> trie;

	public DictionaryMaker() {
		trie = new BinTrie<WordLineItem>();
	}

	/**
	 * 向词典中加入一个词语
	 *
	 * @param word 词语
	 */
	public void add(IWord word) {
		WordLineItem item = trie.get(word.getValue());
		if (item == null) {
			item = new WordLineItem(word.getValue(), word.getLabel());
			trie.put(item.key, item);
		} else {
			item.addLabel(word.getLabel());
		}
	}

	public void add(String value, String label) {
		add(new Word(value, label));
	}

	/**
	 * 删除一个词条
	 * 
	 * @param value
	 */
	public void remove(String value) {
		trie.remove(value);
	}

	public WordLineItem get(String key) {
		return trie.get(key);
	}

	public WordLineItem get(IWord word) {
		return get(word.getValue());
	}

	public TreeSet<String> labelSet() {
		TreeSet<String> labelSet = new TreeSet<String>();
		for (Map.Entry<String, WordLineItem> entry : entrySet()) {
			labelSet.addAll(entry.getValue().labelMap.keySet());
		}

		return labelSet;
	}

	/**
	 * 读取所有条目
	 *
	 * @param path
	 * @return
	 */
	public static List<WordLineItem> loadAsItemList(String path) {
		List<WordLineItem> itemList = new LinkedList<WordLineItem>();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(NlpSetting.IOAdapter == null ? new FileInputStream(path) : NlpSetting.IOAdapter.open(path), "UTF-8"));
			String line;
			while ((line = br.readLine()) != null) {
				WordLineItem item = WordLineItem.create(line);
				if (item == null) {
					NlpLogger.warning(myName, "使用[{}]创建Item失败",line);
					return null;
				}
				itemList.add(item);
			}
		} catch (Exception e) {
			NlpLogger.warning("读取词典[{}]发生异常" ,path, e);
			return null;
		}

		return itemList;
	}

	/**
	 * 从磁盘加载
	 * 
	 * @param path
	 * @return
	 */
	public static DictionaryMaker load(String path) {
		DictionaryMaker dictionaryMaker = new DictionaryMaker();
		dictionaryMaker.addAll(DictionaryMaker.loadAsItemList(path));

		return dictionaryMaker;
	}

	/**
	 * 插入全部条目
	 *
	 * @param itemList
	 */
	public void addAll(List<WordLineItem> itemList) {
		for (WordLineItem item : itemList) {
			add(item);
		}
	}

	/**
	 * 插入新条目，不执行合并
	 *
	 * @param itemList
	 */
	public void addAllNotCombine(List<WordLineItem> itemList) {
		for (WordLineItem item : itemList) {
			addNotCombine(item);
		}
	}

	/**
	 * 插入条目
	 *
	 * @param item
	 */
	public void add(WordLineItem item) {
		WordLineItem innerItem = trie.get(item.key);
		if (innerItem == null) {
			innerItem = item;
			trie.put(innerItem.key, innerItem);
		} else {
			innerItem.combine(item);
		}
	}

	/**
	 * 浏览所有词条
	 * 
	 * @return
	 */
	public Set<Map.Entry<String, WordLineItem>> entrySet() {
		return trie.entrySet();
	}

	public Set<String> keySet() {
		return trie.keySet();
	}

	/**
	 * 插入条目，但是不合并，如果已有则忽略
	 *
	 * @param item
	 */
	public void addNotCombine(WordLineItem item) {
		WordLineItem innerItem = trie.get(item.key);
		if (innerItem == null) {
			innerItem = item;
			trie.put(innerItem.key, innerItem);
		}
	}

	/**
	 * 合并两部词典
	 *
	 * @param pathA
	 * @param pathB
	 * @return
	 */
	public static DictionaryMaker combine(String pathA, String pathB) {
		DictionaryMaker dictionaryMaker = new DictionaryMaker();
		dictionaryMaker.addAll(DictionaryMaker.loadAsItemList(pathA));
		dictionaryMaker.addAll(DictionaryMaker.loadAsItemList(pathB));

		return dictionaryMaker;
	}

	/**
	 * 合并多部词典
	 *
	 * @param pathArray
	 * @return
	 */
	public static DictionaryMaker combine(String... pathArray) {
		DictionaryMaker dictionaryMaker = new DictionaryMaker();
		NlpLogger.info(myName, "正在合并词典...");
		for (String path : pathArray) {
			dictionaryMaker.addAll(DictionaryMaker.loadAsItemList(path));
		}
		NlpLogger.info(myName, "合并词典完成");
		
		return dictionaryMaker;
	}

	/**
	 * 对除第一个之外的词典执行标准化，并且合并
	 *
	 * @param pathArray
	 * @return
	 */
	public static DictionaryMaker combineWithNormalization(String[] pathArray) {
		DictionaryMaker dictionaryMaker = new DictionaryMaker();
		NlpLogger.info(myName, "正在处理主词典{}", pathArray[0]);
		dictionaryMaker.addAll(DictionaryMaker.loadAsItemList(pathArray[0]));
		for (int i = 1; i < pathArray.length; ++i) {
			NlpLogger.debug(myName, "正在处理副词典{}，将执行新词合并模式",pathArray[i]);
			dictionaryMaker.addAllNotCombine(DictionaryMaker.loadAsItemList(pathArray[i]));
		}
		return dictionaryMaker;
	}

	/**
	 * 合并，只补充除第一个词典外其他词典的新词
	 *
	 * @param pathArray
	 * @return
	 */
	public static DictionaryMaker combineWhenNotInclude(String[] pathArray) {
		DictionaryMaker dictionaryMaker = new DictionaryMaker();
		NlpLogger.info(myName, "正在处理主词典{}", pathArray[0]);
		dictionaryMaker.addAll(DictionaryMaker.loadAsItemList(pathArray[0]));
		for (int i = 1; i < pathArray.length; ++i) {
			NlpLogger.debug(myName, "正在处理副词典{}，并且过滤已有词典",pathArray[i]);
			dictionaryMaker.addAllNotCombine(DictionaryMaker.normalizeFrequency(DictionaryMaker.loadAsItemList(pathArray[i])));
		}
		return dictionaryMaker;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("词条数量：");
		sb.append(trie.size());
		return sb.toString();
	}

	@Override
	public boolean saveTxtTo(String path) {
		if (trie.size() == 0)
			return true; // 如果没有词条，那也算成功了
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(IOUtil.newOutputStream(path), "UTF-8"));
			Set<Map.Entry<String, WordLineItem>> entries = trie.entrySet();
			for (Map.Entry<String, WordLineItem> entry : entries) {
				bw.write(entry.getValue().toString());
				bw.newLine();
			}
			bw.close();
		} catch (Exception e) {
			NlpLogger.warning(myName, "保存到[{}]失败" ,path, e);
			return false;
		}

		return true;
	}

	public void add(String param) {
		WordLineItem item = WordLineItem.create(param);
		if (item != null)
			add(item);
	}

	public static interface Filter {
		/**
		 * 是否保存这个条目
		 * 
		 * @param item
		 * @return true表示保存
		 */
		boolean onSave(WordLineItem item);
	}

	/**
	 * 允许保存之前对其做一些调整
	 *
	 * @param path
	 * @param filter
	 * @return
	 */
	public boolean saveTxtTo(String path, Filter filter) {
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(IOUtil.newOutputStream(path), "UTF-8"));
			Set<Map.Entry<String, WordLineItem>> entries = trie.entrySet();
			for (Map.Entry<String, WordLineItem> entry : entries) {
				if (filter.onSave(entry.getValue())) {
					bw.write(entry.getValue().toString());
					bw.newLine();
				}
			}
			bw.close();
		} catch (Exception e) {
			NlpLogger.warning(myName, "保存到[{}]失败" ,path, e);
			return false;
		}

		return true;
	}

	/**
	 * 调整频次，按排序后的次序给定频次
	 *
	 * @param itemList
	 * @return 处理后的列表
	 */
	public static List<WordLineItem> normalizeFrequency(List<WordLineItem> itemList) {
		for (WordLineItem item : itemList) {
			ArrayList<Map.Entry<String, Integer>> entryArray = new ArrayList<Map.Entry<String, Integer>>(item.labelMap.entrySet());
			Collections.sort(entryArray, new Comparator<Map.Entry<String, Integer>>() {
				@Override
				public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
					return o1.getValue().compareTo(o2.getValue());
				}
			});
			int index = 1;
			for (Map.Entry<String, Integer> pair : entryArray) {
				item.labelMap.put(pair.getKey(), index);
				++index;
			}
		}
		return itemList;
	}
}
