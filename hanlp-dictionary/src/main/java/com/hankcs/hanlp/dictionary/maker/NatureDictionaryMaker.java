/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/9/18 19:47</create-date>
 *
 * <copyright file="NatureDictionaryMaker.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.dictionary.maker;

import com.hankcs.hanlp.dictionary.CorpusLoader;
import com.hankcs.hanlp.dictionary.util.Precompiler;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.roletag.Nature;
import com.hankcs.hanlp.meta.word.Document;
import com.hankcs.hanlp.meta.word.IWord;
import com.hankcs.hanlp.meta.word.Word;
import com.hankcs.hanlp.utility.TextUtility;
import com.hankcs.hanlp.utility.Predefine;

import java.util.LinkedList;
import java.util.List;

/**
 * @author hankcs
 */
public class NatureDictionaryMaker extends CommonDictionaryMaker {
	private static String myName = NatureDictionaryMaker.class.getSimpleName();

	public NatureDictionaryMaker() {
		super(null);
	}

	@Override
	protected void addToDictionary(List<List<IWord>> sentenceList) {
		NlpLogger.info(myName, "正在构造词典结构");
		// 制作NGram词典
		int total = 0;
		for (List<IWord> wordList : sentenceList) {
			IWord pre = null;
			for (IWord word : wordList) {
				// 制作词性词频词典
				dictionaryMaker.add(word);
				if (pre != null)
					nGramDictionaryMaker.addPair(pre, word);

				pre = word;

				total++;
			}
		}
		NlpLogger.info(myName, "构造词典完成！共{}词汇", total);
	}

	@Override
	protected void roleTag(List<List<IWord>> sentenceList) {
		NlpLogger.info(myName, "开始标注词性");
		int i = 0;
		int total = sentenceList.size();
		int times = total < 100 ? 100 : (int) Math.ceil(total / 100);
		for (List<IWord> wordList : sentenceList) {
			if (NlpLogger.enableDebug()) {
				if (++i % times == 0 || i == total)
					NlpLogger.debug(myName, "已完成标注 {} / {}", i, total);
			}

			for (IWord word : wordList)
				Precompiler.compile(word); // 编译为等效字符串

			LinkedList<IWord> wordLinkedList = (LinkedList<IWord>) wordList;
			wordLinkedList.addFirst(new Word(Predefine.TAG_BIGIN, Nature.begin.toString()));
			wordLinkedList.addLast(new Word(Predefine.TAG_END, Nature.end.toString()));
		}
		NlpLogger.info(myName, "词性标注完成！");
	}

	/**
	 * 指定语料库文件夹，制作一份词频词典
	 * 
	 * @return
	 */
	static boolean makeCoreDictionary(String inPath, String outPath) {
		final DictionaryMaker dictionaryMaker = new DictionaryMaker();

		CorpusLoader.walk(inPath, new CorpusLoader.Handler() {
			@Override
			public void handle(Document document) {
				for (List<Word> sentence : document.getSimpleSentenceList(true)) {
					for (Word word : sentence) {
						if (shouldInclude(word))
							dictionaryMaker.add(word);
					}
				}
			}

			/**
			 * 是否应当计算这个词语
			 * 
			 * @param word
			 * @return
			 */
			boolean shouldInclude(Word word) {
				if ("m".equals(word.label) || "mq".equals(word.label) || "w".equals(word.label) || "t".equals(word.label)) {
					if (!TextUtility.isAllChinese(word.value))
						return false;
				} else if ("nr".equals(word.label)) {
					return false;
				}

				return true;
			}
		});
		if (outPath != null)
			return dictionaryMaker.saveTxtTo(outPath);

		return false;
	}
}
