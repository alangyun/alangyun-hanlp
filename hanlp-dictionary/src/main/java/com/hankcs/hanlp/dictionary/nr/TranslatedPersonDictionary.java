/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/11/12 14:45</create-date>
 *
 * <copyright file="TranslatedPersonDictionary.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.dictionary.nr;

import com.hankcs.hanlp.collection.trie.DoubleArrayTrie;
import com.hankcs.hanlp.config.NlpSetting;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.utility.Predefine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

/**
 * 翻译人名词典，储存和识别翻译人名
 * 
 * @author hankcs
 */
public class TranslatedPersonDictionary {
	private static String myName = TranslatedPersonDictionary.class.getSimpleName();
	
	private static String path = NlpSetting.TranslatedPersonDictionaryPath;
	private static DoubleArrayTrie<Boolean> trie;

	static {
		long start = System.currentTimeMillis();
		if (!load()) {
			throw new IllegalArgumentException("音译人名词典" + path + "加载失败");
		}

		NlpLogger.info(myName, "音译人名词典“{}”加载成功，耗时{}ms", path, (System.currentTimeMillis() - start));
	}

	static boolean load() {
		trie = new DoubleArrayTrie<Boolean>();
		if (loadDat())
			return true;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(path), "UTF-8"));
			String line;
			TreeMap<String, Boolean> map = new TreeMap<String, Boolean>();
			TreeMap<Character, Integer> charFrequencyMap = new TreeMap<Character, Integer>();
			while ((line = br.readLine()) != null) {
				map.put(line, true);
				// 音译人名常用字词典自动生成
				for (char c : line.toCharArray()) {
					// 排除一些过于常用的字
					if ("不赞".indexOf(c) >= 0)
						continue;
					Integer f = charFrequencyMap.get(c);
					if (f == null)
						f = 0;
					charFrequencyMap.put(c, f + 1);
				}
			}
			br.close();
			map.put(String.valueOf('·'), true);
//            map.put(String.valueOf('-'), true);
//            map.put(String.valueOf('—'), true);
			// 将常用字也加进去
			for (Map.Entry<Character, Integer> entry : charFrequencyMap.entrySet()) {
				if (entry.getValue() < 10)
					continue;
				map.put(String.valueOf(entry.getKey()), true);
			}
			NlpLogger.info(myName, "音译人名词典“{}”开始构建双数组……", path);
			trie.build(map);
//			NlpLogger.info(myName, "音译人名词典“{}”开始编译DAT文件……", path);
			NlpLogger.info(myName, "编译音译人名词典“{}”编译结果：{}", path, saveDat());
		} catch (Exception e) {
			NlpLogger.error(myName, "自定义词典“{}”读取错误！", path, e);
			return false;
		}

		return true;
	}

	/**
	 * 保存dat到磁盘
	 * 
	 * @return
	 */
	static boolean saveDat() {
		return trie.save(path + Predefine.TRIE_EXT);
	}

	static boolean loadDat() {
		return trie.load(path + Predefine.TRIE_EXT);
	}

	/**
	 * 是否包含key
	 * 
	 * @param key
	 * @return
	 */
	public static boolean containsKey(String key) {
		return trie.containsKey(key);
	}

	/**
	 * 时报包含key，且key至少长length
	 * 
	 * @param key
	 * @param length
	 * @return
	 */
	public static boolean containsKey(String key, int length) {
		if (!trie.containsKey(key))
			return false;
		return key.length() >= length;
	}
}
