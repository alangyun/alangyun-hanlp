/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/10/8 1:05</create-date>
 *
 * <copyright file="Occurrence.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.dictionary.occurrence;

import com.hankcs.hanlp.collection.trie.bintrie.BinTrie;
import com.hankcs.hanlp.meta.TermFrequency;
import com.hankcs.hanlp.utility.Predefine;

import java.util.*;

/**
 * 词共现统计，最多统计到三阶共现
 *
 * @author hankcs, hoobort
 * @log 2022.10.10<br/>
 *      addAll(List<Term> resultList)和addAll(String
 *      text)两个函数未被使用，将其移除，消除了与tokenizer和seg两个包的绑定<br/>
 *      修改了compute方法，增加了回调接口ITermFrequency参数，通过该参数实现通过外部来实现获取给定词的词频，从而将该类于dictionary包的解绑<br/>
 *      <br/>
 */
public class Occurrence {
	/** 两个词的正向连接符 中国 RIGHT 人民 */
	public static final char RIGHT = '\u0000';
	/** 两个词的逆向连接符 人民 LEFT 中国 */
	static final char LEFT = '\u0001';
	/** 全部单词数量 */
	double totalTerm;
	/** 全部接续数量，包含正向和逆向 */
	double totalPair;
	/** 2 gram的pair */
	BinTrie<PairFrequency> triePair;
	/** 词频统计用的储存结构 */
	BinTrie<TermFrequency> trieSingle;
	/** 三阶储存结构 */
	BinTrie<TriaFrequency> trieTria;
	/** 软缓存一个pair的setset */
	private Set<Map.Entry<String, PairFrequency>> entrySetPair;

	public Occurrence() {
		triePair = new BinTrie<PairFrequency>();
		trieSingle = new BinTrie<TermFrequency>();
		trieTria = new BinTrie<TriaFrequency>();
		totalTerm = totalPair = 0;
	}

	/**
	 * 添加一个共现
	 *
	 * @param first  第一个词
	 * @param second 第二个词
	 */
	public void addPair(String first, String second) {
		addPair(first, RIGHT, second);
	}

	/**
	 * 统计词频
	 *
	 * @param key 增加一个词
	 */
	public void addTerm(String key) {
		TermFrequency value = trieSingle.get(key);
		if (value == null) {
			value = new TermFrequency(key);
			trieSingle.put(key, value);
		} else {
			value.increase();
		}
		++totalTerm;
	}

	private void addPair(String first, char delimiter, String second) {
		String key = first + delimiter + second;
		PairFrequency value = triePair.get(key);
		if (value == null) {
			value = PairFrequency.create(first, delimiter, second);
			triePair.put(key, value);
		} else {
			value.increase();
		}
		++totalPair;
	}

	public void addTria(String first, String second, String third) {
		String key = first + RIGHT + second + RIGHT + third;
		TriaFrequency value = trieTria.get(key);
		if (value == null) {
			value = TriaFrequency.create(first, RIGHT, second, third);
			trieTria.put(key, value);
		} else {
			value.increase();
		}
		key = second + RIGHT + third + LEFT + first; // 其实两个key只有最后一个连接符方向不同
		value = trieTria.get(key);
		if (value == null) {
			value = TriaFrequency.create(second, third, LEFT, first);
			trieTria.put(key, value);
		} else {
			value.increase();
		}
	}

	/**
	 * 获取词频
	 *
	 * @param term
	 * @return
	 */
	public int getTermFrequency(String term) {
		TermFrequency termFrequency = trieSingle.get(term);
		if (termFrequency == null)
			return 0;
		return termFrequency.getValue();
	}

	public int getPairFrequency(String first, String second) {
		TermFrequency termFrequency = triePair.get(first + RIGHT + second);
		if (termFrequency == null)
			return 0;
		return termFrequency.getValue();
	}

	public void addAll(String[] termList) {
		for (String term : termList) {
			addTerm(term);
		}

		String first = null;
		for (String current : termList) {
			if (first != null) {
				addPair(first, current);
			}
			first = current;
		}
		for (int i = 2; i < termList.length; ++i) {
			addTria(termList[i - 2], termList[i - 1], termList[i]);
		}
	}

	public List<PairFrequency> getPhraseByMi() {
		List<PairFrequency> pairFrequencyList = new ArrayList<PairFrequency>(entrySetPair.size());
		for (Map.Entry<String, PairFrequency> entry : entrySetPair) {
			pairFrequencyList.add(entry.getValue());
		}
		Collections.sort(pairFrequencyList, new Comparator<PairFrequency>() {
			@Override
			public int compare(PairFrequency o1, PairFrequency o2) {
				return -Double.compare(o1.mi, o2.mi);
			}
		});
		return pairFrequencyList;
	}

	public List<PairFrequency> getPhraseByLe() {
		List<PairFrequency> pairFrequencyList = new ArrayList<PairFrequency>(entrySetPair.size());
		for (Map.Entry<String, PairFrequency> entry : entrySetPair) {
			pairFrequencyList.add(entry.getValue());
		}
		Collections.sort(pairFrequencyList, new Comparator<PairFrequency>() {
			@Override
			public int compare(PairFrequency o1, PairFrequency o2) {
				return -Double.compare(o1.le, o2.le);
			}
		});
		return pairFrequencyList;
	}

	public List<PairFrequency> getPhraseByRe() {
		List<PairFrequency> pairFrequencyList = new ArrayList<PairFrequency>(entrySetPair.size());
		for (Map.Entry<String, PairFrequency> entry : entrySetPair) {
			pairFrequencyList.add(entry.getValue());
		}
		Collections.sort(pairFrequencyList, new Comparator<PairFrequency>() {
			@Override
			public int compare(PairFrequency o1, PairFrequency o2) {
				return -Double.compare(o1.re, o2.re);
			}
		});
		return pairFrequencyList;
	}

	public List<PairFrequency> getPhraseByScore() {
		List<PairFrequency> pairFrequencyList = new ArrayList<PairFrequency>(entrySetPair.size());
		for (Map.Entry<String, PairFrequency> entry : entrySetPair) {
			pairFrequencyList.add(entry.getValue());
		}
		Collections.sort(pairFrequencyList, new Comparator<PairFrequency>() {
			@Override
			public int compare(PairFrequency o1, PairFrequency o2) {
				return -Double.compare(o1.score, o2.score);
			}
		});
		return pairFrequencyList;
	}

//    public void addAll(List<Term> resultList)
//    {
//        String[] termList = new String[resultList.size()];
//        int i = 0;
//        for (Term word : resultList)
//        {
//            termList[i] = word.word;
//            ++i;
//        }
//        addAll(termList);
//    }

//    public void addAll(String text)
//    {
//        addAll(NotionalTokenizer.segment(text));
//    }

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("二阶共现：\n");
		for (Map.Entry<String, PairFrequency> entry : triePair.entrySet()) {
			sb.append(entry.getValue()).append('\n');
		}
		sb.append("三阶共现：\n");
		for (Map.Entry<String, TriaFrequency> entry : trieTria.entrySet()) {
			sb.append(entry.getValue()).append('\n');
		}
		return sb.toString();
	}

	public double computeMutualInformation(String first, String second) {
		return Math.log(Math.max(Predefine.MIN_PROBABILITY, getPairFrequency(first, second) / (totalPair / 2))
				/ Math.max(Predefine.MIN_PROBABILITY, (getTermFrequency(first) / totalTerm * getTermFrequency(second) / totalTerm)));
	}

	public double computeMutualInformation(PairFrequency pair, int firsFreq, int secondFreq) {
		return Math.log(Math.max(Predefine.MIN_PROBABILITY, pair.getValue() / totalPair)
				/ Math.max(Predefine.MIN_PROBABILITY, (firsFreq / (double) Predefine.TOTAL_FREQUENCY * secondFreq / (double) Predefine.TOTAL_FREQUENCY)));
	}

	/**
	 * 计算左熵
	 *
	 * @param pair
	 * @return
	 */
	public double computeLeftEntropy(PairFrequency pair) {
		Set<Map.Entry<String, TriaFrequency>> entrySet = trieTria.prefixSearch(pair.getKey() + LEFT);
		return computeEntropy(entrySet);
	}

	/**
	 * 计算右熵
	 *
	 * @param pair
	 * @return
	 */
	public double computeRightEntropy(PairFrequency pair) {
		Set<Map.Entry<String, TriaFrequency>> entrySet = trieTria.prefixSearch(pair.getKey() + RIGHT);
		return computeEntropy(entrySet);
	}

	private double computeEntropy(Set<Map.Entry<String, TriaFrequency>> entrySet) {
		double totalFrequency = 0;
		for (Map.Entry<String, TriaFrequency> entry : entrySet) {
			totalFrequency += entry.getValue().getValue();
		}
		double le = 0;
		for (Map.Entry<String, TriaFrequency> entry : entrySet) {
			double p = entry.getValue().getValue() / totalFrequency;
			le += -p * Math.log(p);
		}
		return le;
	}

	/**
	 * 输入数据完毕，执行计算
	 */
	public void compute(ITermFrequency callback) {
		entrySetPair = triePair.entrySet();
		double total_mi = 0;
		double total_le = 0;
		double total_re = 0;
		for (Map.Entry<String, PairFrequency> entry : entrySetPair) {
			PairFrequency value = entry.getValue();
			value.mi = computeMutualInformation(value, callback.termFrequency(value.first), callback.termFrequency(value.second));
			value.le = computeLeftEntropy(value);
			value.re = computeRightEntropy(value);
			total_mi += value.mi;
			total_le += value.le;
			total_re += value.re;
		}

		for (Map.Entry<String, PairFrequency> entry : entrySetPair) {
			PairFrequency value = entry.getValue();
			value.score = safeDivide(value.mi, total_mi) + safeDivide(value.le, total_le) + safeDivide(value.re, total_re); // 归一化
			value.score *= entrySetPair.size();
		}
	}

	private static double safeDivide(double x, double y) {
		if (y == 0)
			return 0;
		return x / y;
	}

	/**
	 * 获取一阶共现,其实就是词频统计
	 * 
	 * @return
	 */
	public Set<Map.Entry<String, TermFrequency>> getUniGram() {
		return trieSingle.entrySet();
	}

	/**
	 * 获取二阶共现
	 * 
	 * @return
	 */
	public Set<Map.Entry<String, PairFrequency>> getBiGram() {
		return triePair.entrySet();
	}

	/**
	 * 获取三阶共现
	 * 
	 * @return
	 */
	public Set<Map.Entry<String, TriaFrequency>> getTriGram() {
		return trieTria.entrySet();
	}

}
