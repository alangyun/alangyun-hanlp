/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/12/23 20:07</create-date>
 *
 * <copyright file="CoreDictionaryACDAT.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.dictionary;

import com.hankcs.hanlp.config.NlpSetting;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.TreeMap;

import com.hankcs.hanlp.collection.array.ByteArray;
import com.hankcs.hanlp.collection.trie.DoubleArrayTrie;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.roletag.Nature;
import com.hankcs.hanlp.meta.word.WordAttribute;
import com.hankcs.hanlp.utility.Predefine;

/**
 * 使用DoubleArrayTrie实现的核心词典
 *
 * @author hankcs, hoobort
 * @log hoobort于2022.10.10将原来内置的Attribute类替换为<a href=
 *      "com.hankcs.hanlp.meta.word.WordAttribute">WordAttribute</a>
 */
public class CoreDictionary {
	private static String myName = CoreDictionary.class.getSimpleName();

	public static DoubleArrayTrie<WordAttribute> trie = new DoubleArrayTrie<WordAttribute>();
	public final static String path = NlpSetting.CoreDictionaryPath;

	// 自动加载词典
	static {
		long start = System.currentTimeMillis();
		if (!load(path)) {
			throw new IllegalArgumentException("核心词典" + path + "加载失败");
		} else {
			NlpLogger.info(myName, "{}加载成功，{}个词条，耗时{}ms", path, trie.size(), (System.currentTimeMillis() - start));
		}
	}

	// 一些特殊的WORD_ID
	public static final int NR_WORD_ID = getWordID(Predefine.TAG_PEOPLE);
	public static final int NS_WORD_ID = getWordID(Predefine.TAG_PLACE);
	public static final int NT_WORD_ID = getWordID(Predefine.TAG_GROUP);
	public static final int T_WORD_ID = getWordID(Predefine.TAG_TIME);
	public static final int X_WORD_ID = getWordID(Predefine.TAG_CLUSTER);
	public static final int M_WORD_ID = getWordID(Predefine.TAG_NUMBER);
	public static final int NX_WORD_ID = getWordID(Predefine.TAG_PROPER);

	private static boolean load(String path) {
		NlpLogger.info(myName, "核心词典开始加载:{}", path);
		if (loadDat(path))
			return true;
		TreeMap<String, WordAttribute> map = new TreeMap<>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(path), "UTF-8"));
			String line;
			int totalFrequency = 0;
			long start = System.currentTimeMillis();
			while ((line = br.readLine()) != null) {
				String param[] = line.split("\\s");
				int natureCount = (param.length - 1) / 2;
				WordAttribute attribute = new WordAttribute(natureCount);
				for (int i = 0; i < natureCount; ++i) {
					attribute.nature[i] = Nature.create(param[1 + 2 * i]);
					attribute.frequency[i] = Integer.parseInt(param[2 + 2 * i]);
					attribute.totalFrequency += attribute.frequency[i];
				}
				map.put(param[0], attribute);
				totalFrequency += attribute.totalFrequency;
			}
			NlpLogger.info(myName, "核心词典读入词条{} 全部频次{}，耗时{}ms", map.size(), totalFrequency, (System.currentTimeMillis() - start));
			br.close();
			trie.build(map);
			NlpLogger.info(myName, "核心词典加载成功:{}个词条，下面将写入缓存……", trie.size());
			try {
				DataOutputStream out = new DataOutputStream(new BufferedOutputStream(IOUtil.newOutputStream(path + Predefine.BIN_EXT)));
				Collection<WordAttribute> attributeList = map.values();
				out.writeInt(attributeList.size());
				for (WordAttribute attribute : attributeList) {
					out.writeInt(attribute.totalFrequency);
					out.writeInt(attribute.nature.length);
					for (int i = 0; i < attribute.nature.length; ++i) {
						out.writeInt(attribute.nature[i].ordinal());
						out.writeInt(attribute.frequency[i]);
					}
				}
				trie.save(out);
				out.writeInt(totalFrequency);
				Predefine.setTotalFrequency(totalFrequency);
				out.close();
			} catch (Exception e) {
				NlpLogger.warning(myName, "保存失败", e);
				return false;
			}
		} catch (FileNotFoundException e) {
			NlpLogger.warning(myName, myName, "核心词典{}不存在！", path, e);
			return false;
		} catch (IOException e) {
			NlpLogger.warning(myName, "核心词典{}读取错误！", path, e);
			return false;
		}

		return true;
	}

	/**
	 * 从磁盘加载双数组
	 *
	 * @param path
	 * @return
	 */
	static boolean loadDat(String path) {
		try {
			ByteArray byteArray = ByteArray.createByteArray(path + Predefine.BIN_EXT);
			if (byteArray == null)
				return false;
			int size = byteArray.nextInt();
			WordAttribute[] attributes = new WordAttribute[size];
			final Nature[] natureIndexArray = Nature.values();
			for (int i = 0; i < size; ++i) {
				// 第一个是全部频次，第二个是词性个数
				int currentTotalFrequency = byteArray.nextInt();
				int length = byteArray.nextInt();
				attributes[i] = new WordAttribute(length);
				attributes[i].totalFrequency = currentTotalFrequency;
				for (int j = 0; j < length; ++j) {
					attributes[i].nature[j] = natureIndexArray[byteArray.nextInt()];
					attributes[i].frequency[j] = byteArray.nextInt();
				}
			}
			if (!trie.load(byteArray, attributes))
				return false;
			int totalFrequency = 0;
			if (byteArray.hasMore()) // 自从1.8.2起，ngram模型最后一个整型为总词频
			{
				totalFrequency = byteArray.nextInt();
			} else {
				for (WordAttribute attribute : attributes) {
					totalFrequency += attribute.totalFrequency;
				}
			}
			Predefine.setTotalFrequency(totalFrequency);
		} catch (Exception e) {
			NlpLogger.warning(myName, "读取失败", e);
			return false;
		}
		return true;
	}

	/**
	 * 获取条目
	 *
	 * @param key
	 * @return
	 */
	public static WordAttribute get(String key) {
		return trie.get(key);
	}

	/**
	 * 获取条目
	 *
	 * @param wordID
	 * @return
	 */
	public static WordAttribute get(int wordID) {
		return trie.get(wordID);
	}

	/**
	 * 获取词频
	 *
	 * @param term
	 * @return
	 */
	public static int getTermFrequency(String term) {
		WordAttribute attribute = get(term);
		if (attribute == null)
			return 0;
		return attribute.totalFrequency;
	}

	/**
	 * 是否包含词语
	 *
	 * @param key
	 * @return
	 */
	public static boolean contains(String key) {
		return trie.get(key) != null;
	}

	/**
	 * 获取词语的ID
	 *
	 * @param a 词语
	 * @return ID, 如果不存在, 则返回-1
	 */
	public static int getWordID(String a) {
		return CoreDictionary.trie.exactMatchSearch(a);
	}

	/**
	 * 热更新核心词典<br>
	 * 集群环境（或其他IOAdapter）需要自行删除缓存文件
	 *
	 * @return 是否成功
	 */
	public static boolean reload() {
		String path = CoreDictionary.path;
		IOUtil.deleteFile(path + Predefine.BIN_EXT);

		return load(path);
	}
}
