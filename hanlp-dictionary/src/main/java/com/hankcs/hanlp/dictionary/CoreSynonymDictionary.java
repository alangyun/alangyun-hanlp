/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/9/13 13:12</create-date>
 *
 * <copyright file="CoreSynonymDictionary.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.dictionary;

import com.hankcs.hanlp.config.NlpSetting;
import com.hankcs.hanlp.dictionary.common.CommonSynonymDictionary;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;

import java.util.List;

/**
 * 核心同义词词典
 *
 * @author hankcs， hoobort
 * @log 2022.10.10： hoobort createSynonymList(List<Term> sentence, boolean
 *      withUndefinedItem)未使用，解除与seg包的捆绑
 */
public class CoreSynonymDictionary {
	private static String myName = CoreSynonymDictionary.class.getSimpleName();
	
	static CommonSynonymDictionary dictionary;

	static {
		try {
			long start = System.currentTimeMillis();
			dictionary = CommonSynonymDictionary.create(IOUtil.newInputStream(NlpSetting.CoreSynonymDictionaryDictionaryPath));
			NlpLogger.info(myName, "载入核心同义词词典成功，耗时 {}ms", (System.currentTimeMillis() - start));
		} catch (Exception e) {
			throw new IllegalArgumentException("载入核心同义词词典失败" + e);
		}
	}

	/**
	 * 获取一个词的同义词（意义完全相同的，即{@link com.hankcs.hanlp.dictionary.common.CommonSynonymDictionary.SynonymItem#type}
	 * == {@link com.hankcs.hanlp.dictionary.synonym.Synonym.Type#EQUAL}的）列表
	 * 
	 * @param key
	 * @return
	 */
	public static CommonSynonymDictionary.SynonymItem get(String key) {
		return dictionary.get(key);
	}

	/**
	 * 不分词直接转换
	 * 
	 * @param text
	 * @return
	 */
	public static String rewriteQuickly(String text) {
		return dictionary.rewriteQuickly(text);
	}

//	/**
//	 * 重写字典<br/>
//	 * hoobort将其于2022.10.14将其屏蔽，目的是将包之间的关联性松偶,原因：<br/>
//	 * 字典应该是提供给其他应用包使用，而不是和应用包混合<br/>
//	 *
//	 * hoobort
//	 * 2022年10月14日 下午1:18:59
//	 * @param text
//	 * @return
//	 * @deprecated
//	 */
//	public static String rewrite(String text) {
//		return dictionary.rewrite(text);
//	}

	/**
	 * 语义距离
	 * 
	 * @param itemA
	 * @param itemB
	 * @return
	 */
	public static long distance(CommonSynonymDictionary.SynonymItem itemA, CommonSynonymDictionary.SynonymItem itemB) {
		return itemA.distance(itemB);
	}

	/**
	 * 判断两个单词之间的语义距离
	 * 
	 * @param A
	 * @param B
	 * @return
	 */
	public static long distance(String A, String B) {
		CommonSynonymDictionary.SynonymItem itemA = get(A);
		CommonSynonymDictionary.SynonymItem itemB = get(B);
		if (itemA == null || itemB == null)
			return Long.MAX_VALUE;

		return distance(itemA, itemB);
	}

	/**
	 * 计算两个单词之间的相似度，0表示不相似，1表示完全相似
	 * 
	 * @param A
	 * @param B
	 * @return
	 */
	public static double similarity(String A, String B) {
		long distance = distance(A, B);
		if (distance > dictionary.getMaxSynonymItemIdDistance())
			return 0.0;

		return (dictionary.getMaxSynonymItemIdDistance() - distance) / (double) dictionary.getMaxSynonymItemIdDistance();
	}

	/**
	 * 获取语义标签
	 * 
	 * @return
	 */
	public static long[] getLexemeArray(List<CommonSynonymDictionary.SynonymItem> synonymItemList) {
		long[] array = new long[synonymItemList.size()];
		int i = 0;
		for (CommonSynonymDictionary.SynonymItem item : synonymItemList) {
			array[i++] = item.entry.id;
		}
		return array;
	}

}
