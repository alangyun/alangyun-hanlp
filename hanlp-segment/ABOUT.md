# 自然语言理解

### 来源地址
https://github.com/hankcs/HanLP/tree/1.x

### 版本号
1.8.3

### 修订时间
源码下载时间：2022.06.04

源码提交时间：2022.02.21

### 修订记录
| 时间 | 修订内容 |
| -- | -- |
| 2022.06.14 | 1. 为NaiveBayesModel添加了serialVersionUID<br/>2.为AbstractModel添加了serialVersionUID<br/>3.为BinTrie添加了serialVersionUID |
