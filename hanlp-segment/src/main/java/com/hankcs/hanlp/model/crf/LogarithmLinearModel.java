/*
 * <author>Han He</author>
 * <email>me@hankcs.com</email>
 * <create-date>2018-06-28 7:37 PM</create-date>
 *
 * <copyright file="LogLinearModel.java">
 * Copyright (c) 2018, Han He. All Rights Reserved, http://www.hankcs.com/
 * This source is subject to Han He. Please contact Han He for more information.
 * </copyright>
 */
package com.hankcs.hanlp.model.crf;

import com.hankcs.hanlp.collection.array.ByteArray;
import com.hankcs.hanlp.config.NlpSetting;
import com.hankcs.hanlp.io.FileIOAdapter;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.tagger.TaggedType;
import com.hankcs.hanlp.meta.tagger.tag.CWSTagSet;
import com.hankcs.hanlp.meta.tagger.tag.NERTagSet;
import com.hankcs.hanlp.meta.tagger.tag.TagSet;
import com.hankcs.hanlp.model.feature.FeatureMap;
import com.hankcs.hanlp.model.feature.MutableFeatureMap;
import com.hankcs.hanlp.model.linear.LinearModel;
import com.hankcs.hanlp.utility.Predefine;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.*;

import static com.hankcs.hanlp.utility.Predefine.BIN_EXT;

/**
 * 对数线性模型形式的CRF模型
 *
 * @author hankcs, hoobort
 */
public class LogarithmLinearModel extends LinearModel {
	private static String myName=LogarithmLinearModel.class.getSimpleName();
	
	/** 特征模板 */
	private FeatureTemplate[] featureTemplateArray;

	private LogarithmLinearModel(FeatureMap featureMap, float[] parameter) {
		super(featureMap, parameter);
	}

	private LogarithmLinearModel(FeatureMap featureMap) {
		super(featureMap);
	}

	/**
	 * 加载CRF模型
	 *
	 * @param modelFile HanLP的.bin格式，或CRF++的.txt格式（将会自动转换为model.txt.bin，下次会直接加载.txt.bin）
	 * @throws IOException
	 */
	public LogarithmLinearModel(String modelFile) throws IOException {
		super(null, null);
		if (modelFile.endsWith(BIN_EXT)) {
			load(modelFile); // model.bin
			return;
		}
		String binPath = modelFile + Predefine.BIN_EXT;

		if (!((NlpSetting.IOAdapter == null || NlpSetting.IOAdapter instanceof FileIOAdapter) && !IOUtil.isFileExisted(binPath))) {
			try {
				load(binPath); // model.txt -> model.bin
				return;
			} catch (Exception e) {
				// ignore
			}
		}

		convert(modelFile, binPath);
	}

	/**
	 * 加载txt，转换为bin
	 *
	 * @param txtFile txt
	 * @param binFile bin
	 * @throws IOException
	 */
	public LogarithmLinearModel(String txtFile, String binFile) throws IOException {
		super(null, null);
		convert(txtFile, binFile);
	}

	@Override
	public boolean load(ByteArray byteArray) {
		if (!super.load(byteArray))
			return false;
		
		int size = byteArray.nextInt();
		featureTemplateArray = new FeatureTemplate[size];
		for (int i = 0; i < size; ++i) {
			FeatureTemplate featureTemplate = new FeatureTemplate();
			featureTemplate.load(byteArray);
			featureTemplateArray[i] = featureTemplate;
		}
		
		if (!byteArray.hasMore())
			byteArray.close();
		
		return true;
	}

	private void convert(String txtFile, String binFile) throws IOException {
		TagSet tagSet = new TagSet(TaggedType.CLASSIFICATION);
		IOUtil.LineIterator lineIterator = new IOUtil.LineIterator(txtFile);
		if (!lineIterator.hasNext())
			throw new IOException("空白文件");

		//以下目的为移位(存储的文件的格式如此）
		NlpLogger.info(myName, lineIterator.next());   // verson
		NlpLogger.info(myName, lineIterator.next());   // cost-factor
		NlpLogger.info(myName, lineIterator.next());   // maxid
        NlpLogger.info(myName, lineIterator.next());   // xsize
        lineIterator.next();    				// blank
		//以上目的为移位
        
        //将内容按行读取并放置到标注集合中
		String line;
		while ((line = lineIterator.next()).length() != 0)
			tagSet.add(line);
		//根据标注类型，生成标注集实例
		tagSet.setTaskType(guessModelType(tagSet));
		switch (tagSet.taskType()) {
		case CWS:
			tagSet = new CWSTagSet(tagSet.indexByTag("B"), tagSet.indexByTag("M"), tagSet.indexByTag("E"), tagSet.indexByTag("S"));
			break;
		case NER:
			tagSet = new NERTagSet(tagSet.indexByTag("O"), tagSet.tags());
			break;
		default:
			break;
		}
		tagSet.lockIndexTable();
		//生成特征树
		this.featureMap = new MutableFeatureMap(tagSet);
		FeatureMap featureMap = this.featureMap;
		final int sizeOfTagSet = tagSet.size();
		TreeMap<String, FeatureWeight> featureFunctionMap = new TreeMap<String, FeatureWeight>(); // 构建trie树的时候用
		TreeMap<Integer, FeatureWeight> featureFunctionList = new TreeMap<Integer, FeatureWeight>(); // 读取权值的时候用
		ArrayList<FeatureTemplate> featureTemplateList = new ArrayList<FeatureTemplate>();
		float[][] matrix = null;
		//这部分从结果模型文件中解析出模板，构建出模板列表：featureTemplateList
		while ((line = lineIterator.next()).length() != 0) {
			if (!"B".equals(line)) {
				FeatureTemplate featureTemplate = FeatureTemplate.create(line);
				featureTemplateList.add(featureTemplate);
			} else {
				matrix = new float[sizeOfTagSet][sizeOfTagSet];
			}
		}
		this.featureTemplateArray = featureTemplateList.toArray(new FeatureTemplate[0]);

		int b = -1;// 转换矩阵的权重位置
		if (matrix != null) {
			String[] args = lineIterator.next().split(" ", 2); // 0 B
			b = Integer.valueOf(args[0]);
			featureFunctionList.put(b, null);
		}

		while ((line = lineIterator.next()).length() != 0) {
			String[] args = line.split(" ", 2);
			char[] charArray = args[1].toCharArray();
			FeatureWeight featureFunction = new FeatureWeight(charArray, sizeOfTagSet);
			featureFunctionMap.put(args[1], featureFunction);
			featureFunctionList.put(Integer.parseInt(args[0]), featureFunction);
		}

		for (Map.Entry<Integer, FeatureWeight> entry : featureFunctionList.entrySet()) {
			int fid = entry.getKey();
			FeatureWeight featureFunction = entry.getValue();
			if (fid == b) {
				for (int i = 0; i < sizeOfTagSet; i++) {
					for (int j = 0; j < sizeOfTagSet; j++) {
						matrix[i][j] = Float.parseFloat(lineIterator.next());
					}
				}
			} else {
				for (int i = 0; i < sizeOfTagSet; i++) {
					featureFunction.w[i] = Double.parseDouble(lineIterator.next());
				}
			}
		}
		if (lineIterator.hasNext()) {
			NlpLogger.warning(myName, "文本读取有残留，可能会出问题:{}" , txtFile);
		}
		lineIterator.close();
		NlpLogger.info(myName, "文本读取结束，开始转换模型");
		int transitionFeatureOffset = (sizeOfTagSet + 1) * sizeOfTagSet;
		parameter = new float[transitionFeatureOffset + featureFunctionMap.size() * sizeOfTagSet];
		if (matrix != null) {
			for (int i = 0; i < sizeOfTagSet; ++i) {
				for (int j = 0; j < sizeOfTagSet; ++j) {
					parameter[i * sizeOfTagSet + j] = matrix[i][j];
				}
			}
		}
		for (Map.Entry<Integer, FeatureWeight> entry : featureFunctionList.entrySet()) {
			FeatureWeight f = entry.getValue();
			if (f == null)
				continue;
			String feature = new String(f.o);
			for (int tid = 0; tid < featureTemplateList.size(); tid++) {
				FeatureTemplate template = featureTemplateList.get(tid);
				Iterator<String> iterator = template.delimiterList.iterator();
				String header = iterator.next();
				if (feature.startsWith(header)) {
					int fid = featureMap.indexByTag(feature.substring(header.length()) + tid);
					for (int i = 0; i < sizeOfTagSet; ++i) {
						parameter[fid * sizeOfTagSet + i] = (float) f.w[i];
					}
					break;
				}
			}
		}
		DataOutputStream out = new DataOutputStream(IOUtil.newOutputStream(binFile));
		save(out);
		out.writeInt(featureTemplateList.size());
		for (FeatureTemplate template : featureTemplateList) {
			template.save(out);
		}
		out.close();
	}

	private TaggedType guessModelType(TagSet tagSet) {
		if (tagSet.size() == 4 && tagSet.indexByTag("B") != -1 && tagSet.indexByTag("M") != -1 && tagSet.indexByTag("E") != -1 && tagSet.indexByTag("S") != -1)
			return TaggedType.CWS;
		
		if (tagSet.indexByTag("O") != -1) {
			for (String tag : tagSet.tags()) {
				String[] parts = tag.split("-");
				if (parts.length > 1) {
					if (parts[0].length() == 1 && "BMES".contains(parts[0]))
						return TaggedType.NER;
				}
			}
		}
		
		return TaggedType.POS;
	}

	public FeatureTemplate[] getFeatureTemplateArray() {
		return featureTemplateArray;
	}
}
