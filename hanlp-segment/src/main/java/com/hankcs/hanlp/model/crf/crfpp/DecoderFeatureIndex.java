package com.hankcs.hanlp.model.crf.crfpp;

import com.hankcs.hanlp.collection.trie.datrie.MutableDoubleArrayTrieInteger;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhifac
 */
class DecoderFeatureIndex extends FeatureIndex {
	private static String myName = DecoderFeatureIndex.class.getSimpleName();
	
	/** 可变双数组trie树 * */
	private MutableDoubleArrayTrieInteger dat;

	public DecoderFeatureIndex() {
		dat = new MutableDoubleArrayTrieInteger();
	}

	protected int generateID(String key) {
		return dat.get(key);
	}

	@SuppressWarnings("unchecked")
	public boolean open(InputStream stream) {
		try {
			ObjectInputStream ois = new ObjectInputStream(stream);

			costFactor = (Double) ois.readObject();
			maxId = (Integer) ois.readObject();
			colSize = (Integer) ois.readObject();
			rowLabel = (List<String>) ois.readObject();
			unigramTemplates = (List<String>) ois.readObject();
			bigramTemplates = (List<String>) ois.readObject();
			dat = (MutableDoubleArrayTrieInteger) ois.readObject();
			weight = (double[]) ois.readObject();
			ois.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean convert(String binarymodel, String textmodel) {
		try {
			if (!open(IOUtil.newInputStream(binarymodel))) {
				NlpLogger.warning(myName, "Fail to read binary model {}", binarymodel);
				return false;
			}
			OutputStreamWriter osw = new OutputStreamWriter(IOUtil.newOutputStream(textmodel), "UTF-8");
			osw.write("version: " + Encoder.MODEL_VERSION + "\n");
			osw.write("cost-factor: " + costFactor + "\n");
			osw.write("maxid: " + maxId + "\n");
			osw.write("xsize: " + colSize + "\n");
			osw.write("\n");
			for (String y : rowLabel) {
				osw.write(y + "\n");
			}
			osw.write("\n");
			for (String utempl : unigramTemplates) {
				osw.write(utempl + "\n");
			}
			for (String bitempl : bigramTemplates) {
				osw.write(bitempl + "\n");
			}
			osw.write("\n");

			for (MutableDoubleArrayTrieInteger.KeyValuePair pair : dat) {
				osw.write(pair.getValue() + " " + pair.getKey() + "\n");
			}

			osw.write("\n");

			for (int k = 0; k < maxId; k++) {
				String val = new DecimalFormat("0.0000000000000000").format(weight[k]);
				osw.write(val + "\n");
			}
			osw.close();
			return true;
		} catch (Exception e) {
			NlpLogger.error(myName,  "{} does not exist",binarymodel, e);
			return false;
		}
	}

	public boolean openTextModel(String filename1, boolean cacheBinModel) {
		InputStreamReader isr = null;
		try {
			String binFileName = filename1 + ".bin";
			try {
				if (open(IOUtil.newInputStream(binFileName))) {
					NlpLogger.info(myName, "Found binary model {}", binFileName);
					return true;
				}
			} catch (IOException e) {
				// load text model
			}

			isr = new InputStreamReader(IOUtil.newInputStream(filename1), "UTF-8");
			BufferedReader br = new BufferedReader(isr);
			String line;

			int version = Integer.valueOf(br.readLine().substring("version: ".length()));
			costFactor = Double.valueOf(br.readLine().substring("cost-factor: ".length()));
			maxId = Integer.valueOf(br.readLine().substring("maxid: ".length()));
			colSize = Integer.valueOf(br.readLine().substring("xsize: ".length()));
			NlpLogger.info(myName, "读取文件基础信息完毕");
			br.readLine();

			while ((line = br.readLine()) != null && line.length() > 0) {
				rowLabel.add(line);
			}
			NlpLogger.info(myName, "读取标注方法集完毕");
			while ((line = br.readLine()) != null && line.length() > 0) {
				if (line.startsWith("U")) {
					unigramTemplates.add(line);
				} else if (line.startsWith("B")) {
					bigramTemplates.add(line);
				}
			}
			NlpLogger.info(myName, "读取标注模板完毕");
			while ((line = br.readLine()) != null && line.length() > 0) {
				String[] content = line.trim().split(" ");
				dat.put(content[1], Integer.valueOf(content[0]));
			}
			List<Double> alphaList = new ArrayList<Double>();
			while ((line = br.readLine()) != null && line.length() > 0) {
				alphaList.add(Double.valueOf(line));
			}
			NlpLogger.info(myName, "读取特征权重完毕");
			weight = new double[alphaList.size()];
			for (int i = 0; i < alphaList.size(); i++) {
				weight[i] = alphaList.get(i);
			}
			br.close();

			if (cacheBinModel) {
				NlpLogger.info(myName, "Writing binary model to {}", binFileName);
				ObjectOutputStream oos = new ObjectOutputStream(IOUtil.newOutputStream(binFileName));
				oos.writeObject(version);
				oos.writeObject(costFactor);
				oos.writeObject(maxId);
				oos.writeObject(colSize);
				oos.writeObject(rowLabel);
				oos.writeObject(unigramTemplates);
				oos.writeObject(bigramTemplates);
				oos.writeObject(dat);
				oos.writeObject(weight);
				oos.close();
			}
		} catch (Exception e) {
			IOUtil.close(isr);
			NlpLogger.error(myName, "Error reading {}", filename1, e);
			return false;
		}
		
		return true;
	}

}
