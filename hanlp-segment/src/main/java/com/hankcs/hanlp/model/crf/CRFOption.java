package com.hankcs.hanlp.model.crf;

public class CRFOption {
	/** 最低词频阈值 * */
	private Integer freq = 1;
	/** 迭代次数  * */
	private Integer maxiter = 10000;
	/** 代价因子  * */
	private Double cost = 1.0;
	/** 收敛阈值  * */
	private Double eta = 0.0001;
	/** 是否需要转换  * */
	private Boolean convert = false;
	/** 是否转换为文本格式  * */
	private Boolean convertToText = false;
	/** 是否需要输出文本格式  * */
	private Boolean textmodel = false;
	/** 算法：CRF-L1， CRF-L2， MIRA  * */
	private String algorithm = "CRF-L2";
	/** 输出空间大小  * */
	private Integer shrinkingSize = 20;
	/** 是否需要帮助提示  * */
	private Boolean help = false;
	/** 执行线程数  * */
	private Integer thread = Runtime.getRuntime().availableProcessors();

	public Integer getFreq() {
		return freq;
	}

	public void setFreq(Integer freq) {
		this.freq = freq;
	}

	public Integer getMaxiter() {
		return maxiter;
	}

	public void setMaxiter(Integer maxiter) {
		this.maxiter = maxiter;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getEta() {
		return eta;
	}

	public void setEta(Double eta) {
		this.eta = eta;
	}

	public Boolean getConvert() {
		return convert;
	}

	public void setConvert(Boolean convert) {
		this.convert = convert;
	}

	public Boolean getConvertToText() {
		return convertToText;
	}

	public void setConvertToText(Boolean convertToText) {
		this.convertToText = convertToText;
	}

	public Boolean getTextmodel() {
		return textmodel;
	}

	public void setTextmodel(Boolean textmodel) {
		this.textmodel = textmodel;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public Integer getShrinkingSize() {
		return shrinkingSize;
	}

	public void setShrinkingSize(Integer shrinkingSize) {
		this.shrinkingSize = shrinkingSize;
	}

	public Boolean getHelp() {
		return help;
	}

	public void setHelp(Boolean help) {
		this.help = help;
	}

	public Integer getThread() {
		return thread;
	}

	public void setThread(Integer thread) {
		this.thread = thread;
	}

	public enum Algorithm {
		CRF_L2, 
		CRF_L1, 
		MIRA;

		public static Algorithm fromString(String algorithm) {
			algorithm = algorithm.toLowerCase();
			if (algorithm.equals("crf") || algorithm.equals("crf-l2")) {
				return CRF_L2;
			} else if (algorithm.equals("crf-l1")) {
				return CRF_L1;
			} else if (algorithm.equals("mira")) {
				return MIRA;
			}
			throw new IllegalArgumentException("invalid algorithm: " + algorithm);
		}
	}
}
