package com.hankcs.hanlp.model.crf.crfpp;

import java.util.List;

/**
 * 弧<br/>
 * 连接两个节点的边
 *
 * @author zhifac, hoobort
 */
class CrfEdge {
	/** 右节点 * */
	public CrfNode nextNode;
	/** 左节点 * */
	public CrfNode priorNode;
	/** 向量 * */
	public List<Integer> fvector;
	/** 代价值 * */
	public double cost;

	public CrfEdge() {
		clear();
	}

	public void clear() {
		nextNode = priorNode = null;
		fvector = null;
		cost = 0.0;
	}

	/**
	 * 计算边的期望
	 *
	 * @param expected 输出期望
	 * @param Z        规范化因子
	 * @param size     标签个数
	 */
	public void calcExpectation(double[] expected, double Z, int size) {
		double c = Math.exp(priorNode.alpha + cost + nextNode.beta - Z);
		for (int i = 0; fvector.get(i) != -1; i++) {
			int idx = fvector.get(i) + priorNode.y * size + nextNode.y;
			expected[idx] += c;
		}
	}

	public void add(CrfNode _lnode, CrfNode _rnode) {
		priorNode = _lnode;
		nextNode = _rnode;
		priorNode.nextEdges.add(this);
		nextNode.priorEdges.add(this);
	}
}
