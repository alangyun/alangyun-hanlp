感知机在线学习算法的线性序列标注模型。基于这套框架实现了一整套分词、词性标注和命名实体识别功能。

理论参考邓知龙 《基于感知器算法的高效中文分词与词性标注系统设计与实现》，

简介：http://www.hankcs.com/nlp/segment/implementation-of-word-segmentation-device-java-based-on-structured-average-perceptron.html