/*
 * <summary></summary>
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2016-09-04 PM5:23</create-date>
 *
 * <copyright file="FeatureMap.java" company="码农场">
 * Copyright (c) 2008-2016, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.model.feature;

import com.hankcs.hanlp.collection.array.ByteArray;
import com.hankcs.hanlp.io.ICacheAble;
import com.hankcs.hanlp.meta.tagger.TaggedType;
import com.hankcs.hanlp.meta.tagger.map.ITagIndexMap;
import com.hankcs.hanlp.meta.tagger.tag.CWSTagSet;
import com.hankcs.hanlp.meta.tagger.tag.NERTagSet;
import com.hankcs.hanlp.meta.tagger.tag.POSTagSet;
import com.hankcs.hanlp.meta.tagger.tag.TagSet;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * 特征映射
 * @author hankcs, hoobort
 */
public abstract class FeatureMap implements ITagIndexMap, ICacheAble {

	/** 标注集 * */
	public TagSet tagSet;
	/** 是否允许新增特征 */
	public boolean mutable;
	/** * */
	public abstract Set<Map.Entry<String, Integer>> entrySet();

	public abstract int size();
	
	public FeatureMap(TagSet tagSet) {
		this(tagSet, false);
	}

	public FeatureMap(TagSet tagSet, boolean mutable) {
		this.tagSet = tagSet;
		this.mutable = mutable;
	}

	public FeatureMap(boolean mutable) {
		this.mutable = mutable;
	}

	public FeatureMap() {
		this(false);
	}

	/**
	 * 获取标注的所有索引（即标注下标）
	 *
	 * hoobort
	 * 2022年10月13日 上午12:29:50
	 * @return
	 */
	public int[] tagIndexTable() {
		return tagSet.tagIndexTable();
	}

	/**
	 * 获取新的标注序列号（实际就是数组/列表索引）
	 *
	 * hoobort
	 * 2022年10月13日 上午12:30:51
	 * @return
	 */
	public int newTagSequence() {
		return tagSet.size();
	}

	@Override
	public void save(DataOutputStream out) throws IOException {
		tagSet.save(out);
		out.writeInt(size());
		for (Map.Entry<String, Integer> entry : entrySet()) {
			out.writeUTF(entry.getKey());
		}
	}

	@Override
	public boolean load(ByteArray byteArray) {
		loadTagSet(byteArray);
		int size = byteArray.nextInt();
		for (int i = 0; i < size; i++) {
			indexByTag(byteArray.nextUTF());
		}
		return true;
	}

	protected final void loadTagSet(ByteArray byteArray) {
		TaggedType type = TaggedType.values()[byteArray.nextInt()];
		switch (type) {
		case CWS:
			tagSet = new CWSTagSet();
			break;
		case POS:
			tagSet = new POSTagSet();
			break;
		case NER:
			tagSet = new NERTagSet();
			break;
		case CLASSIFICATION:
			tagSet = new TagSet(TaggedType.CLASSIFICATION);
			break;
		}
		tagSet.load(byteArray);
	}
}