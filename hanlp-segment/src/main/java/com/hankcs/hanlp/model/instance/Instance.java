/*
 * <summary></summary>
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2016-09-04 PM5:16</create-date>
 *
 * <copyright file="SentenceInstance.java" company="码农场">
 * Copyright (c) 2008-2016, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.model.instance;

import com.hankcs.hanlp.meta.tagger.tag.TagSet;
import com.hankcs.hanlp.model.feature.FeatureMap;

import java.util.List;

/**
 * @author hankcs
 */
public class Instance {
	/** 特征矩阵 * */
	public int[][] featureMatrix;
	/** 标注索引数组 * */
	public int[] tagIndexArray;

	protected Instance() {
		
	}

	public int[] getFeatureAt(int position) {
		return featureMatrix[position];
	}

	public int length() {
		return tagIndexArray.length;
	}

	protected static void addFeature(CharSequence rawFeature, List<Integer> featureVector, FeatureMap featureMap) {
		int id = featureMap.indexByTag(rawFeature.toString());
		if (id != -1)
			featureVector.add(id);
	}

	/**
	 * 特征列表转换为特征数组
	 *
	 * hoobort
	 * 2022年10月13日 上午12:40:00
	 * @param featureVector
	 * @return
	 */
	protected static int[] toFeatureArray(List<Integer> featureVector) {
		int[] featureArray = new int[featureVector.size() + 1]; // 最后一列留给转移特征
		int index = -1;
		for (Integer feature : featureVector)
			featureArray[++index] = feature;

		return featureArray;
	}

	/**
	 * 添加特征，同时清空缓存
	 *
	 * @param rawFeature
	 * @param featureVector
	 * @param featureMap
	 */
	protected static void addFeatureThenClear(StringBuilder rawFeature, List<Integer> featureVector, FeatureMap featureMap) {
		int id = featureMap.indexByTag(rawFeature.toString());
		if (id != -1)
			featureVector.add(id);
		
		rawFeature.setLength(0);
	}

	/**
	 * 根据标注集还原字符形式的标签
	 *
	 * @param tagSet
	 * @return
	 */
	public String[] tags(TagSet tagSet) {
		assert tagIndexArray != null;

		String[] tags = new String[tagIndexArray.length];
		for (int i = 0; i < tags.length; i++) {
			tags[i] = tagSet.tagByIndex(tagIndexArray[i]);
		}

		return tags;
	}

	/**
	 * 实例大小（有多少个要预测的元素）
	 *
	 * @return
	 */
	public int size() {
		return featureMatrix.length;
	}
}