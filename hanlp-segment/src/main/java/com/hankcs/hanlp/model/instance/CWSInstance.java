/*
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2017-10-26 下午9:21</create-date>
 *
 * <copyright file="CWSInstance.java" company="码农场">
 * Copyright (c) 2017, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.model.instance;

import com.hankcs.hanlp.model.feature.FeatureMap;
import com.hankcs.hanlp.meta.tagger.tag.CWSTagSet;
import com.hankcs.hanlp.meta.word.Sentence;
import com.hankcs.hanlp.meta.word.Word;

import java.util.LinkedList;
import java.util.List;

/**
 * @author hankcs
 */
public class CWSInstance extends Instance {
	private static final char CHAR_BEGIN = '\u0001';
	private static final char CHAR_END = '\u0002';

	/**
	 * 生成分词实例
	 *
	 * @param termArray  分词序列
	 * @param featureMap 特征收集
	 */
	public CWSInstance(String[] termArray, FeatureMap featureMap) {
		String sentence = com.hankcs.hanlp.utility.TextUtility.combine(termArray);
		CWSTagSet tagSet = (CWSTagSet) featureMap.tagSet;

		tagIndexArray = new int[sentence.length()];
		for (int i = 0, j = 0; i < termArray.length; i++) {
			assert termArray[i].length() > 0 : "句子中出现了长度为0的单词，不合法：" + sentence;
			
			if (termArray[i].length() == 1)
				tagIndexArray[j++] = tagSet.S;
			else {
				tagIndexArray[j++] = tagSet.B;
				for (int k = 1; k < termArray[i].length() - 1; k++)
					tagIndexArray[j++] = tagSet.M;				
				tagIndexArray[j++] = tagSet.E;
			}
		}

		initFeatureMatrix(sentence, featureMap);
	}

	public CWSInstance(String sentence, FeatureMap featureMap) {
		initFeatureMatrix(sentence, featureMap);
		tagIndexArray = new int[sentence.length()];
	}

	protected int[] extractFeature(String sentence, FeatureMap featureMap, int position) {
		List<Integer> featureVec = new LinkedList<Integer>();

		char pre2Char = position >= 2 ? sentence.charAt(position - 2) : CHAR_BEGIN;
		char preChar = position >= 1 ? sentence.charAt(position - 1) : CHAR_BEGIN;
		char curChar = sentence.charAt(position);
		char nextChar = position < sentence.length() - 1 ? sentence.charAt(position + 1) : CHAR_END;
		char next2Char = position < sentence.length() - 2 ? sentence.charAt(position + 2) : CHAR_END;

		StringBuilder sbFeature = new StringBuilder();

		sbFeature.delete(0, sbFeature.length());
		sbFeature.append(preChar).append('1');
		addFeature(sbFeature, featureVec, featureMap);

		sbFeature.delete(0, sbFeature.length());
		sbFeature.append(curChar).append('2');
		addFeature(sbFeature, featureVec, featureMap);

		sbFeature.delete(0, sbFeature.length());
		sbFeature.append(nextChar).append('3');
		addFeature(sbFeature, featureVec, featureMap);

		// char bigram feature
		sbFeature.delete(0, sbFeature.length());
		sbFeature.append(pre2Char).append("/").append(preChar).append('4');
		addFeature(sbFeature, featureVec, featureMap);

		sbFeature.delete(0, sbFeature.length());
		sbFeature.append(preChar).append("/").append(curChar).append('5');
		addFeature(sbFeature, featureVec, featureMap);

		sbFeature.delete(0, sbFeature.length());
		sbFeature.append(curChar).append("/").append(nextChar).append('6');
		addFeature(sbFeature, featureVec, featureMap);

		sbFeature.delete(0, sbFeature.length());
		sbFeature.append(nextChar).append("/").append(next2Char).append('7');
		addFeature(sbFeature, featureVec, featureMap);

//		sbFeature = null;
		
		return toFeatureArray(featureVec);
	}

	protected void initFeatureMatrix(String sentence, FeatureMap featureMap) {
		featureMatrix = new int[sentence.length()][];
		for (int i = 0; i < sentence.length(); i++) {
			featureMatrix[i] = extractFeature(sentence, featureMap, i);
		}
	}

	public static CWSInstance create(Sentence sentence, FeatureMap featureMap) {
		if (sentence == null || featureMap == null) {
			return null;
		}
		List<Word> wordList = sentence.toSimpleWordList();
		String[] termArray = new String[wordList.size()];
		int i = 0;
		for (Word word : wordList) {
			termArray[i] = word.getValue();
			++i;
		}
		return new CWSInstance(termArray, featureMap);
	}
}
