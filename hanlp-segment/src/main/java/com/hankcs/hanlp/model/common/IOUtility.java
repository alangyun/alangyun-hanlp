/*
 * <summary></summary>
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2016-09-04 PM7:29</create-date>
 *
 * <copyright file="IOUtility.java" company="码农场">
 * Copyright (c) 2008-2016, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.model.common;

import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.word.Sentence;
import com.hankcs.hanlp.model.instance.Instance;
import com.hankcs.hanlp.model.instance.InstanceHandler;
import com.hankcs.hanlp.model.linear.LinearModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
 * @author hankcs, hoobort
 */
public class IOUtility extends IOUtil {
	private static String myName = IOUtility.class.getSimpleName();

	private static Pattern PATTERN_SPACE = Pattern.compile("\\s+");

	public static String[] readLineToArray(String line) {
		line = line.trim();
		if (line.length() == 0)
			return new String[0];
		return PATTERN_SPACE.split(line);
	}

	/**
	 * 根据指定的语料文件或者语料所在路径的语料文件读取出内容，读取的结果格式由InstanceHandler完成
	 *
	 * hoobort 2022年10月11日 下午9:48:41
	 * 
	 * @param path
	 * @param handler
	 * @return
	 * @throws IOException
	 */
	public static int loadInstance(final String path, InstanceHandler handler) throws IOException {
		int size = 0;
		File root = new File(path);
		File allFiles[];
		// 列出文件列表
		if (root.isDirectory()) {
			allFiles = root.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return pathname.isFile() && pathname.getName().endsWith(".txt");
				}
			});
		} else
			allFiles = new File[] { root };

		int fileTotal = allFiles.length;
		String line = null;
		int lineCount =0 ;
		BufferedReader br = null;
		NlpLogger.info(myName, "准备载入语料文件，共 {} 个", fileTotal);
		for (File file : allFiles) {
			// hoobort: 原来文件打开了未关闭，增加对br的关闭功能
			br = null;
			lineCount =0 ;
			try {
				br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

				NlpLogger.info(myName, "正在读取文件: {}", file.getAbsoluteFile());
				while ((line = br.readLine()) != null) {
					line = line.trim();
					if (line.length() == 0)
						continue;

					Sentence sentence = Sentence.create(line);
					if (sentence.wordList.size() == 0)
						continue;

					++lineCount;

					// 调用回调接口处理句子
					if (handler.process(sentence))
						break;
				}
				size+=lineCount;
				NlpLogger.info(myName, "读取完成，从文件{}中提取到 {} 句。", file.getAbsoluteFile(), lineCount);
			} catch (Exception ex) {
				throw ex;
			} finally {
				close(br);
			}
		}

		return size;
	}

	public static double[] evaluate(Instance[] instances, LinearModel model) {
		int[] stat = new int[2];
		NlpLogger.info(myName, "正在评估线性模型...");
		for (int i = 0; i < instances.length; i++) {
			evaluate(instances[i], model, stat);
			if (NlpLogger.enableDebug()) {
				if (i % 300 == 0 || i == instances.length - 1) {
					NlpLogger.debug(myName, "评估进度: {}%", ((i + 1) / (float) instances.length) * 100);
				}
			}
		}
		NlpLogger.info(myName, "评估完成！");
		
		return new double[] { stat[1] / (double) stat[0] * 100 };
	}

	public static void evaluate(Instance instance, LinearModel model, int[] stat) {
		int[] predLabel = new int[instance.length()];
		model.viterbiDecode(instance, predLabel);
		stat[0] += instance.tagIndexArray.length;
		for (int i = 0; i < predLabel.length; i++) {
			if (predLabel[i] == instance.tagIndexArray[i]) {
				++stat[1];
			}
		}
	}
}
