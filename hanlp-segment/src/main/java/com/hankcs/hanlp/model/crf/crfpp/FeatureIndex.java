package com.hankcs.hanlp.model.crf.crfpp;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.model.crf.CRFConst;

/**
 * 特征索引<br/>
 * 
 * @author zhifac, hoobort
 */
abstract class FeatureIndex {
	private static String myName = FeatureIndex.class.getSimpleName();
	
	protected int maxId;
	/** 特征的代价（权重值） * */
	protected double[] weight;
	private float[] alphaFloat;
	/** 代价因子 * */
	protected double costFactor;
	/** 列大小 * */
	protected int colSize;
	/** 是否要检查特征列大小 * */
	private boolean checkMaxColSize;
	/** 列的最大数量 * */
	private int maxColSize;
	/** U模板列表 * */
	protected List<String> unigramTemplates;
	/** B模板列表 * */
	protected List<String> bigramTemplates;
	/** 标注集列表（特征对应的标注标签，无重复）<br/>
	 * 如果是词典训练：读取的值为BMES标签的其中之一<br/>
	 * 如果是词性模型训练，读取的值为词性标签<br/>
	 * 如果是命名实体训练，读取的值为命名实体标签前缀<br/>
	 *  * */
	protected List<String> rowLabel;

	public FeatureIndex() {
		maxId = 0;
		weight = null;
		alphaFloat = null;
		costFactor = 1.0;
		colSize = 0;
		checkMaxColSize = false;
		maxColSize = 0;
		unigramTemplates = new ArrayList<String>();
		bigramTemplates = new ArrayList<String>();
		rowLabel = new ArrayList<String>();
	}

	/**
	 * 生成特征索引
	 *
	 * hoobort
	 * 2022年10月15日 上午11:35:10
	 * @param s
	 * @return
	 */
	protected abstract int generateID(String s);

	/**
	 * 计算状态特征函数的代价
	 *
	 * @param node
	 */
	public void calcCost(CrfNode node) {
		node.cost = 0.0;
		if (alphaFloat != null) {
			float c = 0.0f;
			for (int i = 0; node.fVector.get(i) != -1; i++) {
				c += alphaFloat[node.fVector.get(i) + node.y];
			}
			node.cost = costFactor * c;
		} else {
			double c = 0.0;
			for (int i = 0; node.fVector.get(i) != -1; i++) {
				c += weight[node.fVector.get(i) + node.y];
			}
			node.cost = costFactor * c;
		}
	}
	
	protected void needCheckMaxColSize(boolean check) {
		checkMaxColSize = check;
	}

	/**
	 * 计算转移特征函数的代价
	 *
	 * @param path 边
	 */
	public void calcCost(CrfEdge path) {
		path.cost = 0.0;
		if (alphaFloat != null) {
			float c = 0.0f;
			for (int i = 0; path.fvector.get(i) != -1; i++) {
				c += alphaFloat[path.fvector.get(i) + path.priorNode.y * rowLabel.size() + path.nextNode.y];
			}
			path.cost = costFactor * c;
		} else {
			double c = 0.0;
			for (int i = 0; path.fvector.get(i) != -1; i++) {
				c += weight[path.fvector.get(i) + path.priorNode.y * rowLabel.size() + path.nextNode.y];
			}
			path.cost = costFactor * c;
		}
	}

	public String makeTempls(List<String> unigramTempls, List<String> bigramTempls) {
		StringBuilder sb = new StringBuilder();
		for (String temp : unigramTempls) {
			sb.append(temp).append("\n");
		}
		for (String temp : bigramTempls) {
			sb.append(temp).append("\n");
		}
		return sb.toString();
	}

	public String getIndex(String[] idxStr, int cur, TaggerImpl tagger) {
		int row = Integer.valueOf(idxStr[0]);
		int col = Integer.valueOf(idxStr[1]);
		int pos = row + cur;
		if (row < -CRFConst.EOS.length || row > CRFConst.EOS.length || col < 0 || col >= tagger.xsize()) {
			return null;
		}

		// TODO(taku): very dirty workaround
		if (checkMaxColSize) 
			maxColSize = Math.max(maxColSize, col + 1);
		
		if (pos < 0) {
			return CRFConst.BOS[-pos - 1];
		} else if (pos >= tagger.size()) {
			return CRFConst.EOS[pos - tagger.size()];
		} else {
			return tagger.x(pos, col);
		}
	}

	public String applyRule(String str, int cur, TaggerImpl tagger) {
		StringBuilder sb = new StringBuilder();
		for (String tmp : str.split("%x", -1)) {
			if (tmp.startsWith("U") || tmp.startsWith("B")) {
				sb.append(tmp);
			} else if (tmp.length() > 0) {
				String[] tuple = tmp.split("]");
				String[] idx = tuple[0].replace("[", "").split(",");
				String r = getIndex(idx, cur, tagger);
				if (r != null) 
					sb.append(r);
				
				if (tuple.length > 1) 
					sb.append(tuple[1]);				
			}
		}

		return sb.toString();
	}

	private boolean buildFeatureFromTempl(List<Integer> feature, List<String> templs, int curPos, TaggerImpl tagger) {
		for (String tmpl : templs) {
			String featureID = applyRule(tmpl, curPos, tagger);
			if (featureID == null || featureID.length() == 0) {
				NlpLogger.warning(myName, "format error");
				return false;
			}
			int id = generateID(featureID);
			if (id != -1) {
				feature.add(id);
			}
		}
		return true;
	}

	public boolean buildFeatures(TaggerImpl tagger) {
		List<Integer> feature = new ArrayList<Integer>();
		List<List<Integer>> featureCache = tagger.getFeatureCache_();
		tagger.setFeature_id_(featureCache.size());

		for (int cur = 0; cur < tagger.size(); cur++) {
			if (!buildFeatureFromTempl(feature, unigramTemplates, cur, tagger)) {
				return false;
			}
			feature.add(-1);
			featureCache.add(feature);
			feature = new ArrayList<Integer>();
		}
		for (int cur = 1; cur < tagger.size(); cur++) {
			if (!buildFeatureFromTempl(feature, bigramTemplates, cur, tagger)) {
				return false;
			}
			feature.add(-1);
			featureCache.add(feature);
			feature = new ArrayList<Integer>();
		}
		return true;
	}

	public void rebuildFeatures(TaggerImpl tagger) {
		int fid = tagger.getFeature_id_();
		List<List<Integer>> featureCache = tagger.getFeatureCache_();
		for (int cur = 0; cur < tagger.size(); cur++) {
			List<Integer> f = featureCache.get(fid++);
			for (int i = 0; i < rowLabel.size(); i++) {
				CrfNode n = new CrfNode();
				n.clear();
				n.x = cur;
				n.y = i;
				n.fVector = f;
				tagger.set_node(n, cur, i);
			}
		}
		for (int cur = 1; cur < tagger.size(); cur++) {
			List<Integer> f = featureCache.get(fid++);
			for (int j = 0; j < rowLabel.size(); j++) {
				for (int i = 0; i < rowLabel.size(); i++) {
					CrfEdge p = new CrfEdge();
					p.clear();
					p.add(tagger.node(cur - 1, j), tagger.node(cur, i));
					p.fvector = f;
				}
			}
		}
	}

	public boolean open(String file) {
		return true;
	}

	public boolean open(InputStream stream) {
		return true;
	}

	public void clear() {

	}

	public int size() {
		return getMaxId();
	}

	public int rowSize() {
		return rowLabel.size();
	}

	public int getMaxId() {
		return maxId;
	}

	public void setMaxId(int maxId) {
		this.maxId = maxId;
	}

	public double[] getWeight() {
		return weight;
	}

	public void setWeight(double[] weight) {
		this.weight = weight;
	}

	public float[] getAlphaFloat() {
		return alphaFloat;
	}

	public void setAlphaFloat(float[] alphaFloat) {
		this.alphaFloat = alphaFloat;
	}

	public double getCostFactor() {
		return costFactor;
	}

	public void setCostFactor(double costFactor) {
		this.costFactor = costFactor;
	}

	public int getColSize() {
		return colSize;
	}

	public void setColSize(int xSize) {
		this.colSize = xSize;
	}

	public int getMaxColSize() {
		return maxColSize;
	}

//	public void setMaxXSize(int maxXSize) {
//		this.maxXSize = maxXSize;
//	}

	public List<String> getUnigramTemplates() {
		return unigramTemplates;
	}

	public void setUnigramTemplates(List<String> unigramTemplates) {
		this.unigramTemplates = unigramTemplates;
	}

	public List<String> getBigramTemplates() {
		return bigramTemplates;
	}

	public void setBigramTemplates(List<String> bigramTemplates) {
		this.bigramTemplates = bigramTemplates;
	}

	public List<String> getRowLabels() {
		return rowLabel;
	}

	public void setRowLabels(List<String> rowLabel) {
		this.rowLabel = rowLabel;
	}

}
