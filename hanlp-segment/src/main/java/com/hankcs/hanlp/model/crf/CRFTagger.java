/*
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2018-03-30 上午2:51</create-date>
 *
 * <copyright file="CRFTagger.java" company="码农场">
 * Copyright (c) 2018, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.model.crf;

import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.word.Sentence;
import com.hankcs.hanlp.model.common.IOUtility;
import com.hankcs.hanlp.model.common.Utility;
import com.hankcs.hanlp.model.crf.crfpp.Encoder;
import com.hankcs.hanlp.model.instance.InstanceHandler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * CRF标注基类<br/>
 * 
 * @author hankcs, hoobort
 */
public abstract class CRFTagger {
	protected LogarithmLinearModel model;

	public CRFTagger() {

	}

	public CRFTagger(String modelPath) throws IOException {
		if (modelPath == null)
			return; // 训练模式
		
		model = new LogarithmLinearModel(modelPath);
	}

	/**
	 * 将CRF++二进制格式转换为text格式
	 *
	 * @param modelFile
	 * @throws IOException
	 */
	private void convert(String modelFile) throws IOException {
		this.model = new LogarithmLinearModel(modelFile + ".txt", modelFile);
	}
	/**
	 * 构建临时的模板文件名(不含扩展名)
	 *
	 * hoobort
	 * 2022年10月11日 下午2:18:24
	 * @return
	 */
	private String buildTemplateFile() {
		return String.format("crfpp-template-%d",  new Date().getTime());
	}
	/**
	 * 构建临时的训练语料文件名(不含扩展名)
	 *
	 * hoobort
	 * 2022年10月11日 下午2:18:42
	 * @return
	 */
	private String buildTrainCorpusFile() {
		return String.format("crfpp-train-%d",  new Date().getTime());
	}

	/**
	 * CRF训练<br/>
	 * 通过该函数进行训练，会提取模型参数作为配置参数，自动创建临时的模板文件<br/>
	 * hoobort
	 * 2022年10月11日 下午1:54:34
	 * @param trainCorpusPath 训练语料文件或训练语料所在路径（语料必须为已经做好标注，格式参照人民日报语料）
	 * @param modelPath 训练结果模型保存文件全路径
	 * @throws IOException
	 */
	public void train(String trainCorpusPath, String modelPath) throws IOException {
		CRFOption option = new CRFOption();
		train(trainCorpusPath, 
				modelPath, 
				option.getMaxiter(), 
				option.getFreq(), 
				option.getEta(), 
				option.getCost(), 
				option.getThread(), 
				option.getShrinkingSize(),
				CRFOption.Algorithm.fromString(option.getAlgorithm()));
	}

	/**
	 * CRF训练<br/>
	 * 通过该函数进行训练，会自动构建一个临时的模板文件<br/>
	 * 
	 * hoobort
	 * 2022年10月11日 下午1:57:01
	 * @param trainFile     训练语料文件或训练语料所在路径（语料必须为已经做好标注，格式参照人民日报语料）
	 * @param modelSaveFile     训练结果模型保存文件全路径
	 * @param maxitr        最大迭代次数 默认10000次
	 * @param freq          特征最低频次 默认1
	 * @param eta           收敛阈值 默认为0.0001
	 * @param C             cost-factor 代价因子，默认为1.0
	 * @param threadNum     线程数 默认为计算机cpu线程数（非核数）
	 * @param shrinkingSize 收缩率，1~100（如果填写，则会剔除掉所填写的比例），默认为20
	 * @param algorithm     训练算法，支持两种算法：拟牛顿法和RIMA
	 * @throws IOException
	 */
	public void train(String trainFile, String modelSaveFile, int maxitr, int freq, double eta, double C, int threadNum, int shrinkingSize,
			CRFOption.Algorithm algorithm) throws IOException {
		//创建模板文件：根据训练目标，由继承的子类实现getDefaultFeatureTemplate抽象函数实现
		String templFile = null;
		File tmpTemplate = File.createTempFile(buildTemplateFile(), ".txt");
		tmpTemplate.deleteOnExit();
		templFile = tmpTemplate.getAbsolutePath();
		String template = getDefaultFeatureTemplate();
		IOUtil.saveTxt(templFile, template);

		//将给定语料文件或给定语料目录下的语料读取并转换为符合训练格式(CRF++实现的格式)的文件
		File tmpTrain = File.createTempFile(buildTrainCorpusFile(),".txt");
		tmpTrain.deleteOnExit();
		//从给定的训练语料中提取训练所需的格式后保存到临时的训练文件中
		convertCorpus(trainFile, tmpTrain.getAbsolutePath());
		trainFile = tmpTrain.getAbsolutePath();
		
		NlpLogger.warning("CRFTagger","Java效率低，建议安装CRF++，执行下列等价训练命令（不要终止本进程，否则临时语料库和特征模板将被清除）：\n" + "crf_learn -m {} -f {} -e {} -c {} -p {} -H {} -a {} -t {} {} {}\n",
				maxitr, freq, eta, C, threadNum, shrinkingSize, algorithm.toString().replace('_', '-'), templFile, trainFile, modelSaveFile);
		//通过Encoder.learn来完成CRF的训练
		Encoder encoder = new Encoder();
		if (!encoder.learn(templFile, trainFile, modelSaveFile, true, maxitr, freq, eta, C, threadNum, shrinkingSize, algorithm)) {
			throw new IOException("[CRF]训练失败");
		}
		//将CRF++格式转为HanLP格式
		convert(modelSaveFile);
	}

	/**
	 * 训练
	 *
	 * @param templFile     模板文件（必须，如果未确定，可以调用另外的同名函数来自动生成）
	 * @param trainFile     训练文件（单文件，如果为多文件，可以调用另外的同名函数来转换和合成）
	 * @param modelSaveFile     模型文件
	 * @param maxitr        最大迭代次数 默认10000次
	 * @param freq          特征最低频次 默认1
	 * @param eta           收敛阈值 默认为0.0001
	 * @param C             cost-factor 代价因子，默认为1.0
	 * @param threadNum     线程数 默认为计算机cpu线程数（非核数）
	 * @param shrinkingSize 收缩率，1~100（如果填写，则会剔除掉所填写的比例），默认为20
	 * @param algorithm     训练算法，支持两种算法：拟牛顿法和RIMA
	 * @return
	 */
	public void train(String templFile, String trainFile, String modelSaveFile, int maxitr, int freq, double eta, double C, int threadNum, int shrinkingSize,
			CRFOption.Algorithm algorithm) throws IOException {
		//通过Encoder.learn来完成CRF的训练
		Encoder encoder = new Encoder();
		if (!encoder.learn(templFile, trainFile, modelSaveFile, true, maxitr, freq, eta, C, threadNum, shrinkingSize, algorithm)) {
			throw new IOException("[CRF]训练失败");
		}
		//将CRF++格式转为HanLP格式
		convert(modelSaveFile);
	}

	/**
	 * 将给定句子的词条转换为训练用的格式(训练目标不同，需求不一样)
	 *
	 * hoobort
	 * 2022年10月11日 下午2:30:02
	 * @param sentence
	 * @param bw
	 * @throws IOException
	 */
	protected abstract void convertCorpus(Sentence sentence, BufferedWriter bw) throws IOException;

	/**
	 * 获取默认的模板内容(训练目标不同，需求不一样)
	 *
	 * hoobort
	 * 2022年10月11日 下午2:28:06
	 * @return
	 */
	protected abstract String getDefaultFeatureTemplate();

	public void convertCorpus(String pkuPath, String tsvPath) throws IOException {
		// 打开临时用训练语料文件
		final BufferedWriter bw = IOUtil.newBufferedWriter(tsvPath);
		try {
			// 读取语料文件内容并按照语料格式拆分成Sentence实例然后交由由子类实现的convertCorpus构造成训练所需的格式
			IOUtility.loadInstance(pkuPath, new InstanceHandler() {
				@Override
				public boolean process(Sentence sentence) {
					// 根据字符映射表对给定的语句中的词条统一转换（标准化）
					Utility.normalize(sentence);
					try {
						// 将给定句子的词条转换为训练用的格式
						convertCorpus(sentence, bw);
						bw.newLine();
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					return false;
				}
			});
		} finally {
			bw.close();
		}
	}

	/**
	 * 导出特征模板
	 *
	 * @param templatePath
	 * @throws IOException
	 */
	public void dumpTemplate(String templatePath) throws IOException {
		BufferedWriter bw = IOUtil.newBufferedWriter(templatePath);
		String template = getTemplate();
		bw.write(template);
		bw.close();
	}

	/**
	 * 获取特征模板
	 *
	 * @return
	 */
	public String getTemplate() {
		String template = getDefaultFeatureTemplate();
		if (model != null && model.getFeatureTemplateArray() != null) {
			StringBuilder sbTemplate = new StringBuilder();
			for (FeatureTemplate featureTemplate : model.getFeatureTemplateArray()) {
				sbTemplate.append(featureTemplate.getTemplate()).append('\n');
			}
		}
		return template;
	}
}
