package com.hankcs.hanlp.model.crf.crfpp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.model.crf.CRFOption;

/**
 * 训练入口
 *
 * @author zhifac, hoobort
 * @log 2022.10.10 hoobort 优化
 */
public class Encoder {
	private static String myName = Encoder.class.getSimpleName();

	public static int MODEL_VERSION = 100;

	public Encoder() {

	}

	private List<List<String[]>> readTagLineFromFile(String filePath) throws IOException {
		BufferedReader reader = null;
		List<List<String[]>> ret = new ArrayList<>();

		reader = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(filePath), "UTF-8"));
		String line = null;
		List<String[]> block = new ArrayList<String[]>();
		while (true) {
			line = reader.readLine();
			if (line == null) {
				if (block.size() > 0)
					ret.add(block);
				break;
			}
			if (line.length() == 0) {
				if (block.size() > 0) {
					ret.add(block);
					block = new ArrayList<String[]>();
				}
				continue;
			}

			block.add(line.split("[\t ]", -1));
		}
		reader.close();

		return ret;
	}

	private long fileLines(String filePath) throws IOException {
		BufferedReader reader = null;
		long ret = 0L;
		try {
			reader = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(filePath), "UTF-8"));
			ret = reader.lines().count();
		} finally {
			reader.close();
		}
		return ret;
	}

	/**
	 * 训练<br/>
	 * 训练文件必须是如下的几种格式：<br/>
	 * 【词典训练】<br/>
	 * [词条]\[标注集标签如SBME]<br/>
	 * 【词性标注训练】<br/>
	 * [词条名]\t[词条第一个字]\t[词条前两字]\[词条最后一个字]\[词条最后两个字]\[词性]<br/>
	 * 【命名实体识别训练】<br/>
	 * 由多行构成，每行表示一个词条，格式为：[词条名]\t[词性]\t[标注集标签]
	 *
	 * @param templFile     模板文件
	 * @param trainFile     训练文件
	 * @param modelFile     模型文件
	 * @param textModelFile 是否输出文本形式的模型文件
	 * @param maxitr        最大迭代次数
	 * @param freq          特征最低频次
	 * @param eta           收敛阈值
	 * @param C             cost-factor
	 * @param threadNum     线程数
	 * @param shrinkingSize
	 * @param algorithm     训练算法
	 * @return
	 */
	public boolean learn(String templFile, String trainFile, String modelFile, boolean textModelFile, int maxitr, int freq, double eta, double C,
			int threadNum, int shrinkingSize, CRFOption.Algorithm algorithm) {
		String error = null;
		if (eta <= 0)
			error = "收敛阈值必须大于0";
		else if (C < 0.0)
			error = "代价因子必须大于0";
		else if (threadNum <= 0)
			error = "只要需要一个线程来执行训练";
		else if (algorithm == CRFOption.Algorithm.MIRA) {
			if (shrinkingSize < 1)
				error = "使用MIRA算法要求压缩尺寸必须至少为1";
		}
		if (error != null) {
			NlpLogger.error(myName, error);
			return false;
		}
		// 手工清理内存
		System.gc();

		EncoderFeatureIndex featureIndex = new EncoderFeatureIndex();
		if (!featureIndex.open(templFile, trainFile))
			NlpLogger.warning(myName, "打开文件失败：{} - {}", templFile, trainFile);

		long t1 = 0, t2 = 0;
		List<TaggerImpl> x = new ArrayList<TaggerImpl>();
		try {
			NlpLogger.info(myName, "正在从[{}]中读取训练语料", trainFile);
			t1 = System.currentTimeMillis();
			List<List<String[]>> crfLines = readTagLineFromFile(trainFile);
			long lineCount = crfLines.size();
			int step = lineCount < 100 ? (int) lineCount : (int) Math.ceil(lineCount / 100.0);

			t2 = System.currentTimeMillis();
			NlpLogger.info(myName, "读取训练完成，共 {} 条,耗时 {} 毫秒。准备将语料转换为可训练的特征", lineCount, t2 - t1);
			t1 = t2;
			int lineNo = 0;
			for (List<String[]> crfLine : crfLines) {
				TaggerImpl tagger = new TaggerImpl(TaggerImpl.Mode.LEARN);
				tagger.open(featureIndex);
				// 读取一组数据
				if (!tagger.read(crfLine)) {
					NlpLogger.warning(myName, "转换训练语料为可训练特征失败：{}", trainFile);
					return false;
				}

				if (tagger.empty())
					continue;

				if (!tagger.shrink()) {
					NlpLogger.warning(myName, "构建CRF训练特征失败！ ");
					return false;
				}
				tagger.setThread_id_(lineNo % threadNum);
				x.add(tagger);

				lineNo++;
				if (((lineNo % step) == 0) || (lineNo == lineCount))
					NlpLogger.info(myName, "[CRF]已完成 {}%", lineNo * 100 / lineCount);
			}
			crfLines = null;
		} catch (IOException e) {
			NlpLogger.error(myName, "训练语料{}不存在！", trainFile);
			return false;
		}

		t2 = System.currentTimeMillis();
		NlpLogger.info(myName, "训练特征转换完成，耗时 {} 毫秒。开始对转换的结果特征根据指定的最低词频过滤特征值...", t2 - t1);
		System.gc();// 手工清理内存
		t1 = t2;
		featureIndex.shrink(freq, x);
		t2 = System.currentTimeMillis();
		NlpLogger.info(myName, "训练特征过滤完成, 耗时 {} 毫秒", t2 - t1);
		t1 = t2;

		double[] weightArray = new double[featureIndex.size()];
		Arrays.fill(weightArray, 0.0);
		featureIndex.setWeight(weightArray);

		System.gc(); // 手工清理内存

		NlpLogger.info(myName,
				new StringBuilder()
					.append("句子总数: ").append(x.size())
					.append("\n特征总数:  ").append(featureIndex.size())
					.append("\n线程数量: ").append(threadNum)
					.append("\n迭代次数: ").append(maxitr)
					.append("\n词频阈值: ").append(freq)
					.append("\n收敛阈值: ").append(eta)
					.append("\n代价因子: ").append(C)
					.append("\n压缩比例: ").append(shrinkingSize)
						.toString());

		boolean ret = true;
		switch (algorithm) {
		case CRF_L1:
			ret=runCRF(x, featureIndex, weightArray, maxitr, C, eta, shrinkingSize, threadNum, true);
			break;
		case CRF_L2:
			ret=runCRF(x, featureIndex, weightArray, maxitr, C, eta, shrinkingSize, threadNum, false);
			break;
		case MIRA:
			ret=runMIRA(x, featureIndex, weightArray, maxitr, C, eta, shrinkingSize, threadNum);
			break;
		default:
			break;
		}
		
		if(!ret)
			NlpLogger.warning(myName, "使用{}算法训练失败",algorithm.toString());
		else {
			if (!featureIndex.save(modelFile, textModelFile)) {
				NlpLogger.error(myName, "训练完成！ 耗时 {} 毫秒,但存储训练结果模型到文件“{}”失败", System.currentTimeMillis() - t1, modelFile);
			} else {
				NlpLogger.info(myName, "训练完成！ 耗时 {} 毫秒", System.currentTimeMillis() - t1);
			}
		}

		return true;
	}

	/**
	 * 
	 * 老代码，准备用新的learn代替
	 * 
	 */
	@Deprecated
	public boolean learn_origin(String templFile, String trainFile, String modelFile, boolean textModelFile, int maxitr, int freq, double eta, double C, int threadNum,
			int shrinkingSize, CRFOption.Algorithm algorithm) {
		String error = null;
		if (eta <= 0)
			error = "收敛阈值必须大于0";
		else if (C < 0.0)
			error = "代价因子必须大于0";
		else if (threadNum <= 0)
			error = "只要需要一个线程来执行训练";
		else if (algorithm == CRFOption.Algorithm.MIRA) {
			if (shrinkingSize < 1)
				error = "使用MIRA算法要求压缩尺寸必须至少为1";
		}
		if (error != null) {
			NlpLogger.error(myName, error);
			return false;
		}

		// 手工清理内存
		System.gc();

		EncoderFeatureIndex featureIndex = new EncoderFeatureIndex();
		if (!featureIndex.open(templFile, trainFile))
			NlpLogger.warning(myName, "打开文件失败：{} - {}", templFile, trainFile);

		NlpLogger.info(myName, "正在从[{}]中读取训练语料", trainFile);
		long t1 = 0, t2 = 0;
		BufferedReader fileReader = null;
		List<TaggerImpl> x = new ArrayList<TaggerImpl>();
		try {
			long lineCount = fileLines(trainFile);
			int step = lineCount < 1000 ? (int) lineCount : (int) Math.ceil(lineCount / 1000.0);
			fileReader = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(trainFile), "UTF-8"));

			t2 = System.currentTimeMillis();
			NlpLogger.info(myName, "正在读取训练语料并转换为可训练的特征，行数：{}，耗时 {} 毫秒", lineCount, t2 - t1);
			t1 = t2;
			int lineNo = 0;
			while (true) {
				TaggerImpl tagger = new TaggerImpl(TaggerImpl.Mode.LEARN);
				tagger.open(featureIndex);
				// 读取一组数据
				TaggerImpl.ReadStatus status = tagger.read(fileReader);
				if (status == TaggerImpl.ReadStatus.ERROR) {
					NlpLogger.error(myName, "读取训练语料失败：{}", trainFile);
					return false;
				}
				if (!tagger.empty()) {
					if (!tagger.shrink()) {
						NlpLogger.error(myName, "构建词条训练特征失败！ ");
						return false;
					}
					tagger.setThread_id_(lineNo % threadNum);
					x.add(tagger);
				} else if (status == TaggerImpl.ReadStatus.EOF) {
					break;
				} else {
					continue;
				}
				++lineNo;
				if ((lineNo % step == 0) || (lineNo == lineCount))
					NlpLogger.info(myName, "[CRF]已完成 {}% ...", lineNo * 100 / lineCount);
			}
		} catch (IOException e) {
			NlpLogger.error(myName, "训练语料{}不存在！", trainFile);
			return false;
		} finally {
			IOUtil.close(fileReader);
		}

		t2 = System.currentTimeMillis();
		NlpLogger.info(myName, "转换完成，耗时 {} 毫秒，正在根据指定的最低词频裁剪待训练的特征值...", t2 - t1);
		t1 = t2;
		featureIndex.shrink(freq, x);


		double[] weightArray = new double[featureIndex.size()];
		Arrays.fill(weightArray, 0.0);
		featureIndex.setWeight(weightArray);

		NlpLogger.info(myName,
				new StringBuilder().append("句子总数: ").append(x.size())
				.append("\n特征总数:  ").append(featureIndex.size())
				.append("\n线程数量: ").append(threadNum)
				.append("\n最低词频: ").append(freq)
				.append("\n收敛阈值: ").append(eta)
				.append("\n代价因子: ").append(C)
				.append("\n压缩比例: ").append(shrinkingSize)
				.toString());

		t2 = System.currentTimeMillis();
		NlpLogger.info(myName, "裁剪完成，耗时 {} 毫秒，开始训练模型...", t2 - t1);
		t1 = t2;
		boolean ret = true;
		switch (algorithm) {
		case CRF_L1:
			ret = runCRF(x, featureIndex, weightArray, maxitr, C, eta, shrinkingSize, threadNum, true);
			break;
		case CRF_L2:
			ret = runCRF(x, featureIndex, weightArray, maxitr, C, eta, shrinkingSize, threadNum, false);
			break;
		case MIRA:
			ret = runMIRA(x, featureIndex, weightArray, maxitr, C, eta, shrinkingSize, threadNum);
			break;
		default:
			break;
		}

		if (!ret){
			NlpLogger.error(myName, "使用{}算法训练失败", algorithm.toString());
		} else {
			ret = featureIndex.save(modelFile, textModelFile);
			if (ret)
				NlpLogger.error(myName, "训练结束，共耗时{}毫秒，但存储训练结果模型到文件“{}”失败", System.currentTimeMillis() - t1, modelFile);
			else
				NlpLogger.info(myName, "训练完成！耗时 {} 毫秒", System.currentTimeMillis() - t1);
		}

		return ret;
	}

	/**
	 * CRF训练
	 *
	 * @param x             句子列表
	 * @param featureIndex  特征编号表
	 * @param weights       特征函数的代价
	 * @param maxItr        最大迭代次数
	 * @param C             cost factor
	 * @param eta           收敛阈值
	 * @param shrinkingSize 未使用
	 * @param threadNum     线程数
	 * @param orthant       是否使用L1范数
	 * @return 是否成功
	 */
	private boolean runCRF(List<TaggerImpl> x, EncoderFeatureIndex featureIndex, double[] weights, int maxItr, double C, double eta, int shrinkingSize,
			int threadNum, boolean orthant) {
		double oldObj = 1e+37;
		int converge = 0;
		LbfgsOptimizer lbfgs = new LbfgsOptimizer();
		List<CRFEncoderThread> threads = new ArrayList<CRFEncoderThread>();

		for (int i = 0; i < threadNum; i++) {
			CRFEncoderThread thread = new CRFEncoderThread(weights.length);
			thread.start_i = i;
			thread.size = x.size();
			thread.threadNum = threadNum;
			thread.x = x;
			threads.add(thread);
		}

		int all = 0;
		for (int i = 0; i < x.size(); i++)
			all += x.get(i).size();

		ExecutorService executor = Executors.newFixedThreadPool(threadNum);
		for (int itr = 0; itr < maxItr; itr++) {
			featureIndex.clear();

			try {
				executor.invokeAll(threads);
			} catch (Exception e) {
				NlpLogger.error(myName, e);
				return false;
			}

			for (int i = 1; i < threadNum; i++) {
				threads.get(0).obj += threads.get(i).obj;
				threads.get(0).err += threads.get(i).err;
				threads.get(0).zeroone += threads.get(i).zeroone;
			}
			for (int i = 1; i < threadNum; i++) {
				for (int k = 0; k < featureIndex.size(); k++)
					threads.get(0).expected[k] += threads.get(i).expected[k];
			}
			int numNonZero = 0;
			if (orthant) {
				for (int k = 0; k < featureIndex.size(); k++) {
					threads.get(0).obj += Math.abs(weights[k] / C);
					if (weights[k] != 0.0)
						numNonZero++;
				}
			} else {
				numNonZero = featureIndex.size();
				for (int k = 0; k < featureIndex.size(); k++) {
					threads.get(0).obj += (weights[k] * weights[k] / (2.0 * C));
					threads.get(0).expected[k] += weights[k] / C;
				}
			}
			for (int i = 1; i < threadNum; i++) {
				threads.get(i).expected = null;
			}

			double diff = (itr == 0 ? 1.0 : Math.abs(oldObj - threads.get(0).obj) / oldObj);

			NlpLogger.info(myName, "iter={} terr={} serr={} act={} obj={} diff={}", itr, 1.0 * threads.get(0).err / all,
					1.0 * threads.get(0).zeroone / x.size(), numNonZero, threads.get(0).obj, diff);

			oldObj = threads.get(0).obj;
			converge += (diff < eta ? 1 : 0);

			if (itr > maxItr || converge == 3)
				break;

			int ret = lbfgs.optimize(featureIndex.size(), weights, threads.get(0).obj, threads.get(0).expected, orthant, C);
			if (ret <= 0)
				return false;
		}
		executor.shutdown();
		try {
			executor.awaitTermination(-1, TimeUnit.SECONDS);
		} catch (Exception e) {
			NlpLogger.error(myName, "训练线程结束失败", e);
		}
		return true;
	}

	public boolean runMIRA(List<TaggerImpl> x, EncoderFeatureIndex featureIndex, double[] alpha, int maxItr, double C, double eta, int shrinkingSize,
			int threadNum) {
		Integer[] shrinkArr = new Integer[x.size()];
		Arrays.fill(shrinkArr, 0);
		List<Integer> shrink = Arrays.asList(shrinkArr);
		Double[] upperArr = new Double[x.size()];
		Arrays.fill(upperArr, 0.0);
		List<Double> upperBound = Arrays.asList(upperArr);
		Double[] expectArr = new Double[featureIndex.size()];
		List<Double> expected = Arrays.asList(expectArr);

		if (threadNum > 1) {
			NlpLogger.warning(myName, "WARN: MIRA does not support multi-threading");
		}
		int converge = 0;
		int all = 0;
		for (int i = 0; i < x.size(); i++) {
			all += x.get(i).size();
		}

		for (int itr = 0; itr < maxItr; itr++) {
			int zeroone = 0;
			int err = 0;
			int activeSet = 0;
			int upperActiveSet = 0;
			double maxKktViolation = 0.0;

			for (int i = 0; i < x.size(); i++) {
				if (shrink.get(i) >= shrinkingSize) {
					continue;
				}
				++activeSet;
				for (int t = 0; t < expected.size(); t++) {
					expected.set(t, 0.0);
				}
				double costDiff = x.get(i).collins(expected);
				int errorNum = x.get(i).eval();
				err += errorNum;
				if (errorNum != 0) {
					++zeroone;
				}
				if (errorNum == 0) {
					shrink.set(i, shrink.get(i) + 1);
				} else {
					shrink.set(i, 0);
					double s = 0.0;
					for (int k = 0; k < expected.size(); k++) {
						s += expected.get(k) * expected.get(k);
					}
					double mu = Math.max(0.0, (errorNum - costDiff) / s);

					if (upperBound.get(i) + mu > C) {
						mu = C - upperBound.get(i);
						upperActiveSet++;
					} else {
						maxKktViolation = Math.max(errorNum - costDiff, maxKktViolation);
					}

					if (mu > 1e-10) {
						upperBound.set(i, upperBound.get(i) + mu);
						upperBound.set(i, Math.min(C, upperBound.get(i)));
						for (int k = 0; k < expected.size(); k++) {
							alpha[k] += mu * expected.get(k);
						}
					}
				}
			}
			double obj = 0.0;
			for (int i = 0; i < featureIndex.size(); i++) {
				obj += alpha[i] * alpha[i];
			}

			NlpLogger.info(myName, "iter={} terr={} serr={} act={} uact={} obj={} kkt={}", itr, 1.0 * err / all, 1.0 * zeroone / x.size(), activeSet,
					upperActiveSet, obj, maxKktViolation);

			if (maxKktViolation <= 0.0) {
				for (int i = 0; i < shrink.size(); i++) {
					shrink.set(i, 0);
				}
				converge++;
			} else {
				converge = 0;
			}
			if (itr > maxItr || converge == 2) {
				break;
			}
		}
		return true;
	}

}
