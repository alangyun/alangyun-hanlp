/*
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2017-10-26 下午5:51</create-date>
 *
 * <copyright file="PerceptronTrainer.java" company="码农场">
 * Copyright (c) 2017, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.model.perceptron;

import com.hankcs.hanlp.model.common.IOUtility;
import com.hankcs.hanlp.model.common.Utility;
import com.hankcs.hanlp.model.feature.ImmutableFeatureMap;
import com.hankcs.hanlp.model.feature.MutableFeatureMap;
import com.hankcs.hanlp.model.instance.Instance;
import com.hankcs.hanlp.model.instance.InstanceHandler;
import com.hankcs.hanlp.model.linear.LinearModel;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.tagger.tag.TagSet;
import com.hankcs.hanlp.meta.word.Sentence;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * 感知机训练基类
 *
 * @author hankcs, hoobort
 */
public abstract class PerceptronTrainer extends InstanceConsumer {
	/** 日志 * */
	private static String myName = PerceptronTrainer.class.getSimpleName();

	/**
	 * 创建标注集
	 *
	 * @return
	 */
	protected abstract TagSet createTagSet();

	public Result train(String trainingFile, String modelFile) throws IOException {
		return train(trainingFile, trainingFile, modelFile);
	}

	public Result train(String trainingFile, String developFile, String modelFile) throws IOException {
		return train(trainingFile, developFile, modelFile, 0.1, 50, Runtime.getRuntime().availableProcessors());
	}
	
	/**
	 * 训练
	 *
	 * @param trainingFile  训练集(可以是单语料文件，也可以是语料所在目录。语料必须经过标注的语料，格式参见人民日报语料标注)
	 * @param developFile   开发集,如果为空，则设置为训练集
	 * @param modelFile     模型保存路径
	 * @param compressRatio 压缩比
	 * @param maxIteration  最大迭代次数
	 * @param threadNum     线程数
	 * @return 一个包含模型和精度的结构
	 * @throws IOException
	 */
	public Result train(String trainingFile, String developFile, String modelFile, final double compressRatio, final int maxIteration, final int threadNum)
			throws IOException {
		if (developFile == null)
			developFile = trainingFile;
		
		// 加载训练语料
		TagSet tagSet = createTagSet();
		MutableFeatureMap mutableFeatureMap = new MutableFeatureMap(tagSet);
		NlpLogger.info(myName, "开始加载训练集...");
		//加载语料并生成训练用的特征集
		Instance[] instances = loadTrainCorpus(trainingFile, mutableFeatureMap);
		//锁定加载的特征标注集
		tagSet.lockIndexTable();
		NlpLogger.info(myName, "加载完毕，一共{}句，特征总数{}", instances.length, mutableFeatureMap.size() * tagSet.size());

		// 开始训练
		ImmutableFeatureMap immutableFeatureMap = new ImmutableFeatureMap(mutableFeatureMap.featureIdMap, tagSet);
		mutableFeatureMap = null;
		double[] accuracy = null;

		if (threadNum == 1) {
			AveragedLinearModel model;
			model = new AveragedLinearModel(immutableFeatureMap);
			final double[] total = new double[model.parameter.length];
			final int[] timestamp = new int[model.parameter.length];
			int current = 0;
			for (int iter = 1; iter <= maxIteration; iter++) {
				Utility.shuffleArray(instances);
				for (Instance instance : instances) {
					++current;
					int[] guessLabel = new int[instance.length()];
					model.viterbiDecode(instance, guessLabel);
					for (int i = 0; i < instance.length(); i++) {
						int[] featureVector = instance.getFeatureAt(i);
						int[] goldFeature = new int[featureVector.length];
						int[] predFeature = new int[featureVector.length];
						for (int j = 0; j < featureVector.length - 1; j++) {
							goldFeature[j] = featureVector[j] * tagSet.size() + instance.tagIndexArray[i];
							predFeature[j] = featureVector[j] * tagSet.size() + guessLabel[i];
						}
						goldFeature[featureVector.length - 1] = (i == 0 ? tagSet.newSequenceId() : instance.tagIndexArray[i - 1]) * tagSet.size()
								+ instance.tagIndexArray[i];
						predFeature[featureVector.length - 1] = (i == 0 ? tagSet.newSequenceId() : guessLabel[i - 1]) * tagSet.size() + guessLabel[i];
						model.update(goldFeature, predFeature, total, timestamp, current);
					}
				}

				// 在开发集上校验
				accuracy = trainingFile.equals(developFile) ? IOUtility.evaluate(instances, model) : evaluate(developFile, model);
				NlpLogger.info(myName, "Iter#{} - ", iter);
				printAccuracy(accuracy);
			}
			// 平均
			model.average(total, timestamp, current);
			accuracy = trainingFile.equals(developFile) ? IOUtility.evaluate(instances, model) : evaluate(developFile, model);
			NlpLogger.info(myName, "AP - ");
			printAccuracy(accuracy);
			NlpLogger.info(myName, "以压缩比 {} 保存模型到 {} ... ", compressRatio, modelFile);
			model.save(modelFile, immutableFeatureMap.featureIdMap.entrySet(), compressRatio);
			NlpLogger.info(myName, " 保存完毕\n");
			if (compressRatio == 0)
				return new Result(model, accuracy);
		} else {
			// 多线程用Structure Perceptron
			StructuredLinearModel[] models = new StructuredLinearModel[threadNum];
			for (int i = 0; i < models.length; i++) {
				models[i] = new StructuredLinearModel(immutableFeatureMap);
			}

			TrainingWorker[] workers = new TrainingWorker[threadNum];
			int job = instances.length / threadNum;
			for (int iter = 1; iter <= maxIteration; iter++) {
				Utility.shuffleArray(instances);
				try {
					for (int i = 0; i < workers.length; i++) {
						workers[i] = new TrainingWorker(instances, i * job, i == workers.length - 1 ? instances.length : (i + 1) * job, models[i]);
						workers[i].start();
					}
					for (TrainingWorker worker : workers) {
						worker.join();
					}
					for (int j = 0; j < models[0].parameter.length; j++) {
						for (int i = 1; i < models.length; i++) {
							models[0].parameter[j] += models[i].parameter[j];
						}
						models[0].parameter[j] /= threadNum;
					}
					accuracy = trainingFile.equals(developFile) ? IOUtility.evaluate(instances, models[0]) : evaluate(developFile, models[0]);
					NlpLogger.info(myName, "Iter#{} - ", iter);
					printAccuracy(accuracy);
				} catch (InterruptedException e) {
					NlpLogger.error("线程同步异常，训练失败", e);
					return null;
				}
			}
			NlpLogger.info(myName, "以压缩比{}保存模型到{} ... ", compressRatio, modelFile);
			models[0].save(modelFile, immutableFeatureMap.featureIdMap.entrySet(), compressRatio);
			NlpLogger.info(myName, " 保存完毕\n");
			if (compressRatio == 0)
				return new Result(models[0], accuracy);
		}

		LinearModel model = new LinearModel(modelFile);
		if (compressRatio > 0) {
			accuracy = evaluate(developFile, model);
			NlpLogger.info(myName, "{} compressed model - ", compressRatio);
			printAccuracy(accuracy);
		}

		return new Result(model, accuracy);
	}

	private void printAccuracy(double[] accuracy) {
		if (accuracy.length == 3)
			NlpLogger.info(myName, "P:{} R:{} F:{}", accuracy[0], accuracy[1], accuracy[2]);
		else 
			NlpLogger.info(myName, "P:{}", accuracy[0]);
	}

	protected Instance[] loadTrainCorpus(String trainingFile, final MutableFeatureMap mutableFeatureMap) throws IOException {
		final List<Instance> instanceList = new LinkedList<Instance>();
		IOUtility.loadInstance(trainingFile, new InstanceHandler() {
			@Override
			public boolean process(Sentence sentence) {
				Utility.normalize(sentence);
				instanceList.add(PerceptronTrainer.this.createInstance(sentence, mutableFeatureMap));
				return false;
			}
		});
		Instance[] instances = new Instance[instanceList.size()];
		instanceList.toArray(instances);
		
		return instances;
	}

	/**
	 * 训练线程
	 * com.hankcs.hanlp.model.perceptron.PerceptronTrainer.java
	 * @author hoobort
	 * @email klxukun@126.com
	 * @unit 北京诚朗信息技术有限公司
	 * 2022年10月15日 下午8:12:43
	 *
	 */
	private static class TrainingWorker extends Thread {
		private Instance[] instances;
		private int start;
		private int end;
		private StructuredLinearModel model;

		public TrainingWorker(Instance[] instances, int start, int end, StructuredLinearModel model) {
			this.instances = instances;
			this.start = start;
			this.end = end;
			this.model = model;
		}

		@Override
		public void run() {
			for (int s = start; s < end; ++s) {
				Instance instance = instances[s];
				model.update(instance);
			}
		}
	}

	/** 训练结果 */
	public static class Result {
		/** 模型 */
		LinearModel model;
		/**
		 * 精确率(Precision), 召回率(Recall)和F1-Measure<br>
		 * 中文参考：https://blog.argcv.com/articles/1036.c
		 */
		double prf[];

		public Result(LinearModel model, double[] prf) {
			this.model = model;
			this.prf = prf;
		}

		/**
		 * 获取准确率
		 *
		 * @return
		 */
		public double getAccuracy() {
			if (prf.length == 3) {
				return prf[2];
			}
			return prf[0];
		}

		/**
		 * 获取模型
		 *
		 * @return
		 */
		public LinearModel getModel() {
			return model;
		}
	}

}
