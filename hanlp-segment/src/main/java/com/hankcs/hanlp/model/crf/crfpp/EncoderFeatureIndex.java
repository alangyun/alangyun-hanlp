package com.hankcs.hanlp.model.crf.crfpp;

import com.hankcs.hanlp.collection.trie.datrie.IntArrayList;
import com.hankcs.hanlp.collection.trie.datrie.MutableDoubleArrayTrieInteger;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author zhifac, hoobort
 */
class EncoderFeatureIndex extends FeatureIndex {
	private static String myName = EncoderFeatureIndex.class.getSimpleName();
	
	/** 可变双数组trie树(数组存储的是键值对KeyValuePair，其中key为特征名如字/词条，value为特征的ID（或叫索引） * */
	private MutableDoubleArrayTrieInteger dic_;
	/** 特征集的特征词频（数组下标即为特征的ID（或叫索引），数值值为特征的词频 * */
	private IntArrayList frequency;
	/** 上一特征索引 * */
	private int priorId = Integer.MAX_VALUE;

	public EncoderFeatureIndex() {
		dic_ = new MutableDoubleArrayTrieInteger();
		frequency = new IntArrayList();
	}

	/**
	 * 生成特征索引<br/>
	 * 如果为B，则ID为size^2,否则为size
	 */
	protected int generateID(String key) {
		int k = dic_.get(key);
		if (k == -1) {
			dic_.put(key, maxId);
			frequency.append(1);
			int n = maxId;
			if (key.charAt(0) == 'U') {
				maxId += rowLabel.size();
			} else {
				priorId = n;
				maxId += rowLabel.size() * rowLabel.size();
			}
			return n;
		} else {
			int cid = continuousId(k);
			int oldVal = frequency.get(cid);
			frequency.set(cid, oldVal + 1);
			return k;
		}
	}

	private int continuousId(int id) {
		if (id <= priorId) {
			return id / rowLabel.size();
		} else {
			return id / rowLabel.size() - rowLabel.size() + 1;
		}
	}

	/**
	 * 读取特征模板文件,一般格式如下：<br/>
	 * # Unigram\n"<br/>
	 * U0:%x[-1,0]<br/>
	 * U1:%x[0,0]<br/>
	 * U2:%x[1,0]<br/>
	 * U3:%x[0,1]<br/>
	 * U4:%x[0,2]<br/>
	 * U5:%x[0,3]<br/>
	 * U6:%x[0,4]<br/>
	 * 
	 * # Bigram<br/>
	 * B<br/>
	 *
	 * @param templateFilename CRF模板文件
	 * @return
	 */
	private boolean openTemplate(String templateFilename) {
		BufferedReader br = null;
		boolean ret = true;
		try {
			br = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(templateFilename), "UTF-8"));
			String line;
			while ((line = br.readLine()) != null) {
				if (line.length() == 0)
					continue;
				
				char c = line.charAt(0);
				if (c == '\0' || c == ' ' || c == '#') 
					continue;
				
				else if (c == 'U') 
					unigramTemplates.add(line.trim());
				else if (c == 'B') 
					bigramTemplates.add(line.trim());
				else {
					ret = false;
					NlpLogger.warning(myName, "CRF模板格式不符合要求: {}", line);	
					break;
				}
			}
		} catch (Exception e) {
			NlpLogger.error(myName, "打开CRF模板文件“{}”失败。", templateFilename, e);
		}finally {
			IOUtil.close(br);
		}
		return ret;
	}

	/**
	 * 读取训练文件中的标注集
	 *
	 * @param modelFile 特征集文件
	 * @return
	 */
	private boolean openFeature(String modelFile) {
		boolean ret = true;
		int max_size = 0;
		BufferedReader br = null;
		rowLabel.clear();
		Set<String> myset = new HashSet<>();
		try {
			br = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(modelFile), "UTF-8"));
			String line;
			while ((line = br.readLine()) != null) {
				if (line.length() == 0)
					continue;
				
				char firstChar = line.charAt(0);
				if (firstChar == '\0' || firstChar == ' ' || firstChar == '\t') 
					continue;
				
				String[] cols = line.split("[\t ]", -1);
				//训练集的格式长度是固定的，如果长度不一致表示特征集数据错误
				if (max_size == 0)
					max_size = cols.length;				
				if (max_size != cols.length) 
					throw new RuntimeException("特征文件“"+modelFile+"”中的特征集合长度不一致：应该包含"+max_size+"组，实际是"+cols.length+"组。");
				
				colSize = cols.length - 1;
				//从训练语料中读取提取特征
				//如果是词典训练：读取的值为BMES标签的其中之一
				//如果是词性模型训练，读取的值为词性标签
				//如果是命名实体训练，读取的值为命名实体标签前缀
				//(O_TAG,O_TAG_CHAR, B_TAG_PREFIX ,B_TAG_CHAR,M_TAG_PREFIX, E_TAG_PREFIX, S_TAG, S_TAG_CHAR)+词性的其中之一
				String last = cols[max_size - 1];
				if(myset.add(last)) 
					rowLabel.add(last);
				
			}
			Collections.sort(rowLabel);
		} catch (Exception e) {
			NlpLogger.error(myName, "读取CRF模型文件“{}”提取特征失败", modelFile, e);
			ret= false;
		}finally {
			IOUtil.close(br);
		}
		return ret;
	}

	public boolean open(String filename1, String filename2) {
		needCheckMaxColSize(true);
		return openTemplate(filename1) && openFeature(filename2);
	}

	public boolean save(String filename, boolean textModelFile) {
		ObjectOutputStream oos= null;
		OutputStreamWriter osw = null;
		try {
			oos = new ObjectOutputStream(IOUtil.newOutputStream(filename));
			oos.writeObject(Encoder.MODEL_VERSION); //版本号
			oos.writeObject(costFactor);//代价因子
			oos.writeObject(maxId);//最大索引号（或叫IDD）
			if (getMaxColSize() > 0)
				colSize = Math.min(colSize, getMaxColSize());

			oos.writeObject(colSize);//长度
			oos.writeObject(rowLabel);//特征数据
			oos.writeObject(unigramTemplates);//U模板
			oos.writeObject(bigramTemplates);//B模板
			oos.writeObject(dic_);//字典
			oos.writeObject(weight);//alpha
			oos.flush();
			//如果指定需要保存文本格式文件，则写入一份文本格式文件
			if (textModelFile) {
				osw = new OutputStreamWriter(IOUtil.newOutputStream(filename + ".txt"), "UTF-8");
				osw.write("version: " + Encoder.MODEL_VERSION + "\n");
				osw.write("cost-factor: " + costFactor + "\n");
				osw.write("maxid: " + maxId + "\n");
				osw.write("xsize: " + colSize + "\n");
				osw.write("\n");
				for (String y : rowLabel) {
					osw.write(y + "\n");
				}
				osw.write("\n");
				for (String utempl : unigramTemplates) {
					osw.write(utempl + "\n");
				}
				for (String bitempl : bigramTemplates) {
					osw.write(bitempl + "\n");
				}
				osw.write("\n");
				for (MutableDoubleArrayTrieInteger.KeyValuePair pair : dic_) {
					osw.write(pair.getValue() + " " + pair.getKey() + "\n");
				}
				osw.write("\n");

				for (int k = 0; k < maxId; k++) {
					String val = new DecimalFormat("0.0000000000000000").format(weight[k]);
					osw.write(val + "\n");
				}
				osw.flush();
			}
		} catch (Exception e) {
			NlpLogger.error(myName, "存储模型到文件“{}”失败", filename, e);
			return false;
		} finally {
			IOUtil.close(oos);
			IOUtil.close(osw);
		}
		return true;
	}

	public void clear() {

	}

	/**
	 * 根据指定的最小频率阈值对给定的特征集裁剪<br/>
	 *
	 * hoobort
	 * 2022年10月15日 上午11:29:13
	 * @param freq 最低频率阈值
	 * @param taggers 特征集
	 */
	public void shrink(int freq, List<TaggerImpl> taggers) {
		if (freq <= 1)
			return;

		int newMaxId = 0;
		Map<Integer, Integer> old2new = new TreeMap<Integer, Integer>();
		List<String> deletedKeys = new ArrayList<String>(dic_.size() / 8);

		// 遍历特征值，将不符合最低阈值要求的特征删除，符合条件的特征重新计算ID（索引位置）
		for (MutableDoubleArrayTrieInteger.KeyValuePair pair : dic_) {
			String key = pair.key();
			int id = pair.value();
			int cid = continuousId(id);
			int f = frequency.get(cid);
			if (f >= freq) {
				old2new.put(id, newMaxId);
				pair.setValue(newMaxId);
				newMaxId += (key.charAt(0) == 'U' ? rowLabel.size() : rowLabel.size() * rowLabel.size());
			} else {
				deletedKeys.add(key);
			}
		}
		//删除不符合条件的特征值
		for (String key : deletedKeys)
			dic_.remove(key);
		// 重新构造？这是干嘛用的？？
		for (TaggerImpl tagger : taggers) {
			List<List<Integer>> featureCache = tagger.getFeatureCache_();
			for (int k = 0; k < featureCache.size(); k++) {
				List<Integer> featureCacheItem = featureCache.get(k);
				List<Integer> newCache = new ArrayList<Integer>();
				for (Integer it : featureCacheItem) {
					if (it == -1)
						continue;

					Integer nid = old2new.get(it);
					if (nid != null)
						newCache.add(nid);
				}
				newCache.add(-1);
				featureCache.set(k, newCache);
			}
		}
		maxId = newMaxId;
	}

	/**
	 * 将文本文件模型转换为二进制模型文件
	 *
	 * hoobort
	 * 2022年10月15日 下午12:50:13
	 * @param textModelFile 文本模型文件
	 * @param binarymodelFile 二进制模型文件
	 * @return
	 */
	public boolean convert(String textModelFile, String binarymodelFile) {
		BufferedReader fileReader = null;
		try {
			fileReader = new BufferedReader(new InputStreamReader(IOUtil.newInputStream(textModelFile), "UTF-8"));
			String line;

			costFactor = Double.valueOf(fileReader.readLine().substring("cost-factor: ".length()));
			maxId = Integer.valueOf(fileReader.readLine().substring("maxid: ".length()));
			colSize = Integer.valueOf(fileReader.readLine().substring("xsize: ".length()));
			NlpLogger.info(myName, "读取文件基本信息完成");
			fileReader.readLine();//空行
			//循环读取标注方法
			while ((line = fileReader.readLine()) != null && line.length() > 0) {
				rowLabel.add(line);
			}
			NlpLogger.info(myName, "读取特征标注法完成");
			//读取特征模板
			while ((line = fileReader.readLine()) != null && line.length() > 0) {
				if (line.startsWith("U"))
					unigramTemplates.add(line);
				else if (line.startsWith("B"))
					bigramTemplates.add(line);
			}
			NlpLogger.info(myName, "读取特征模板内容完成");
			//读取特征值
			//特征值格式（行）：[索引ID][空格][特征模板键]：[特征键（如字、词条）]
			dic_ = new MutableDoubleArrayTrieInteger();
			while ((line = fileReader.readLine()) != null && line.length() > 0) {
				String[] content = line.trim().split(" ");
				if (content.length != 2) {
					NlpLogger.error(myName, "特征值格式错误，请检查模型文件");
					return false;
				}
				dic_.put(content[1], Integer.valueOf(content[0]));
			}
			NlpLogger.info(myName, "读取特征值内容完成");
			List<Double> weightList = new ArrayList<Double>();
			while ((line = fileReader.readLine()) != null && line.length() > 0) {
				weightList.add(Double.valueOf(line));
			}
			NlpLogger.info(myName, "读取特征权重完成（代价值）");
			weight = new double[weightList.size()];
			for (int i = 0; i < weightList.size(); i++) {
				weight[i] = weightList.get(i);
			}
			NlpLogger.info(myName, "写入二进制文件“{}”", binarymodelFile);
			return save(binarymodelFile, false);
		} catch (Exception e) {
			NlpLogger.error(myName,  e);
			return false;
		} finally {
			IOUtil.close(fileReader);
		}
	}

}
