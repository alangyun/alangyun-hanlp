package com.hankcs.hanlp.model.crf;

public class CRFConst {

	public static String[] BOS = { 
			"_B-1", 
			"_B-2", 
			"_B-3", 
			"_B-4", 
			"_B-5", 
			"_B-6", 
			"_B-7", 
			"_B-8" 
			};
	
	
	public static String[] EOS = { 
			"_B+1", 
			"_B+2", 
			"_B+3", 
			"_B+4", 
			"_B+5", 
			"_B+6", 
			"_B+7", 
			"_B+8" 
			};

}
