/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/11/17 19:34</create-date>
 *
 * <copyright file="PlaceRecognition.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.liNTunsoft.com/
 * This source is subject to the LiNTunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.seg.recog.nt;

import com.hankcs.hanlp.dictionary.nt.OrganizationDictionary;
import com.hankcs.hanlp.dictionary.wgraph.Vertex;
import com.hankcs.hanlp.dictionary.wgraph.WordNet;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.NatureFreqSet;
import com.hankcs.hanlp.meta.roletag.NT;
import com.hankcs.hanlp.meta.roletag.Nature;
import com.hankcs.hanlp.seg.viterbi.ViterbiAlgorithm;

import static com.hankcs.hanlp.meta.roletag.Nature.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * 地址识别
 *
 * @author hankcs
 */
public class OrganizationRecognition {
	public static boolean recognition(List<Vertex> pWordSegResult, WordNet wordNetOptimum, WordNet wordNetAll) {
		List<NatureFreqSet<NT>> roleTagList = roleTag(pWordSegResult, wordNetAll);
		if (NlpLogger.enableDebug()) {
			StringBuilder sbLog = new StringBuilder();
			Iterator<Vertex> iterator = pWordSegResult.iterator();
			for (NatureFreqSet<NT> NTEnumItem : roleTagList) {
				sbLog.append('[');
				sbLog.append(iterator.next().realWord);
				sbLog.append(' ');
				sbLog.append(NTEnumItem);
				sbLog.append(']');
			}
		}
		List<NT> NTList = viterbiCompute(roleTagList);
		if (NlpLogger.enableDebug()) {
			StringBuilder sbLog = new StringBuilder();
			Iterator<Vertex> iterator = pWordSegResult.iterator();
			sbLog.append('[');
			for (NT NT : NTList) {
				sbLog.append(iterator.next().realWord);
				sbLog.append('/');
				sbLog.append(NT);
				sbLog.append(" ,");
			}
			if (sbLog.length() > 1)
				sbLog.delete(sbLog.length() - 2, sbLog.length());
			sbLog.append(']');
		}

		OrganizationDictionary.parsePattern(NTList, pWordSegResult, wordNetOptimum, wordNetAll);
		return true;
	}

	public static List<NatureFreqSet<NT>> roleTag(List<Vertex> vertexList, WordNet wordNetAll) {
		List<NatureFreqSet<NT>> tagList = new LinkedList<NatureFreqSet<NT>>();
		
		for (Vertex vertex : vertexList) {
			// 构成更长的
			Nature nature = vertex.guessNature();
			if (nature == nrf) {
				if (vertex.getAttribute().totalFrequency <= 1000) {
					tagList.add(new NatureFreqSet<NT>(NT.F, 1000));
					continue;
				}
			} else if (nature == ni || nature == nic || nature == nis || nature == nit) {
				NatureFreqSet<NT> ntEnumItem = new NatureFreqSet<NT>(NT.K, 1000);
				ntEnumItem.addLabel(NT.D, 1000);
				tagList.add(ntEnumItem);
				continue;
			} else if (nature == m) {
				NatureFreqSet<NT> ntEnumItem = new NatureFreqSet<NT>(NT.M, 1000);
				tagList.add(ntEnumItem);
				continue;
			}

			NatureFreqSet<NT> NTEnumItem = OrganizationDictionary.dictionary.get(vertex.word); // 此处用等效词，更加精准
			if (NTEnumItem == null) {
				NTEnumItem = new NatureFreqSet<NT>(NT.Z, OrganizationDictionary.transformMatrixDictionary.getTotalFrequency(NT.Z));
			}
			tagList.add(NTEnumItem);
		}
		return tagList;
	}

	/**
	 * 维特比算法求解最优标签
	 *
	 * @param roleTagList
	 * @return
	 */
	public static List<NT> viterbiCompute(List<NatureFreqSet<NT>> roleTagList) {
		return ViterbiAlgorithm.computeEnum(roleTagList, OrganizationDictionary.transformMatrixDictionary);
	}
}
