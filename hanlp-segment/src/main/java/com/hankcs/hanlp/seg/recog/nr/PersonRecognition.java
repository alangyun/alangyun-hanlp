/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/05/2014/5/26 13:52</create-date>
 *
 * <copyright file="UnknowWord.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.seg.recog.nr;

import com.hankcs.hanlp.dictionary.nr.PersonDictionary;
import com.hankcs.hanlp.dictionary.wgraph.Vertex;
import com.hankcs.hanlp.dictionary.wgraph.WordNet;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.NatureFreqSet;
import com.hankcs.hanlp.meta.roletag.NR;
import com.hankcs.hanlp.meta.roletag.Nature;
import com.hankcs.hanlp.seg.viterbi.ViterbiAlgorithm;

import static com.hankcs.hanlp.meta.roletag.Nature.nnt;
import static com.hankcs.hanlp.meta.roletag.Nature.nr;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * 人名识别
 * 
 * @author hankcs
 */
public class PersonRecognition {
	private static String myName = PersonRecognition.class.getSimpleName();

	public static boolean recognition(List<Vertex> pWordSegResult, WordNet wordNetOptimum, WordNet wordNetAll) {
		List<NatureFreqSet<NR>> roleTagList = roleObserve(pWordSegResult);
		if (NlpLogger.enableDebug()) {
			StringBuilder sbLog = new StringBuilder();
			Iterator<Vertex> iterator = pWordSegResult.iterator();
			for (NatureFreqSet<NR> nrEnumItem : roleTagList) {
				sbLog.append('[');
				sbLog.append(iterator.next().realWord);
				sbLog.append(' ');
				sbLog.append(nrEnumItem);
				sbLog.append(']');
			}
			NlpLogger.debug(myName, "人名角色观察：{}", sbLog.toString());
		}
		List<NR> nrList = viterbiComputeSimply(roleTagList);
		if (NlpLogger.enableDebug()) {
			StringBuilder sbLog = new StringBuilder();
			Iterator<Vertex> iterator = pWordSegResult.iterator();
			sbLog.append('[');
			for (NR nr : nrList) {
				sbLog.append(iterator.next().realWord);
				sbLog.append('/');
				sbLog.append(nr);
				sbLog.append(" ,");
			}
			if (sbLog.length() > 1)
				sbLog.delete(sbLog.length() - 2, sbLog.length());
			sbLog.append(']');
			NlpLogger.debug(myName, "人名角色标注：{}", sbLog.toString());
		}

		PersonDictionary.parsePattern(nrList, pWordSegResult, wordNetOptimum, wordNetAll);
		return true;
	}

	/**
	 * 角色观察(从模型中加载所有词语对应的所有角色,允许进行一些规则补充)
	 * 
	 * @param wordSegResult 粗分结果
	 * @return
	 */
	public static List<NatureFreqSet<NR>> roleObserve(List<Vertex> wordSegResult) {
		List<NatureFreqSet<NR>> tagList = new LinkedList<NatureFreqSet<NR>>();
		Iterator<Vertex> iterator = wordSegResult.iterator();
		iterator.next();
		tagList.add(new NatureFreqSet<NR>(NR.A, NR.K)); // 始##始 A K
		while (iterator.hasNext()) {
			Vertex vertex = iterator.next();
			NatureFreqSet<NR> nrEnumItem = PersonDictionary.dictionary.get(vertex.realWord);
			if (nrEnumItem == null) {
				Nature nature = vertex.guessNature();
				if (nature == nr) {
					// 有些双名实际上可以构成更长的三名
					if (vertex.getAttribute().totalFrequency <= 1000 && vertex.realWord.length() == 2) {
						nrEnumItem = new NatureFreqSet<NR>();
						nrEnumItem.labelMap.put(NR.X, 2); // 认为是三字人名前2个字=双字人名的可能性更高
						nrEnumItem.labelMap.put(NR.G, 1);
					} else
						nrEnumItem = new NatureFreqSet<NR>(NR.A, PersonDictionary.transformMatrixDictionary.getTotalFrequency(NR.A));
				} else if (nature == nnt) {
					// 姓+职位
					nrEnumItem = new NatureFreqSet<NR>(NR.G, NR.K);
				} else {
					nrEnumItem = new NatureFreqSet<NR>(NR.A, PersonDictionary.transformMatrixDictionary.getTotalFrequency(NR.A));
				}
			}
			tagList.add(nrEnumItem);
		}
		return tagList;
	}

	/**
	 * 维特比算法求解最优标签
	 * 
	 * @param roleTagList
	 * @return
	 */
	public static List<NR> viterbiCompute(List<NatureFreqSet<NR>> roleTagList) {
		return ViterbiAlgorithm.computeEnum(roleTagList, PersonDictionary.transformMatrixDictionary);
	}

	/**
	 * 简化的"维特比算法"求解最优标签
	 * 
	 * @param roleTagList
	 * @return
	 */
	public static List<NR> viterbiComputeSimply(List<NatureFreqSet<NR>> roleTagList) {
		return ViterbiAlgorithm.computeEnumSimply(roleTagList, PersonDictionary.transformMatrixDictionary);
	}
}
