/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2015/1/19 21:02</create-date>
 *
 * <copyright file="Node.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.seg.viterbi;

import com.hankcs.hanlp.dictionary.wgraph.TermUtils;
import com.hankcs.hanlp.dictionary.wgraph.Vertex;

/**
 * @author hankcs
 */
public class ViterbiNode {
	/**
	 * 到该节点的最短路径的前驱节点
	 */
	ViterbiNode from;
	/**
	 * 最短路径对应的权重
	 */
	double weight;
	/**
	 * 节点代表的顶点
	 */
	Vertex vertex;

	public ViterbiNode(Vertex vertex) {
		this.vertex = vertex;
	}

	public void updateFrom(ViterbiNode from) {
		double weight = from.weight + TermUtils.calculateWeight(from.vertex, this.vertex);
		if (this.from == null || this.weight > weight) {
			this.from = from;
			this.weight = weight;
		}
	}

	@Override
	public String toString() {
		return vertex.toString();
	}
}
