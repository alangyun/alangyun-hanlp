package com.hankcs;

import com.hankcs.hanlp.dictionary.ts.TraditionalChineseDictionary;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.viterbi.ViterbiSegment;

public class SegmentUtils {

	public static Segment newSegment() {
		return new ViterbiSegment(); // Viterbi分词器是目前效率和效果的最佳平衡
	}

	/**
	 * 繁转简
	 *
	 * @param traditionalChineseString 繁体中文
	 * @return 简体中文
	 */
	public static String convertToSimplifiedChinese(String traditionalChineseString) {
		return TraditionalChineseDictionary.convertToSimplifiedChinese(traditionalChineseString.toCharArray());
	}
}
