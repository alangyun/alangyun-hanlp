阿朗云自然语言处理组件

基于hanlp自然语言处理组件1.8.3版本，将该组件中已经过期的和已经不再使用的部分剔除，并根据实际用途进行拆分而形成。

| 工程包 | 说明 |
| -- | -- |
| hanlp-base | 整个工程组中其他组件包的基础。仅依赖log4j |
| hanlp-data | 无任何意义，仅作为自然语言处理组件所需数据词典的说明 |
| hanlp-dictionary | 词典组件，为工程组中的其他组件提供词典装载和应用支持，并提供词典训练支持。依赖hanlp-base |
| hanlp-segment | 提供分词、简繁体转换、拼音转换支持。 依赖hanlp-base和hanlp-dictionary |
| hanlp-dependency | 提供依存句法支持，依赖hanlp-base、hanlp-dictionary和hanlp-segment |
| hanlp-exploit | 提供应用支持，包括文本分类、自动聚类、推荐词、自动摘要等支持。依赖hanlp-base、hanlp-dictionary、hanlp-segment和hanlp-dependency |