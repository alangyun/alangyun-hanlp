package com.hankcs.hanlp.ahocorasick.interval;

/**
 * 区段比较器接口
 */
public interface Intervalable {
	/**
	 * 起点
	 * 
	 * @return
	 */
	public int getStart();

	/**
	 * 终点
	 * 
	 * @return
	 */
	public int getEnd();

	/**
	 * 长度
	 * 
	 * @return
	 */
	public int size();

}
