package com.hankcs.hanlp.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NlpLogger {

	private static Logger logger = LogManager.getLogger("alangyun-nlp");
	
	public static final Logger getLogger(Class<?> clazz) {
		return LogManager.getLogger(clazz);
	}

	public static boolean enableDebug() {
		return logger.isDebugEnabled();
	}
	
	private static Object[] joinParams(String clzName, Object... args) {
		Object[] ret = new Object[args.length+1];
		ret[0] = clzName;
		System.arraycopy(args, 0, ret, 1, args.length);
		
		return ret;
	}

	public static void debug(String clzName, String message) {
		logger.debug("[{}]{}", clzName, message);
	}

	public static void debug(String clzName, String format, Object... args) {
		logger.debug("[{}]" + format, joinParams( clzName, args));
	}

	public static void info(String clzName, String message) {
		logger.info("[{}]{}", clzName, message);
	}

	public static void info(String clzName, String format, Object... args) {
		logger.info("[{}]" + format, joinParams(clzName, args));
	}

	public static void warning(String clzName, String message) {
		logger.warn("[{}]{}", clzName, message);
	}

	public static void warning(String clzName, String format, Object... args) {
		logger.warn("[{}]" + format, joinParams(clzName, args));
	}

	public static void warning(String clzName, String format, Throwable throwable, Object... args) {
		logger.warn("[{}]" + format, joinParams(clzName, args));
		logger.warn(throwable);
	}

	public static void warning(String clzName, String message, Throwable throwable) {
		logger.warn("[{}]{}", clzName, message);
		logger.warn(throwable);
	}

	public static void severe(String clzName, String message) {
		logger.trace("[{}]{}", clzName, message);
	}

	public static void error(String clzName, String format, Object... args) {
		logger.error("[{}]" + format, joinParams(clzName, args));
	}

	public static void error(String clzName, Throwable throwable) {
		logger.error(clzName, throwable);
	}

	public static void error(String clzName, String message, Throwable throwable) {
		logger.error("[{}]{}", clzName, message);
		logger.error(throwable);
	}

	public static long beginLog(String clzName, String format, Object... args) {
		logger.info("[{}]" + format, clzName, args);
		return System.currentTimeMillis();
	}

	public static void endLog(String clzName, long start, String format, Object... args) {
		logger.info("[{}]耗时 {} ms," + format, joinParams(clzName, (System.currentTimeMillis() - start), args));
	}

}
