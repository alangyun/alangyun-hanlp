package com.hankcs.hanlp.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.util.Properties;

import com.hankcs.hanlp.io.IIOAdapter;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.utility.Predefine;
import com.hankcs.hanlp.utility.TextUtility;

/**
 * 库的全局配置，既可以用代码修改，也可以通过hanlp.properties配置（按照 变量名=值 的形式）<br/>
 * 将Hanlp中的config静态类提取出来<br/>
 * 
 * com.hankcs.hanlp.config.NlpPreference.java
 * 
 * @author hoobort
 * @email klxukun@126.com
 * @unit 北京诚朗信息技术有限公司 2022年10月10日 下午1:35:43
 *
 */
public class NlpSetting {
	
	private static final String ENVORIMENT_NAME = "HANLP_ROOT";
	private static String myName = NlpSetting.class.getSimpleName();

	/** 核心词典路径 */
	public static String CoreDictionaryPath = DictionaryNames.Core.contextFilePath();
	/** 核心词典词性转移矩阵路径 */
	public static String CoreDictionaryTransformMatrixDictionaryPath = DictionaryNames.CoreTransformMatrix.contextFilePath();
	/** 用户自定义词典路径 */
	public static String CustomDictionaryPath[] = new String[] { DictionaryNames.Custom.contextFilePath() };
	/** 用户自定义词典是否自动重新生成缓存（根据词典文件的最后修改时间是否大于缓存文件的时间判断） */
	public static boolean CustomDictionaryAutoRefreshCache = true;
	/** 2元语法词典路径 */
	public static String BiGramDictionaryPath = DictionaryNames.BiGram.contextFilePath();
	/** 停用词词典路径 */
	public static String CoreStopWordDictionaryPath = DictionaryNames.CoreStopWord.contextFilePath();
	/** 同义词词典路径 */
	public static String CoreSynonymDictionaryDictionaryPath = DictionaryNames.CoreSynonym.contextFilePath();
	/** 人名词典路径 */
	public static String PersonDictionaryPath = DictionaryNames.Person.contextFilePath();
	/** 人名词典转移矩阵路径 */
	public static String PersonDictionaryTrPath = DictionaryNames.PersonTr.contextFilePath();
	/** 地名词典路径 */
	public static String PlaceDictionaryPath = DictionaryNames.Place.contextFilePath();
	/** 地名词典转移矩阵路径 */
	public static String PlaceDictionaryTrPath = DictionaryNames.PlaceTr.contextFilePath();
	/** 地名词典路径 */
	public static String OrganizationDictionaryPath = DictionaryNames.Organization.contextFilePath();
	/** 地名词典转移矩阵路径 */
	public static String OrganizationDictionaryTrPath = DictionaryNames.OrganizationTr.contextFilePath();
	/** 简繁转换词典根目录 */
	public static String tcDictionaryRoot = DictionaryNames.tcRoot.contextFilePath();
	/** 拼音词典路径 */
	public static String PinyinDictionaryPath = DictionaryNames.Pinyin.contextFilePath();
	/** 音译人名词典 */
	public static String TranslatedPersonDictionaryPath = DictionaryNames.TranslatedPerson.contextFilePath();
	/** 日本人名词典路径 */
	public static String JapanesePersonDictionaryPath = DictionaryNames.JapanesePerson.contextFilePath();
	/** 字符类型对应表 */
	public static String CharTypePath = DictionaryNames.CharType.contextFilePath();
	/** 字符正规化表（全角转半角，繁体转简体） */
	public static String CharTablePath = DictionaryNames.CharTable.contextFilePath();
	/** 词性标注集描述表，用来进行中英映射（对于Nature词性，可直接参考Nature.java中的注释） */
	public static String PartOfSpeechTagDictionary = DictionaryNames.PartOfSpeechTag.contextFilePath();
	/** 词-词性-依存关系模型 */
	public static String WordNatureModelPath = DictionaryNames.WordNatureModel.contextFilePath();
	/** 神经网络依存模型路径 */
	public static String NNParserModelPath = DictionaryNames.NNParserModel.contextFilePath();
	/** 感知机ArcEager依存模型路径 */
	public static String PerceptronParserModelPath = DictionaryNames.PerceptronParserModel.contextFilePath();
	/** CRF分词模型 */
	public static String CRFCWSModelPath = DictionaryNames.CRFCWS.contextFilePath();
	/** CRF词性标注模型 */
	public static String CRFPOSModelPath = DictionaryNames.CRFPOS.contextFilePath();
	/** CRF命名实体识别模型 */
	public static String CRFNERModelPath = DictionaryNames.CRFNER.contextFilePath();
	/** 感知机分词模型 */
	public static String PerceptronCWSModelPath = DictionaryNames.PerceptronCWS.contextFilePath();
	/** 感知机词性标注模型 */
	public static String PerceptronPOSModelPath = DictionaryNames.PerceptronPOS.contextFilePath();
	/** 感知机命名实体识别模型 */
	public static String PerceptronNERModelPath = DictionaryNames.PerceptronNER.contextFilePath();
	/** 分词结果是否展示词性 */
	public static boolean ShowTermNature = true;
	/** 是否执行字符正规化（繁体->简体，全角->半角，大写->小写），切换配置后必须删CustomDictionary.txt.bin缓存 */
	public static boolean Normalization = false;
	/**
	 * IO适配器（默认null，表示从本地文件系统读取），实现com.hankcs.hanlp.corpus.io.IIOAdapter接口
	 * 以在不同的平台（Hadoop、Redis等）上运行HanLP
	 */
	public static IIOAdapter IOAdapter;
	
	static {
		initSetting();
	}

	public static void initSetting() {
		NlpLogger.info(myName, "正在初始化NLP环境变量...");
		// 自动读取配置
		Properties p = new Properties();
		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			if (loader == null)
				loader = NlpSetting.class.getClassLoader();// IKVM (v.0.44.0.5) doesn't set context classloader

			try {
				p.load(new InputStreamReader(Predefine.HANLP_PROPERTIES_PATH == null ? loader.getResourceAsStream("hanlp.properties")
						: new FileInputStream(Predefine.HANLP_PROPERTIES_PATH), "UTF-8"));
			} catch (Exception e) {
				String rootPath = System.getProperty(ENVORIMENT_NAME);
				if (rootPath == null)
					rootPath = System.getenv(ENVORIMENT_NAME);
				if (rootPath != null) {
					rootPath = rootPath.trim();
					p = new Properties();
					p.setProperty("root", rootPath);
					NlpLogger.info(myName, "使用环境变量 {}={}",ENVORIMENT_NAME, rootPath);
				} else
					throw e;
			}
			String root = p.getProperty("root", "").replaceAll("\\\\", "/");
			if (root.length() > 0 && !root.endsWith("/"))
				root += "/";
			CoreDictionaryPath = DictionaryNames.Core.absoluteFilePath(root, p);
			CoreDictionaryTransformMatrixDictionaryPath = DictionaryNames.CoreTransformMatrix.absoluteFilePath(root, p);
			BiGramDictionaryPath = DictionaryNames.BiGram.absoluteFilePath(root, p);
			CoreStopWordDictionaryPath = DictionaryNames.CoreStopWord.absoluteFilePath(root, p);
			CoreSynonymDictionaryDictionaryPath = DictionaryNames.CoreSynonym.absoluteFilePath(root, p);
			PersonDictionaryPath = DictionaryNames.Person.absoluteFilePath(root, p);
			PersonDictionaryTrPath = DictionaryNames.PersonTr.absoluteFilePath(root, p);
			//自定义词典可能有多个，词典放置在不同目录下为一组，每组之间用“;”分隔
			//同一组可能有多个词典文件，则除了第一个词典文件带有目录相对路径（相对于词典目录），其他同组的词典文件用半角空格分隔；
			//例如：dir1/custom1.txt custom2;dir2/custom21.txt custom22.txt custom23.txt
			String[] pathArray = p.getProperty(DictionaryNames.Custom.key(), DictionaryNames.Custom.contextFilePath()).split(";");
			String prePath = root;
			for (int i = 0; i < pathArray.length; ++i) {
				if (pathArray[i].startsWith(" ")) {
					pathArray[i] = prePath + pathArray[i].trim();
				} else {
					pathArray[i] = root + pathArray[i];
					int lastSplash = pathArray[i].lastIndexOf('/');
					if (lastSplash != -1) {
						prePath = pathArray[i].substring(0, lastSplash + 1);
					}
				}
			}
			CustomDictionaryPath = pathArray;
			CustomDictionaryAutoRefreshCache = "true".equals(p.getProperty("CustomDictionaryAutoRefreshCache", "true"));
			tcDictionaryRoot = DictionaryNames.tcRoot.absoluteFilePath(root, p);
			if (!tcDictionaryRoot.endsWith("/"))
				tcDictionaryRoot += '/';
			PinyinDictionaryPath = DictionaryNames.Pinyin.absoluteFilePath(root, p);
			TranslatedPersonDictionaryPath = DictionaryNames.TranslatedPerson.absoluteFilePath(root, p);
			JapanesePersonDictionaryPath = DictionaryNames.JapanesePerson.absoluteFilePath(root, p);
			PlaceDictionaryPath = DictionaryNames.Place.absoluteFilePath(root, p);
			PlaceDictionaryTrPath = DictionaryNames.PlaceTr.absoluteFilePath(root, p);
			OrganizationDictionaryPath = DictionaryNames.Organization.absoluteFilePath(root, p);
			OrganizationDictionaryTrPath = DictionaryNames.OrganizationTr.absoluteFilePath(root, p);
			CharTypePath = DictionaryNames.CharType.absoluteFilePath(root, p);
			CharTablePath = DictionaryNames.CharTable.absoluteFilePath(root, p);
			PartOfSpeechTagDictionary = DictionaryNames.PartOfSpeechTag.absoluteFilePath(root, p);
			WordNatureModelPath = DictionaryNames.WordNatureModel.absoluteFilePath(root, p);
			NNParserModelPath = DictionaryNames.NNParserModel.absoluteFilePath(root, p);
			PerceptronParserModelPath = DictionaryNames.PerceptronParserModel.absoluteFilePath(root, p);
			CRFCWSModelPath = DictionaryNames.CRFCWS.absoluteFilePath(root, p);
			CRFPOSModelPath = DictionaryNames.CRFPOS.absoluteFilePath(root, p);
			CRFNERModelPath = DictionaryNames.CRFNER.absoluteFilePath(root, p);
			PerceptronCWSModelPath = DictionaryNames.PerceptronCWS.absoluteFilePath(root, p);
			PerceptronPOSModelPath = DictionaryNames.PerceptronPOS.absoluteFilePath(root, p);
			PerceptronNERModelPath = DictionaryNames.PerceptronNER.absoluteFilePath(root, p);
			ShowTermNature = "true".equals(p.getProperty("ShowTermNature", "true"));
			Normalization = "true".equals(p.getProperty("Normalization", "false"));

			String ioAdapterClassName = p.getProperty("IOAdapter");
			if (ioAdapterClassName != null) {
				try {
					Class<?> clazz = Class.forName(ioAdapterClassName);
					Constructor<?> ctor = clazz.getConstructor();
					Object instance = ctor.newInstance();
					if (instance != null)
						IOAdapter = (IIOAdapter) instance;
				} catch (ClassNotFoundException e) {
					NlpLogger.warning(myName, "找不到IO适配器类： {} ，请检查第三方插件jar包", ioAdapterClassName);
				} catch (NoSuchMethodException e) {
					NlpLogger.warning(myName, "工厂类[{}]没有默认构造方法，不符合要求", ioAdapterClassName);
				} catch (SecurityException e) {
					NlpLogger.warning(myName, "工厂类[{}]默认构造方法无法访问，不符合要求", ioAdapterClassName);
				} catch (Exception e) {
					NlpLogger.warning(myName, "工厂类[{}]构造失败：{}", ioAdapterClassName, TextUtility.exceptionToString(e));
				}
			}
		} catch (Exception e) {
			if (new File("data/dictionary/CoreNatureDictionary.tr.txt").isFile()) {
				NlpLogger.info(myName, "使用当前目录下的data");
			} else {
				StringBuilder sbInfo = new StringBuilder("========Tips========\n请将hanlp.properties放在下列目录：\n"); // 打印一些友好的tips
				if (new File("src/main/java").isDirectory()) {
					sbInfo.append("src/main/resources");
				} else {
					String classPath = (String) System.getProperties().get("java.class.path");
					if (classPath != null) {
						for (String path : classPath.split(File.pathSeparator)) {
							if (new File(path).isDirectory()) {
								sbInfo.append(path).append('\n');
							}
						}
					}
					sbInfo.append("Web项目则请放到下列目录：\n" + "Webapp/WEB-INF/lib\n" + "Webapp/WEB-INF/classes\n" + "Appserver/lib\n" + "JRE/lib\n");
					sbInfo.append("并且编辑root=PARENT/path/to/your/data\n");
					sbInfo.append("现在HanLP将尝试从").append(System.getProperties().get("user.dir")).append("读取data……");
				}
				NlpLogger.error(myName, "没有找到hanlp.properties，可能会导致找不到data{} ", sbInfo);
			}
		}

		NlpLogger.info(myName, "初始化NLP环境变量完成！");
	}

}
