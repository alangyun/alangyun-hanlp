package com.hankcs.hanlp.config;

import java.util.Properties;

/**
 * 字典路径参数名常量定义<br/>
 * 通过该枚举可以获取：<br/>
 * 1. 参数变量名<br/>
 * 2. 相对于根路径的相对路径<br/>
 * 3. 默认的文件名<br/>
 * 
 * com.hankcs.hanlp.config.DictionaryNames.java
 * 
 * @author hoobort
 * @email klxukun@126.com
 * @unit 北京诚朗信息技术有限公司 2022年10月25日 上午10:56:34
 *
 */
public enum DictionaryNames {

	/** 核心词典路径 */
	Core("CoreDictionaryPath", "data/dictionary", "CoreNatureDictionary.txt"),
	/** 核心词典词性转移矩阵路径 */
	CoreTransformMatrix("CoreDictionaryTransformMatrixDictionaryPath", "data/dictionary", "CoreNatureDictionary.tr.txt"),
	/** 用户自定义词典路径 */
	Custom("CustomDictionaryPath", "data/dictionary/custom", "CustomDictionary.txt"),
	/** 2元语法词典路径 */
	BiGram("BiGramDictionaryPath", "data/dictionary", "CoreNatureDictionary.ngram.txt"),
	/** 停用词词典路径 */
	CoreStopWord("CoreStopWordDictionaryPath", "data/dictionary", "stopwords.txt"),
	/** 同义词词典路径 */
	CoreSynonym("CoreSynonymDictionaryDictionaryPath", "data/dictionary/synonym", "CoreSynonym.txt"),
	/** 人名词典路径 */
	Person("PersonDictionaryPath", "data/dictionary/person", "nr.txt"),
	/** 人名词典转移矩阵路径 */
	PersonTr("PersonDictionaryTrPath", "data/dictionary/person", "nr.tr.txt"),
	/** 地名词典路径 */
	Place("PlaceDictionaryPath", "data/dictionary/place", "ns.txt"),
	/** 地名词典转移矩阵路径 */
	PlaceTr("PlaceDictionaryTrPath", "data/dictionary/place", "ns.tr.txt"),
	/** 地名词典路径 */
	Organization("OrganizationDictionaryPath", "data/dictionary/organization", "nt.txt"),
	/** 地名词典转移矩阵路径 */
	OrganizationTr("OrganizationDictionaryTrPath", "data/dictionary/organization", "nt.tr.txt"),
	/** 简繁转换词典根目录 */
	tcRoot("tcDictionaryRoot", "data/dictionary/tc", ""),
	/** 拼音词典路径 */
	Pinyin("PinyinDictionaryPath", "data/dictionary/pinyin", "pinyin.txt"),
	/** 音译人名词典 */
	TranslatedPerson("TranslatedPersonDictionaryPath", "data/dictionary/person", "nrf.txt"),
	/** 日本人名词典路径 */
	JapanesePerson("JapanesePersonDictionaryPath", "data/dictionary/person", "nrj.txt"),
	/** 字符类型对应表 */
	CharType("CharTypePath", "data/dictionary/other", "CharType.bin"),
	/** 字符正规化表（全角转半角，繁体转简体） */
	CharTable("CharTablePath", "data/dictionary/other", "CharTable.txt"),
	/** 词性标注集描述表，用来进行中英映射（对于Nature词性，可直接参考Nature.java中的注释） */
	PartOfSpeechTag("PartOfSpeechTagDictionary", "data/dictionary/other", "TagPKU98.csv"),
	/** 词-词性-依存关系模型 */
	WordNatureModel("WordNatureModelPath", "data/model/dependency", "WordNature.txt"),
	/** 神经网络依存模型路径 */
	NNParserModel("NNParserModelPath", "data/model/dependency", "NNParserModel.txt"),
	/** 感知机ArcEager依存模型路径 */
	PerceptronParserModel("PerceptronParserModelPath", "data/model/dependency", "perceptron.bin"),
	/** CRF分词模型 */
	CRFCWS("CRFCWSModelPath", "data/model/crf", "cws.txt"),
	/** CRF词性标注模型 */
	CRFPOS("CRFPOSModelPath", "data/model/crf", "pos.txt"),
	/** CRF命名实体识别模型 */
	CRFNER("CRFNERModelPath", "data/model/crf", "ner.txt"),
	/** 感知机分词模型 */
	PerceptronCWS("PerceptronCWSModelPath", "data/model/perceptron", "cws.bin"),
	/** 感知机词性标注模型 */
	PerceptronPOS("PerceptronPOSModelPath", "data/model/perceptron", "pos.bin"),
	/** 感知机命名实体识别模型 */
	PerceptronNER("PerceptronNERModelPath", "data/model/perceptron", "ner.bin");

	private String keyCode;
	private String context;
	private String filename;

	private DictionaryNames(String key, String contextPath, String fileName) {
		this.keyCode = key;
		this.context = contextPath;
		this.filename = fileName;
	}

	/**
	 * 获取参数变量名
	 *
	 * hoobort 2022年10月25日 上午11:37:02
	 * 
	 * @return
	 */
	public String key() {
		return keyCode;
	}

	/**
	 * 获取相对目录路径
	 *
	 * hoobort 2022年10月25日 上午11:37:16
	 * 
	 * @return
	 */
	public String contextPath() {
		return context;
	}

	/**
	 * 获取对应文件所在目录的绝对路径
	 *
	 * hoobort
	 * 2022年10月25日 下午1:39:38
	 * @param root
	 * @return
	 */
	public String absolutePath(String root) {
		String ret = root;
		if (!ret.endsWith("/"))
			ret += "/";

		ret = ret + context;

		return ret;
	}

	/**
	 * 获取文件名
	 *
	 * hoobort 2022年10月25日 上午11:37:29
	 * 
	 * @return
	 */
	public String fileName() {
		return filename;
	}

	/**
	 * 获取相对于根目录的相对路径
	 *
	 * hoobort 2022年10月25日 上午11:37:43
	 * 
	 * @return
	 */
	public String contextFilePath() {
		return context + "/" + filename;
	}

	/**
	 * 获取绝对的文件全路径
	 *
	 * hoobort 2022年10月25日 上午11:38:25
	 * 
	 * @param root 根路径
	 * @return
	 */
	public String absoluteFilePath(String root) {
		String ret = root;
		if (!ret.endsWith("/"))
			ret += "/";

		ret = ret + context + "/" + filename;

		return ret;
	}

	/**
	 * 获取绝对的文件全路径
	 *
	 * hoobort
	 * 2022年10月25日 下午12:34:02
	 * @param root 根路径
	 * @param customContextFilePath 自定义相对路径
	 * @return
	 */
	public String absoluteFilePath(String root, String customContextFilePath) {
		if(customContextFilePath==null || customContextFilePath.trim().length()==0)
			return absoluteFilePath(root);
		

		String ret = root;
		if (ret.endsWith("/")){
			if(customContextFilePath.startsWith("/"))
				ret = ret + customContextFilePath.substring(1);
			else
				ret += customContextFilePath;
		} else {
			if(customContextFilePath.startsWith("/"))
				ret += customContextFilePath;
			else
				ret = ret + "/" + customContextFilePath;
		}
		return ret;
	}
	
	/**
	 * 获取绝对的文件全路径
	 *
	 * hoobort
	 * 2022年10月25日 下午12:34:07
	 * @param root 根路径
	 * @param p 配置的properties文件
	 * @return
	 */
	public String absoluteFilePath(String root, Properties p) {
		String customPath = p.getProperty(keyCode);
		
		return absoluteFilePath(root, customPath);
	}
}
