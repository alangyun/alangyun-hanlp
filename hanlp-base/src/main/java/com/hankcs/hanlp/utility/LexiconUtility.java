/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>me@hankcs.com</email>
 * <create-date>2015/7/14 11:01</create-date>
 *
 * <copyright file="WordNatureUtil.java" company="码农场">
 * Copyright (c) 2008-2015, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.utility;

import com.hankcs.hanlp.meta.roletag.Nature;

import java.util.LinkedHashSet;

/**
 * 跟词语与词性有关的工具类，可以全局动态修改HanLP内部词库
 *
 * @author hankcs
 */
public class LexiconUtility {
	/**
	 * 将字符串词性转为Enum词性
	 * 
	 * @param name                  词性名称
	 * @param customNatureCollector 一个收集集合
	 * @return 转换结果
	 */
	public static Nature convertStringToNature(String name, LinkedHashSet<Nature> customNatureCollector) {
		Nature nature = Nature.fromString(name);
		if (nature == null) {
			nature = Nature.create(name);
			if (customNatureCollector != null)
				customNatureCollector.add(nature);
		}
		return nature;
	}

	/**
	 * 将字符串词性转为Enum词性
	 * 
	 * @param name 词性名称
	 * @return 转换结果
	 */
	public static Nature convertStringToNature(String name) {
		return convertStringToNature(name, null);
	}
}
