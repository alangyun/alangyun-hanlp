package com.hankcs.hanlp.meta.word;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;

import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.meta.roletag.Nature;

/**
 * 使用DoubleArrayTrie实现的核心词典<br/>
 * 将CoreDictionary.Attribute迁移出来并删除该类，使用现在的WordAttribute替代<br/>
 * 
 * com.hankcs.hanlp.meta.word.WordAttribute.java
 * 
 * @author hoobort
 * @email klxukun@126.com
 * @unit 北京诚朗信息技术有限公司 2022年10月10日 上午10:01:53
 *
 */
public class WordAttribute implements Serializable {
	private static String myName = WordAttribute.class.getSimpleName();

	/** 词性列表 */
	public Nature nature[];
	/** 词性对应的词频 */
	public int frequency[];
	/** 所有词性的总词频 * */
	public int totalFrequency;

	public WordAttribute(int size) {
		nature = new Nature[size];
		frequency = new int[size];
	}

	public WordAttribute(Nature[] nature, int[] frequency) {
		this.nature = nature;
		this.frequency = frequency;
	}

	public WordAttribute(Nature nature, int frequency) {
		this(1);
		this.nature[0] = nature;
		this.frequency[0] = frequency;
		totalFrequency = frequency;
	}

	public WordAttribute(Nature[] nature, int[] frequency, int totalFrequency) {
		this.nature = nature;
		this.frequency = frequency;
		this.totalFrequency = totalFrequency;
	}

	/**
	 * 使用单个词性，默认词频1000构造
	 *
	 * @param nature
	 */
	public WordAttribute(Nature nature) {
		this(nature, 1000);
	}

	public static WordAttribute create(String natureWithFrequency) {
		try {
			String param[] = natureWithFrequency.split(" ");
			if (param.length % 2 != 0) {
				return new WordAttribute(Nature.create(natureWithFrequency.trim()), 1); // 儿童锁
			}
			int natureCount = param.length / 2;
			WordAttribute attribute = new WordAttribute(natureCount);
			for (int i = 0; i < natureCount; ++i) {
				attribute.nature[i] = Nature.create(param[2 * i]);
				attribute.frequency[i] = Integer.parseInt(param[1 + 2 * i]);
				attribute.totalFrequency += attribute.frequency[i];
			}
			return attribute;
		} catch (Exception e) {
			NlpLogger.warning(myName, "使用字符串“{}”创建词条属性失败！",natureWithFrequency);
			return null;
		}
	}

	/**
	 * 获取词性的词频
	 *
	 * @param nature 词性
	 * @return 词频
	 */
	public int getNatureFrequency(final Nature nature) {
		int i = 0;
		for (Nature pos : this.nature) {
			if (nature == pos) {
				return frequency[i];
			}
			++i;
		}
		return 0;
	}

	/**
	 * 是否有某个词性
	 *
	 * @param nature
	 * @return
	 */
	public boolean hasNature(Nature nature) {
		return getNatureFrequency(nature) > 0;
	}

	/**
	 * 是否有以某个前缀开头的词性
	 *
	 * @param prefix 词性前缀，比如u会查询是否有ude, uzhe等等
	 * @return
	 */
	public boolean hasNatureStartsWith(String prefix) {
		for (Nature n : nature) {
			if (n.startsWith(prefix))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < nature.length; ++i) {
			sb.append(nature[i]).append(' ').append(frequency[i]).append(' ');
		}
		return sb.toString();
	}

	public void save(DataOutputStream out) throws IOException {
		out.writeInt(totalFrequency);
		out.writeInt(nature.length);
		for (int i = 0; i < nature.length; ++i) {
			out.writeInt(nature[i].ordinal());
			out.writeInt(frequency[i]);
		}
	}

}
