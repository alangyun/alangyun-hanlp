/*
 * <summary></summary>
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2016-09-04 PM7:41</create-date>
 *
 * <copyright file="FrequencyMap.java" company="码农场">
 * Copyright (c) 2008-2016, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.meta.tagger.map;

import java.util.TreeMap;

/**
 * 词和词频映射
 * 
 * @author hankcs
 */
public class FrequencyMap extends TreeMap<String, Integer> {
	
	/** 总词频(所有词的词频） * */
	public int totalFrequency;

	/**
	 * 添加词
	 *
	 * hoobort
	 * 2022年10月13日 上午1:57:24
	 * @param word 词条
	 * @return 返回词条的词频
	 */
	public int add(String word) {
		++totalFrequency;
		Integer frequency = get(word);
		if (frequency == null) {
			put(word, 1);
			return 1;
		} else {
			put(word, ++frequency);
			return frequency;
		}
	}
}
