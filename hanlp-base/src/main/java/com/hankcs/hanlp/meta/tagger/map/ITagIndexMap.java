/*
 * <summary></summary>
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2016-09-04 PM4:39</create-date>
 *
 * <copyright file="LabelIdMap.java" company="码农场">
 * Copyright (c) 2008-2016, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */

package com.hankcs.hanlp.meta.tagger.map;

/**
 * id(index)和标签映射接口定义<br/>
 * 
 * com.hankcs.hanlp.model.common.IStringIdMap.java
 * @author hoobort
 * @email klxukun@126.com
 * @unit 北京诚朗信息技术有限公司
 * 2022年10月11日 下午3:44:15
 *
 */
public interface ITagIndexMap {
	/**
	 * 根据给定的标签获取标签的索引号（或数组下标）
	 *
	 * hoobort
	 * 2022年10月13日 上午2:00:39
	 * @param tag
	 * @return
	 */
	int indexByTag(String tag);
}
