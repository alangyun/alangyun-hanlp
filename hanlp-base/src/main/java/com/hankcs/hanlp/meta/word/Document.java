/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/9/8 19:01</create-date>
 *
 * <copyright file="Document.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.meta.word;

import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;

import java.io.File;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author hankcs
 */
public class Document implements Serializable {
	private static String myName = Document.class.getSimpleName();
	
	/** 文档构成句子顺序列表 * */
	public List<Sentence> sentenceList;

	public Document(List<Sentence> sentenceList) {
		this.sentenceList = sentenceList;
	}

	public static Document create(String param) {
		Pattern pattern = Pattern.compile(".+?((。/w)|(！/w )|(？/w )|\\n|$)");
		Matcher matcher = pattern.matcher(param);
		List<Sentence> sentenceList = new LinkedList<Sentence>();
		while (matcher.find()) {
			String single = matcher.group();
			Sentence sentence = Sentence.create(single);
			if (sentence == null) {
				NlpLogger.warning(myName, "使用参数“{}”构建文档句子失败",single);
				return null;
			}
			sentenceList.add(sentence);
		}
		return new Document(sentenceList);
	}

	/**
	 * 获取单词序列
	 *
	 * @return
	 */
	public List<IWord> getWordList() {
		List<IWord> wordList = new LinkedList<IWord>();
		for (Sentence sentence : sentenceList) {
			wordList.addAll(sentence.wordList);
		}
		return wordList;
	}

	/**
	 * 获取单词（如果有复合词，将复合词拆分成单词）序列
	 *
	 * hoobort
	 * 2022年10月11日 下午8:41:19
	 * @return
	 */
	public List<Word> getSimpleWordList() {
		List<IWord> wordList = getWordList();
		List<Word> simpleWordList = new LinkedList<Word>();
		for (IWord word : wordList) {
			if (word instanceof CompoundWord) {
				simpleWordList.addAll(((CompoundWord) word).innerList);
			} else {
				simpleWordList.add((Word) word);
			}
		}

		return simpleWordList;
	}

	/**
	 * 获取简单的句子列表，其中复合词会被拆分为简单词
	 *
	 * @return
	 */
	public List<List<Word>> getSimpleSentenceList() {
		List<List<Word>> simpleList = new LinkedList<List<Word>>();
		for (Sentence sentence : sentenceList) {
			List<Word> wordList = new LinkedList<Word>();
			for (IWord word : sentence.wordList) {
				if (word instanceof CompoundWord) {
					for (Word inner : ((CompoundWord) word).innerList) {
						wordList.add(inner);
					}
				} else {
					wordList.add((Word) word);
				}
			}
			simpleList.add(wordList);
		}

		return simpleList;
	}

	/**
	 * 获取复杂句子列表，句子中的每个单词有可能是复合词，有可能是简单词
	 *
	 * @return
	 */
	public List<List<IWord>> getComplexSentenceList() {
		List<List<IWord>> complexList = new LinkedList<List<IWord>>();
		for (Sentence sentence : sentenceList) {
			complexList.add(sentence.wordList);
		}

		return complexList;
	}

	/**
	 * 获取简单的句子列表
	 *
	 * @param spilt 如果为真，其中复合词会被拆分为简单词
	 * @return
	 */
	public List<List<Word>> getSimpleSentenceList(boolean spilt) {
		List<List<Word>> simpleList = new LinkedList<List<Word>>();
		for (Sentence sentence : sentenceList) {
			List<Word> wordList = new LinkedList<Word>();
			for (IWord word : sentence.wordList) {
				if (word instanceof CompoundWord) {
					if (spilt) {
						for (Word inner : ((CompoundWord) word).innerList) {
							wordList.add(inner);
						}
					} else {
						wordList.add(((CompoundWord) word).toWord());
					}
				} else {
					wordList.add((Word) word);
				}
			}
			simpleList.add(wordList);
		}

		return simpleList;
	}

	/**
	 * 获取简单的句子列表，其中复合词的标签如果是set中指定的话会被拆分为简单词
	 *
	 * @param labelSet
	 * @return
	 */
	public List<List<Word>> getSimpleSentenceList(Set<String> labelSet) {
		List<List<Word>> simpleList = new LinkedList<List<Word>>();
		for (Sentence sentence : sentenceList) {
			List<Word> wordList = new LinkedList<Word>();
			for (IWord word : sentence.wordList) {
				if (word instanceof CompoundWord) {
					if (labelSet.contains(word.getLabel())) {
						for (Word inner : ((CompoundWord) word).innerList) {
							wordList.add(inner);
						}
					} else {
						wordList.add(((CompoundWord) word).toWord());
					}
				} else {
					wordList.add((Word) word);
				}
			}
			simpleList.add(wordList);
		}

		return simpleList;
	}

	/**
	 * 将文档的组成单词转换为用空格分隔的单词(不含词性)序列字符串
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Sentence sentence : sentenceList) {
			sb.append(sentence);
			sb.append(' ');
		}
		if (sb.length() > 0)
			sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	/**
	 * 将给定的语料文件（已经做词性标定，格式参见人民日报1998年语料标注样式）拆分为单词文档
	 *
	 * hoobort
	 * 2022年10月11日 下午8:45:24
	 * @param file
	 * @return
	 */
	public static Document create(File file) {
		IOUtil.LineIterator lineIterator = new IOUtil.LineIterator(file.getAbsolutePath());
		List<Sentence> sentenceList = new LinkedList<Sentence>();
		for (String line : lineIterator) {
			line = line.trim();
			if (line.isEmpty())
				continue;
			Sentence sentence = Sentence.create(line);
			if (sentence == null) {
				NlpLogger.warning(myName, "使用参数“{}”创建文档句子失败", line);
				return null;
			}
			sentenceList.add(sentence);
		}
		
		return new Document(sentenceList);
	}
}
