/*
 * <summary></summary>
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2016-09-04 PM4:36</create-date>
 *
 * <copyright file="IdLabelMap.java" company="码农场">
 * Copyright (c) 2008-2016, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.meta.tagger.map;

/**
 * 索引和标签内的映射接口<br/>
 * 
 * @author hankcs, hoobort
 */
public interface IIndexTagMap {
	/**
	 * 通过给定的标签的索引号（实际为数组下标或列表索引）
	 *
	 * hoobort
	 * 2022年10月13日 上午1:59:08
	 * @param index 索引号
	 * @return 标签
	 */
	String tagByIndex(int index);
}
