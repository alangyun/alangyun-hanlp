/*
 * <summary></summary>
 * <author>He Han</author>
 * <email>hankcs.cn@gmail.com</email>
 * <create-date>2014/12/5 15:37</create-date>
 *
 * <copyright file="CharType.java" company="上海林原信息科技有限公司">
 * Copyright (c) 2003-2014, 上海林原信息科技有限公司. All Right Reserved, http://www.linrunsoft.com/
 * This source is subject to the LinrunSpace License. Please contact 上海林原信息科技有限公司 to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.meta.chartable;

import com.hankcs.hanlp.collection.array.ByteArray;
import com.hankcs.hanlp.config.NlpSetting;
import com.hankcs.hanlp.io.IOUtil;
import com.hankcs.hanlp.log.NlpLogger;
import com.hankcs.hanlp.utility.TextUtility;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * 字符类型<br/>
 * 字符映射文件中，按照utf8字节码分区段，将区段设定到定义的类型中，格式：<br/>
 * [byte1][byte2][byte3][byte4][byte5]<br/>
 * (byte1<<8)+byte2 构成区段的起始位置<br/>
 * (byte3<<8)+byte4 构成区段的结束位置<br/>
 * byte5为类型，类型包括CT_SINGLE，CT_DELIMITER，CT_CHINESE，CT_LETTER等<br/>
 *
 * @author hankcs
 */
public class CharType {
	private static String myName = CharType.class.getSimpleName();
	
	/** 单字节 */
	public static final byte CT_SINGLE = 5;
	/** 分隔符"!,.?()[]{}+= */
	public static final byte CT_DELIMITER = CT_SINGLE + 1;
	/** 中文字符 */
	public static final byte CT_CHINESE = CT_SINGLE + 2;
	/** 字母 */
	public static final byte CT_LETTER = CT_SINGLE + 3;
	/** 数字 */
	public static final byte CT_NUM = CT_SINGLE + 4;
	/** 序号 */
	public static final byte CT_INDEX = CT_SINGLE + 5;
	/** 中文数字 */
	public static final byte CT_CNUM = CT_SINGLE + 6;
	/** 其他 */
	public static final byte CT_OTHER = CT_SINGLE + 12;

	public static byte[] type;

	static {
		type = new byte[65536];
		NlpLogger.info(myName, "字符类型对应表开始加载 {} ", NlpSetting.CharTypePath);
		long start = System.currentTimeMillis();
		ByteArray byteArray = ByteArray.createByteArray(NlpSetting.CharTypePath);
		if (byteArray == null) {
			try {
				byteArray = generate();
			} catch (IOException e) {
				throw new IllegalArgumentException("字符类型对应表 " + NlpSetting.CharTypePath + " 加载失败： " + TextUtility.exceptionToString(e));
			}
		}
		//从文件中读取字符类型映射并写入到类型表中
		//数组下标对应的是字对应的字码，每个下标对应的字即为字的类型
		while (byteArray.hasMore()) {
			int b = byteArray.nextChar();
			int e = byteArray.nextChar();
			byte t = byteArray.nextByte();
			for (int i = b; i <= e; ++i) {
				type[i] = t;
			}
		}
		NlpLogger.info(myName, "字符类型对应表加载成功，耗时{}ms", (System.currentTimeMillis() - start));
	}

	private static ByteArray generate() throws IOException {
		int preType = 5;
		int preChar = 0;
		List<int[]> typeList = new LinkedList<int[]>();
		for (int i = 0; i <= Character.MAX_VALUE; ++i) {
			int type = TextUtility.charType((char) i);
			if (type != preType) {
				typeList.add(new int[] { preChar, i - 1, preType });

				preChar = i;
			}
			preType = type;
		}
		typeList.add(new int[] { preChar, (int) Character.MAX_VALUE, preType });

		DataOutputStream out = new DataOutputStream(IOUtil.newOutputStream(NlpSetting.CharTypePath));
		for (int[] array : typeList) {
			out.writeChar(array[0]);
			out.writeChar(array[1]);
			out.writeByte(array[2]);
		}
		out.close();
		ByteArray byteArray = ByteArray.createByteArray(NlpSetting.CharTypePath);

		return byteArray;
	}

	/**
	 * 获取字符的类型
	 *
	 * @param c
	 * @return
	 */
	public static byte get(char c) {
		return type[(int) c];
	}

	/**
	 * 设置字符类型
	 *
	 * @param c 字符
	 * @param t 类型
	 */
	public static void set(char c, byte t) {
		type[c] = t;
	}
	
}
