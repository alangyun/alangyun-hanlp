package com.hankcs.hanlp.meta;

import java.io.Serializable;

/**
 * 分词词条实体接口类<br/>
 * 目前暂时不用
 * com.hankcs.hanlp.meta.IWordTerm.java
 * @author hoobort
 * @email klxukun@126.com
 * @unit 北京诚朗信息技术有限公司
 * 2022年10月10日 下午1:12:47
 *
 */
public interface ITerm extends Serializable {

}
