/*
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2017-10-26 下午4:40</create-date>
 *
 * <copyright file="Tag.java" company="码农场">
 * Copyright (c) 2017, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.meta.tagger.tag;

import com.hankcs.hanlp.collection.array.ByteArray;
import com.hankcs.hanlp.io.ICacheAble;
import com.hankcs.hanlp.meta.tagger.TaggedType;
import com.hankcs.hanlp.meta.tagger.map.IIndexTagMap;
import com.hankcs.hanlp.meta.tagger.map.ITagIndexMap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * 标注集合<br/>
 * @author hankcs, hoobort
 */
public class TagSet implements IIndexTagMap, ITagIndexMap, Iterable<Map.Entry<String, Integer>>, ICacheAble {
	/** 
	 * 由idStringMap存储标注，形成标注集，通过stringIdMap构建标注和标注在列表中的位置映射<br/>
	 * 通过这个组合，可以根据索引提取标注，通过标注提取索引
	 *  * */
	private List<String> tagList; // 标注列表
	private Map<String, Integer> tagIndexMap; //标注和索引的映射
	/** 标签表（通过lockIndexTable来生成tag的id数组备用） * */
	private int[] indexTable;
	/** 标注集对应的训练任务类型 * */
	private TaggedType type;

	public TagSet(TaggedType type) {
		tagIndexMap = new TreeMap<String, Integer>();
		tagList = new ArrayList<String>();
		this.type = type;
	}

	/**
	 * 添加新的标签
	 *
	 * hoobort
	 * 2022年10月12日 下午4:10:12
	 * @param tag 新标签
	 * @return 如果标签存在，则返回标签的索引号（或称下标或称为id），如果不存在，则添加新的标签并返回索引号（或称下标或称为id）
	 */
	public int add(String tag) {
		Integer identity = tagIndexMap.get(tag);
		if (identity == null) {
			identity = tagList.size();
			tagList.add(tag);
			tagIndexMap.put(tag, identity);
		}

		return identity;
	}

	/**
	 * 标签总数
	 *
	 * hoobort
	 * 2022年10月12日 下午4:12:41
	 * @return
	 */
	public int size() {
		return tagList.size();
	}

	/**
	 * 已有标签总数+1, 即size+
	 *
	 * hoobort
	 * 2022年10月12日 下午4:15:34
	 * @return
	 */
	public int sizePlus() {
		return size() + 1;
	}

	/**
	 * 获取新可用序列号
	 *
	 * hoobort
	 * 2022年10月12日 下午4:16:39
	 * @return
	 */
	public int newSequenceId() {
		return size();
	}

	/**
	 * 创建并锁定标注的索引表
	 *
	 * hoobort
	 * 2022年10月12日 下午4:17:51
	 */
	public void lockIndexTable() {
		indexTable = new int[size()];
		for (int i = 0; i < size(); i++) {
			indexTable[i] = i;
		}
	}
	
	public TaggedType taskType() {
		return type;
	}
	
	public boolean sameTask(TaggedType type) {
		return this.type == type;
	}
	
	public void setTaskType(TaggedType type) {
		this.type = type;
	}

	/**
	 * 根据给定的标注id（index）获取标注
	 * @param id 映射表的索引号
	 */
	@Override
	public String tagByIndex(int id) {
		return tagList.get(id);
	}

	/**
	 * 根据标签获取索引
	 * 
	 * @param tag 标签
	 */
	@Override
	public int indexByTag(String tag) {
		Integer id = tagIndexMap.get(tag);
		if (id == null)
			id = -1;
		
		return id;
	}

	@Override
	public Iterator<Map.Entry<String, Integer>> iterator() {
		return tagIndexMap.entrySet().iterator();
	}

	/**
	 * 获取所有标签索引表
	 *
	 * @return
	 */
	public int[] tagIndexTable() {
		return indexTable;
	}

	public void save(DataOutputStream out) throws IOException {
		out.writeInt(type.ordinal());
		out.writeInt(size());
		for (String tag : tagList) {
			out.writeUTF(tag);
		}
	}

	@Override
	public boolean load(ByteArray byteArray) {
		tagList.clear();
		tagIndexMap.clear();
		int size = byteArray.nextInt();
		for (int i = 0; i < size; i++) {
			String tag = byteArray.nextUTF();
			tagList.add(tag);
			tagIndexMap.put(tag, i);
		}
		lockIndexTable();
		
		return true;
	}

	public void load(DataInputStream in) throws IOException {
		tagList.clear();
		tagIndexMap.clear();
		int size = in.readInt();
		for (int i = 0; i < size; i++) {
			String tag = in.readUTF();
			tagList.add(tag);
			tagIndexMap.put(tag, i);
		}
		lockIndexTable();
	}

	public Collection<String> tags() {
		return tagList;
	}

	public boolean contains(String tag) {
		return tagList.contains(tag);
	}
	
}
