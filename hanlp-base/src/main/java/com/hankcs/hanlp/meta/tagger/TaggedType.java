/*
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2017-10-26 下午5:22</create-date>
 *
 * <copyright file="TagSetType.java" company="码农场">
 * Copyright (c) 2017, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.meta.tagger;

/**
 * 训练任务类型<br/>
 * @author hankcs, hoobort
 */
public enum TaggedType {
	/** CRF词典模型 * */
	CWS,
	/** CRF词性标注模型 * */
	POS,
	/** CRF命名实体模型 * */
	NER,
	/** * */
	CLASSIFICATION;
}
