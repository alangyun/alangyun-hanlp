/*
 * <summary></summary>
 * <author>Hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2016-09-07 PM5:25</create-date>
 *
 * <copyright file="ByteArrayStream.java" company="码农场">
 * Copyright (c) 2008-2016, 码农场. All Right Reserved, http://www.hankcs.com/
 * This source is subject to Hankcs. Please contact Hankcs to get more information.
 * </copyright>
 */
package com.hankcs.hanlp.collection.array;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.hankcs.hanlp.config.NlpSetting;
import com.hankcs.hanlp.io.ByteArrayFileStream;
import com.hankcs.hanlp.io.ByteArrayOtherStream;
import com.hankcs.hanlp.log.NlpLogger;

/**
 * 流数组<br/>
 * 将字节流转为字节数组，利用字节数组来操作
 * @author hankcs
 */
public abstract class ByteArrayStream extends ByteArray {
	private static String myName = ByteArrayStream.class.getSimpleName();
	
	/** 每次读取1mb */
	protected int bufferSize;

	public ByteArrayStream(byte[] bytes, int bufferSize) {
		super(bytes);
		this.bufferSize = bufferSize;
	}

	public static ByteArrayStream createByteArrayStream(String path) {
		if (NlpSetting.IOAdapter == null)
			return ByteArrayFileStream.createByteArrayFileStream(path);

		try {
			InputStream is = NlpSetting.IOAdapter.open(path);
			if (is instanceof FileInputStream)
				return ByteArrayFileStream.createByteArrayFileStream((FileInputStream) is);
			return ByteArrayOtherStream.createByteArrayOtherStream(is);
		} catch (IOException e) {
			NlpLogger.warning(myName, "打开文件“{}”失败。", path);
			return null;
		}
	}

	@Override
	public int nextInt() {
		ensureAvailableBytes(4);
		return super.nextInt();
	}

	@Override
	public char nextChar() {
		ensureAvailableBytes(2);
		return super.nextChar();
	}

	@Override
	public double nextDouble() {
		ensureAvailableBytes(8);
		return super.nextDouble();
	}

	@Override
	public byte nextByte() {
		ensureAvailableBytes(1);
		return super.nextByte();
	}

	@Override
	public float nextFloat() {
		ensureAvailableBytes(4);
		return super.nextFloat();
	}

	protected abstract void ensureAvailableBytes(int size);
}
