/*
 * <summary></summary>
 * <author>hankcs</author>
 * <email>me@hankcs.com</email>
 * <create-date>2015/5/15 10:23</create-date>
 *
 * <copyright file="ValueArray.java">
 * Copyright (c) 2003-2015, hankcs. All Right Reserved, http://www.hankcs.com/
 * </copyright>
 */
package com.hankcs.hanlp.collection.trie.bintrie;

/**
 * 对值数组的包装，可以方便地取下一个
 * 
 * @author hankcs, hoobort
 */
public class TrieValueArray<V> {
	/** 值数组 * */
	protected V[] value;
	/** 当前位置 * */
	private int position;

	/**
	 * 仅仅给子类用，不要用
	 */
	protected TrieValueArray() {
		
	}

	public TrieValueArray(V[] value) {
		this.value = value;
	}

	public V nextValue() {
		return value[position++];
	}

	public TrieValueArray<V> setValue(V[] value) {
		this.value = value;
		position = 0;//hoobort add it at 2022.10.14
		
		return this;
		
	}
}
