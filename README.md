# alangyun-hanlp

#### 介绍
在开源 [Hanlp 1.7.8](https://github.com/hankcs/HanLP) 基础上，拆分和优化而形成的自然语言理解组件库，提供拼音转换、简繁体转换、关键字抽取、自动摘要、自动聚类、文本分类、分词等能力,  并提供集成在elasticsearch的分装组件，该elasticsearch组件来源于[elasticsearch-analysis-hanlp](https://github.com/KennFalcon/elasticsearch-analysis-hanlp)

#### 项目结构
| 包名 | 说明 |
| -- | -- |
| hanlp-base | 为该工程组中的其他工程提供通用的辅助工具类 |
| hanlp-data | 工程中所需的数据集，由于数据集较大，有需要的可以转到[Hanlp 1.7.8](https://github.com/hankcs/HanLP)官网提供的1.7.5的数据集 |
| hanlp-dictionary | 分离出来的针对于词典管理的工程 |
| hanlp-segment | 分离出来的专门对分词提供算法的工程 |
| elasticsearch-analysis-hanlp | 基于hanlp提供给elasticesearch使用的组件工程 |

#### 使用方法
##### hanlp使用
+ 将组件所需的词典放置到指定的目录下
+ 然后修改hanlp.properties文件中的root参数的值指向放置所需的词典根目录下
+ 初始化加载

```
在系统初始化的时候进行加载：
Predefine.HANLP_PROPERTIES_PATH = [配置文件全路径];
之后就可以正常使用了
```

#### elasticsearch-analysis-hanlp的使用
作为elasticsearch的插件，和其他elasticsearch插件部署相同

这里注意的是，<b>该版本对应的elasticsearch为7.*版本，我们使用的是elasticsearch 7.6.2的版本</b>

#### 鸣谢

感谢以下优秀开源项目：

+ [Hanlp 1.7.8](https://github.com/hankcs/HanLP)
+ [elasticsearch-analysis-hanlp](https://github.com/KennFalcon/elasticsearch-analysis-hanlp)



### 作者
[阿朗云](https://www.cheleon.com)
